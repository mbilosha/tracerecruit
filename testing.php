<?php

require __DIR__.'/vendor/autoload.php';

use GuzzleHttp\Client;

$client = new Client(['base_uri' => "http://localhost:8000",
    'defaults' => [
        'exceptions' => false        
    ],'verify' => false,'timeout'  => 10000000]);

$firstname = 'raj'.rand(0,999);
$data = array(
    'firstname' => $firstname,
    'lastname' => 'Singh',
    'email' => 'raj'.rand(0,999).'k2@gmail.com',
    'username' => 'raj2k2',
    'plainPassword' => '1234',
);

//$response = $client->post('/api/tokens',[
//    'auth' => ['raj2k2', 1234]
//]);
//
//echo $response->getBody();
//echo "\n";die;

$response = $client->post('/api/register',[
    'body' => json_encode($data)
]);

echo $response->getBody();
echo "\n";
