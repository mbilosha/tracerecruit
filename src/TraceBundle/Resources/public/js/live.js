//validation check to activate or deactivate the submit button
function is_activable(campaignid) {
    $.ajax({
        type: 'POST',
        url: Routing.generate('isactivable'),
        contentType: 'application/x-www-form-urlencoded',
        data: {campaign_id: campaignid},
        success: function (result, status, xhr) {
            res = JSON.parse(result);
            if (res == 'success') {
                $('.activate').hide().prev("input[disabled]").prop("disabled", false);
            } else if (res == 'error') {
                $('.submit').prop("disabled", true);
            }
        },
        error: function (xhr, status, error) {
            console.log(status);
        }
    });
}

//update welcome msg, thankyou msg and deadline
function update_value(n, v, campaignid, c) {
    return $.ajax({
        type: 'GET',
        dataType: 'html',
        url: Routing.generate('editlivepage'),
        contentType: 'application/x-www-form-urlencoded',
        data: {name: n, value: v, campaign_id: campaignid},
        success: function (data) {
            var res = JSON.parse(data);
            console.log(res);
            is_activable(campaignid);
        }
    });
}
//passing the name and value for welcome msg, thankyou msg and deadline to update_value() one at a time
function bindAjaxForms(campaignid, context) {
    context = $(context || 'body');
    context.find('.ajax-form:enabled').not('.criteria').autoSave(function () {
        return update_value($(this).attr('name'), $(this).val(),campaignid);
    });
}

$(document).ready(function () {
    var campaignid = $('div .campaign').attr('data-campaign-id');
    
    //load thank you and welcome message
    $.ajax({
        type: 'POST',
        url: Routing.generate('loadlivepage'),
        contentType: 'application/x-www-form-urlencoded',
        data: {campaign_id: campaignid},
        success: function (result, status, xhr) {
            var res = JSON.parse(result);
            
            //load the values of welcome and thank you msg
            $('textarea[name=live_txt_intro]').val(res['welcome']);
            $('textarea[name=live_txt_outro]').val(res['thankyou']);
            is_activable(campaignid);
        },
        error: function (xhr, status, error) {
            console.log(status);
        }
    });

    //somesh
    tinymce.init({
        selector: 'textarea.tiny-content',
        height: '400',
        language: 'en_GB',
        relative_urls: false,
        statusbar: false,
        menubar: false,
        plugins: [
            'autolink lists link image charmap',
            'contextmenu paste'
        ],
        browser_spellcheck: true,
        default_link_target: '_blank',
        link_title: false,
        toolbar: 'styleselect | bold italic | alignleft aligncenter alignright | bullist'
        , setup: function (editor) {
            editor.on('keyup', function (e) {
                var field_name = $(tinyMCE.activeEditor.getElement()).attr('name');
                var field_content = tinyMCE.activeEditor.getContent();
                $(tinyMCE.activeEditor.getElement()).autoSaveNow(function () {
                    update_value(field_name, field_content, campaignid);
                });
            });
        }
    });

    bindAjaxForms(campaignid);
});