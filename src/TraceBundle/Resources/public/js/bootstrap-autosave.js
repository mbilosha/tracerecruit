(function( $ ) {
	
	'use strict';

	$.fn.autoSave = function(doAjax) {
		this.each(function() {
			var ttype = $(this).attr('type');

			if (ttype == 'text') {
				$(this).on('keydown', function(ev) {
					if (ev.keyCode == 9) {
						throttle($(this), 0, doAjax, ev);
					}
				});
				$(this).on('keyup', function(ev) {
					if (ev.keyCode != 9) {
						throttle($(this), 500, doAjax, ev);
					}
				});
				$(this).on('paste', function(ev) { throttle($(this), 0, doAjax, ev) });
			}
			else if (ttype == 'radio' || ttype == 'checkbox' || $(this).is('select')) {
				$(this).change(function(ev) { throttle($(this), 0, doAjax, ev) });
			}
			else {
				$(this).blur(function(ev) { throttle($(this), 0, doAjax, ev) });
			}
		});

		return this;
	};

    $.fn.autoSaveNow = function(doAjax) {
		this.each(function() {
			throttle($(this), 500, doAjax, null);
		});

		return this;
    };

    function throttle($input, delay, callback, ev) {
    	$input.parent().find('span.form-autosave-feedback').remove();
		clearTimeout($input.data('timer'));
		var delayTimer = setTimeout(function() {execute($input, callback)}, delay);
		$input.data('timer', delayTimer);
	};

	function execute($input, callback) {
		var icon;
		var success;

		var loading = setTimeout(function () {
			$input.after('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate form-autosave-feedback"></span>');
		}, 300);

		$.when(callback.call($input))
			.done(function() {
		 		icon = 'glyphicon-ok text-success';
				success = true;
			})
			.fail(function() {
		 		icon = 'glyphicon-warning-sign text-warning';
				success = false;
			})
			.always(function() {
				clearTimeout(loading);
				
				$input.parent().find('span.form-autosave-feedback').remove();
				var $span = $('<span class="glyphicon ' + icon + ' form-autosave-feedback" style="display: none;"></span>');
				$input.after($span);
				$span.fadeIn(300);

				if (success) {
					$span.delay(2000).fadeOut(300);
				}
			});
	};

}( jQuery ));