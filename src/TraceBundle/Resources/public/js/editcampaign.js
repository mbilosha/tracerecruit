var $collectionHolder;

// setup an "add a document" link
var $addDocumentLink = $('<a href="#" class="btn btn-primary">Add</a>');
var $newLinkLi = $('<li class="adddocument"></li>').append($addDocumentLink);

jQuery(document).ready(function () {

    var input = document.getElementById('campaign_location');
    var options = {
//        types: ['(cities)'],
        types: ['address'],
        componentRestrictions: {
            country: "in"
        }
    };
    autocomplete = new google.maps.places.Autocomplete(input, options);

    // Get the ul that holds the collection of documents
    $collectionHolder = $('ul.documents');

    // add a delete link to all of the existing tag form li elements
    $collectionHolder.find('li').each(function () {
        addDocumentFormDeleteLink($(this));
    });

    // add the "Add" anchor and li to the tags ul
    $collectionHolder.append($newLinkLi);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);

    $addDocumentLink.on('click', function (e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new document form (see next code block)
        addDocumentForm($collectionHolder, $newLinkLi);
    });
});

function addDocumentForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    var newForm = prototype;
    // You need this only if you didn't set 'label' => false in your tags field in TaskType
    // Replace '__name__label__' in the prototype's HTML to
    // instead be a number based on how many items we have
    // newForm = newForm.replace(/__name__label__/g, index);

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a document" link li
    var $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi.before($newFormLi);

    // add a delete link to the new form
    addDocumentFormDeleteLink($newFormLi);
}

function addDocumentFormDeleteLink($documentFormLi) {
    var $removeFormA = $('<a href="#" class="btn btn-danger">Delete</a>');
    $documentFormLi.append($removeFormA);

    $removeFormA.on('click', function (e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // remove the li for the document form
        $documentFormLi.remove();
    });
} 