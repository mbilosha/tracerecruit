var $collectionHolder;
var res;
// setup an "add a tag" link
var $addTagLink = $('<a href="#" class="add_tag_link btn btn-default"><span class="glyphicon glyphicon-plus-sign"></span>\n\
Add a question</a>');
var $newLinkLi = $('<li class="addquestion"></li>').append($addTagLink);

jQuery(document).ready(function() {
    // Get the ul that holds the collection of questions
    $collectionHolder = $('ul.questions');
    
    $collectionHolder.find('li').each(function() {
        addTagFormDeleteLink($(this));
    });
    
    // add the "add a tag" anchor and li to the questions ul
    $collectionHolder.append($newLinkLi);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);

    $addTagLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        
        
        $.ajax({
            type: 'POST', 
            url:   Routing.generate('addquestion'),
            contentType: 'application/x-www-form-urlencoded',
            data: {flag: 'addquestion'},
           success: function(result,status,xhr){
                res = JSON.parse(result); 
                console.log(res);
        // add a new tag form (see next code block)
        addTagForm($collectionHolder, $newLinkLi);
            },
            error: function(xhr, status, error) {     
                    console.log(status);
                } 
        });
    });
});

function addTagForm($collectionHolder, $newLinkLi) {
            // Get the data-prototype explained earlier
            var prototype = $collectionHolder.data('prototype');

            // get the new index
            var index = $collectionHolder.data('index');

            var newForm = prototype;
            // You need this only if you didn't set 'label' => false in your questions field in TaskType
            // Replace '__name__label__' in the prototype's HTML to
            // instead be a number based on how many items we have
            // newForm = newForm.replace(/__name__label__/g, index);

            // Replace '__name__' in the prototype's HTML to
                    // instead be a number based on how many items we have
                newForm = newForm.replace(/__name__/g, index);

                // increase the index with one for the next item
                $collectionHolder.data('index', index + 1);
                
                // Display the form in the page in an li, before the "Add a tag" link li
                var $newFormLi = $('<ul class="question" data-id=\"'+ res +'\"><li></li></ul>').append(newForm);
                $newLinkLi.before($newFormLi);

                // add a delete link to the new form
                addTagFormDeleteLink($newFormLi);
        }

function addTagFormDeleteLink($tagFormLi) {
            var $removeFormA = $('<a href="#" class="collection-remove btn btn-default"><span class="glyphicon glyphicon-trash"></span>\n\
        Delete above question</a>');
            $tagFormLi.append($removeFormA);

            $removeFormA.on('click', function(e) {
                // prevent the link from creating a "#" on the URL
                e.preventDefault();

                // remove the li for the tag form
                $tagFormLi.remove();
            });
        }    