$(document).ready(function () {
    var campaignid = $('div .campaign').attr('data-campaign-id');
    var documentid;
    var questionid;
    $('#dropzone-media').dropzone({
        acceptedFiles: '.mp4, .mpeg',
        dictInvalidFileType: 'Incorrect file format',
        maxFilesize: Math.round(10485760 / 1024 / 1024),
        dictFileTooBig: 'File too large',
        url: Routing.generate("videouploadpage"),
        processing: function () {
            $('.spinner-container').removeClass('hidden');
        },
        complete: function (file) {
            this.removeFile(file);
            $('.spinner-container').addClass('hidden');
        },
        success: function (file, data) {
            $('#documents').append(data);
            $('.video-div').click(function () {

                $(".video-div").removeClass('active');
                $(this).addClass('active');
                documentid = $(this).find("video").attr('data-document-id');

                if ($(".video-div").hasClass('active')) {
                    $('.activate').hide().prev("input[disabled]").prop("disabled", false);

                    $('.insert-btn').click(function () {
                        questionid = $(this).attr('data-question-id');
                        $.ajax({
                            type: 'POST',
                            url: Routing.generate('videoselectpage'),
                            contentType: 'application/x-www-form-urlencoded',
                            data: {question_id: questionid, document_id: documentid},
                            success: function (result, status, xhr) {
                                res = JSON.parse(result);
                                if (res == 'success') {
                                    $(".video-div").removeClass('active');
                                    $('#videoModal').modal('hide');
                                    $("#videoModal").on('hidden.bs.modal', function () {
                                        $(this).data('bs.modal', null);
                                        $(this).remove();
                                    });
                                    $('.videolink' + questionid).children().attr('src', '/noformcomponentproject/web/bundles/app/img/play-active.png');
                                    $('.videolink' + questionid).children().css("top", "28px");
                                    $('.videolink' + questionid).addClass('active');
                                    $('.removevideo' + questionid).css("display", "initial");
                                    $('.removevideo' + questionid).attr('data-document-id', documentid);
                                    $('.videolink' + questionid).attr('data-document-id', documentid);
                                    $('.videolink' + questionid).css("background-color", "#3C74B8");
                                    $('.videolink' + questionid).attr('class', 'media-video show-edition-mode customvideoplay videolink' + questionid);

                                    $('.removevideo' + questionid).click(function () {
                                        var docid = $(this).attr('data-document-id');
                                        $.ajax({
                                            type: 'POST',
                                            url: Routing.generate('videoremovepage'),
                                            contentType: 'application/x-www-form-urlencoded',
                                            data: {question_id: questionid, document_id: docid},
                                            success: function (result, status, xhr) {
                                                res = JSON.parse(result);
                                                if (res == 'success') {
                                                    $('.videolink' + questionid).children().attr('src', '/noformcomponentproject/web/bundles/app/img/play.png');
                                                    $('.videolink' + questionid).children().css("top", "17px");
                                                    $('.videolink' + questionid).removeClass('active');
                                                    $('.removevideo' + questionid).css("display", "none");
                                                    $('.videolink' + questionid).css("background-color", "#E3E5EE");
                                                    $('.videolink' + questionid).removeClass('customvideoplay');

                                                }
                                                is_activable(campaignid);
                                            },
                                            error: function (xhr, status, error) {
                                                console.log(status);
                                            }
                                        });
                                    });
                                }
                                is_activable(campaignid);
                            },
                            error: function (xhr, status, error) {
                                console.log(status);
                            }
                        });
                    });

                } else {
                    $(this).prop("disabled", true);
                }
            });
        },
        error: function (file, error) {
            $('#error-message').html(error);
        }
    });

    $("#videoModal").on('hidden.bs.modal', function () {
        $(this).data('bs.modal', null);
        $(this).remove();
    });

    $('.video-div').click(function () {

        $(".video-div").removeClass('active');
        $(this).addClass('active');
        documentid = $(this).find("video").attr('data-document-id');

        if ($(".video-div").hasClass('active')) {
            $('.activate').hide().prev("input[disabled]").prop("disabled", false);

            $('.insert-btn').click(function () {
                questionid = $(this).attr('data-question-id');
                $.ajax({
                    type: 'POST',
                    url: Routing.generate('videoselectpage'),
                    contentType: 'application/x-www-form-urlencoded',
                    data: {question_id: questionid, document_id: documentid},
                    success: function (result, status, xhr) {
                        res = JSON.parse(result);
                        if (res == 'success') {
                            $(".video-div").removeClass('active');
                            $('#videoModal').modal('hide');
                            $("#videoModal").on('hidden.bs.modal', function () {
                                $(this).data('bs.modal', null);
                                $(this).remove();
                            });
                            $('.videolink' + questionid).children().attr('src', '/noformcomponentproject/web/bundles/app/img/play-active.png');
                            $('.videolink' + questionid).children().css("top", "28px");
                            $('.videolink' + questionid).addClass('active');
                            $('.removevideo' + questionid).css("display", "initial");
                            $('.removevideo' + questionid).attr('data-document-id', documentid);
                            $('.videolink' + questionid).attr('data-document-id', documentid);
                            $('.videolink' + questionid).css("background-color", "#3C74B8");
                            $('.videolink' + questionid).attr('class', 'media-video show-edition-mode customvideoplay videolink' + questionid);

                            $('.removevideo' + questionid).click(function () {
                                var docid = $(this).attr('data-document-id');
                                $.ajax({
                                    type: 'POST',
                                    url: Routing.generate('videoremovepage'),
                                    contentType: 'application/x-www-form-urlencoded',
                                    data: {question_id: questionid, document_id: docid},
                                    success: function (result, status, xhr) {
                                        res = JSON.parse(result);
                                        if (res == 'success') {
                                            $('.videolink' + questionid).children().attr('src', '/noformcomponentproject/web/bundles/app/img/play.png');
                                            $('.videolink' + questionid).children().css("top", "17px");
                                            $('.videolink' + questionid).removeClass('active');
                                            $('.removevideo' + questionid).css("display", "none");
                                            $('.videolink' + questionid).css("background-color", "#E3E5EE");
                                            $('.videolink' + questionid).removeClass('customvideoplay');

                                        }
                                        is_activable(campaignid);
                                    },
                                    error: function (xhr, status, error) {
                                        console.log(status);
                                    }
                                });
                            });
                        }
                        is_activable(campaignid);
                    },
                    error: function (xhr, status, error) {
                        console.log(status);
                    }
                });
            });

        } else {
            $(this).prop("disabled", true);
        }
    });
});