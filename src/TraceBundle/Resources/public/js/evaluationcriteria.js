$(document).ready(function () {
    $('.add-criteria').toggleClass('hidden', $('.criterias .criteria').length >= 10);

    $('.add-criteria').click(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: Routing.generate('evalcriteriapage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {flag: 'addcriterion'},
            success: function (result, status, xhr) {
                var res = JSON.parse(result);
                $('.criterias').append(res['html']);
                $('.add-criteria').toggleClass('hidden', $('.criterias .criteria').length >= 10);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });
    });

    //remove criterion
    $(document).on('click', '.btn_remove', function (e) {
        e.preventDefault();
        var button_id = $(this).attr("id");
        $.ajax({
            type: 'POST',
            url: Routing.generate('evalcriteriapage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {criterion_id: button_id, flag: 'deletecriterion'},
            success: function (result, status, xhr) {
                res = JSON.parse(result);
                if (res == 'success') {
                    $(".row" + button_id + "").remove();
                    $('.add-criteria').toggleClass('hidden', $('.criterias .criteria').length >= 10);
                }
                is_activable();
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });

    });

    //edit criterion
    $(document).on('input', '.criteria-label', function () {
        var value = $(this).val();
        var criterionid = $(this).attr("data-criterion-id");
        $.ajax({
            type: 'POST',
            url: Routing.generate('evalcriteriapage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {criterion_id: criterionid, value: value, flag: 'editcriterion'},
            success: function (result, status, xhr) {
                res = JSON.parse(result);
                is_activable();
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });

    });
});