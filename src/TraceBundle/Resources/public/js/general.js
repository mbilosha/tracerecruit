//validation check to activate or deactivate the submit button
function is_activable(campaignid) {
    $.ajax({
        type: 'POST',
        url: Routing.generate('isactivable'),
        contentType: 'application/x-www-form-urlencoded',
        data: {campaign_id: campaignid},
        success: function (result, status, xhr) {
            var res = JSON.parse(result);
            if (res == 'success') {
                $('.activate').hide().prev("input[disabled]").prop("disabled", false);
            } else if (res == 'error') {
                $('.submit').prop("disabled", true);
            }
        },
        error: function (xhr, status, error) {
            console.log(status);
        }
    });
}

function update_jobreference(value, campaignid) {
    $.ajax({
        type: 'POST',
        url: Routing.generate('editjobreferencepage'),
        contentType: 'application/x-www-form-urlencoded',
        data: {value: value, campaign_id: campaignid},
        success: function (result, status, xhr) {
            var res = JSON.parse(result);
            is_activable(campaignid);
        },
        error: function (xhr, status, error) {
            console.log(status);
        }
    });
}

function update_documentname(value, documentid, campaignid) {
    $.ajax({
        type: 'POST',
        url: Routing.generate('editdocumentnamepage'),
        contentType: 'application/x-www-form-urlencoded',
        data: {document_id: documentid, value: value},
        success: function (result, status, xhr) {
            var res = JSON.parse(result);
            is_activable(campaignid);
        },
        error: function (xhr, status, error) {
            console.log(status);
        }
    });
}


$(document).ready(function () {
    var campaignid = $('.campaign-general').attr('data-campaign-id');

    //hide/show the set reference field
    $('#addreference').click(function (ev) {
        ev.preventDefault();
        $('.setreference').toggleClass('hidden');
        if ($('.setreference').hasClass('hidden')) {
            $('#jobreference').val('');
            var value = $('#jobreference').val();
            update_jobreference(value, campaignid);
        }
    });

    //display jobreference field on page load if it has value
    if ($('#jobreference').val() != '') {
        $('.setreference').removeClass('hidden');
    }

    //google autocomplete places api
    var input = document.getElementById('location');
    var options = {
        //        types: ['(cities)'],
        types: ['address'],
        componentRestrictions: {
            country: "in"
        }
    };
    autocomplete = new google.maps.places.Autocomplete(input, options);

    //Note:rest of the fields get loaded using twig except for language
    //load applicant language
    $.ajax({
        type: 'POST',
        url: Routing.generate('loadapplicantlanguagepage'),
        contentType: 'application/x-www-form-urlencoded',
        data: {campaign_id: campaignid},
        success: function (result, status, xhr) {
            var res = JSON.parse(result);
            $('#applicant-language').val(res);
            is_activable(campaignid);
        },
        error: function (xhr, status, error) {
            console.log(status);
        }
    });

    //edit campaign title
    $(document).on('input', '#title', function () {
        var value = $(this).val();
        $.ajax({
            type: 'POST',
            url: Routing.generate('edittitlepage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {value: value, campaign_id: campaignid},
            success: function (result, status, xhr) {
                var res = JSON.parse(result);
                is_activable(campaignid);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });
    });

    //edit campaign location
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var value = $('#location').val();
        $.ajax({
            type: 'POST',
            url: Routing.generate('editlocationpage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {value: value, campaign_id: campaignid},
            success: function (result, status, xhr) {
                var res = JSON.parse(result);
                is_activable(campaignid);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });
    });

    //edit job reference
    $(document).on('input', '#jobreference', function () {
        var value = $(this).val();
        update_jobreference(value, campaignid);
    });

    //edit applicant language
    $(document).on('change', '#applicant-language', function () {
        var value = $(this).val();
        $.ajax({
            type: 'POST',
            url: Routing.generate('editapplicantlanguagepage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {value: value, campaign_id: campaignid},
            success: function (result, status, xhr) {
                var res = JSON.parse(result);
                is_activable(campaignid);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });
    });

    //add documents requested
    $('#adddocument').click(function (e) {
        $.ajax({
            type: 'POST',
            url: Routing.generate('adddocumentpage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {campaign_id: campaignid},
            success: function (result, status, xhr) {
                var res = JSON.parse(result);
                $(".dynamic-document").append(res);
                is_activable(campaignid);
                //update requested document name after addition
                $(document).on('input', '.document', function () {
                    var value = $(this).val();
                    var documentid = $(this).attr("data-document-id");
                    update_documentname(value, documentid, campaignid);
                });
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });
    });

    //remove document requested
    $(document).on('click', '.remove_document', function (e) {
        e.preventDefault();
        var documentid = $(this).attr("id");
        $.ajax({
            type: 'POST',
            url: Routing.generate('deletedocumentpage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {document_id: documentid},
            success: function (result, status, xhr) {
                var res = JSON.parse(result);
                if (res == 'success') {
                    $('.dynamic-document .row' + documentid).remove();
                }
                is_activable(campaignid);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });
    });

    //edit requested document name
    $(document).on('input', '.document', function () {
        var value = $(this).val();
        var documentid = $(this).attr("data-document-id");
        update_documentname(value, documentid, campaignid);
    });
}); 