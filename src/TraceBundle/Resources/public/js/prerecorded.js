//manage the order of the criterions on add, delete and load
function manage_criterion_order() {
    var index = 0;
    $('.criterias .control-label').each(function () {
        ++index;
        $(this).html('Criterion n°' + index);
    });
}
//manage the order of the questions on add, delete, load and sort
function manage_order(campaignid) {
    var index = 0;
    $('#dynamic_field .question-index').each(function () {
        $(this).html(++index);
    });

    $.ajax({
        type: 'POST',
        url: Routing.generate('sortorderpage'),
        contentType: 'application/x-www-form-urlencoded',
        data: {
            questions: $.map($('#dynamic_field .questn'), function (q) {
                return $(q).attr('data-question-id');
            }),
            campaign_id: campaignid
        },
        success: function (result, status, xhr) {
            res = JSON.parse(result);
            is_activable(campaignid);
        },
        error: function (xhr, status, error) {
            console.log(status);
        }
    });
}
//step-form(onclick function to switch btw questionaire,Eval criteria and Invtn&welcome)
function openPage(pageName, elmnt) {
    // Hide all elements with class="tabcontent" by default */
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.backgroundColor = "white";
        tablinks[i].style.color = "#AFB1B2";
    }
    // Show the specific tab content
    document.getElementById(pageName).style.display = "block";

    // Add the specific color to the button used to open the tab content
    elmnt.style.color = '#00537C';

}
//validation check to activate or deactivate the submit button
function is_activable(campaignid) {
    $.ajax({
        type: 'POST',
        url: Routing.generate('isactivable'),
        contentType: 'application/x-www-form-urlencoded',
        data: {campaign_id: campaignid},
        success: function (result, status, xhr) {
            res = JSON.parse(result);
            if (res == 'success') {
                $('.activate').hide().prev("input[disabled]").prop("disabled", false);
            } else if (res == 'error') {
                $('.submit').prop("disabled", true);
            }
        },
        error: function (xhr, status, error) {
            console.log(status);
        }
    });
}

//update welcome msg, thankyou msg and deadline
function update_value(n, v, campaignid, c) {
    return $.ajax({
        type: 'GET',
        dataType: 'html',
        url: Routing.generate('editmsgsanddeadlinepage'),
        contentType: 'application/x-www-form-urlencoded',
        data: {name: n, value: v, campaign_id: campaignid},
        success: function (data) {
            var res = JSON.parse(data);
            console.log(res);
        }
    });
}
//passing the name and value for welcome msg, thankyou msg and deadline to update_value() one at a time
function bindAjaxForms(campaignid, context) {
    context = $(context || 'body');
    context.find('.ajax-form:enabled').not('.criteria').autoSave(function () {
        return update_value($(this).attr('name'), $(this).val(),campaignid);
    });
}

function edit_choice_value(choiceid, value, campaignid) {
    $.ajax({
        type: 'POST',
        url: Routing.generate('editchoicepage'),
        contentType: 'application/x-www-form-urlencoded',
        data: {choice_id: choiceid, value: value, flag: 'editchoicevalue'},
        success: function (result, status, xhr) {
            res = JSON.parse(result);
            is_activable(campaignid);
        },
        error: function (xhr, status, error) {
            console.log(status);
        }
    });
}

function edit_hasmultiple(questionid, value, campaignid) {
    $.ajax({
        type: 'POST',
        url: Routing.generate('editchoicepage'),
        contentType: 'application/x-www-form-urlencoded',
        data: {question_id: questionid, value: value, flag: 'edithasmultiple'},
        success: function (result, status, xhr) {
            res = JSON.parse(result);
            is_activable(campaignid);
        },
        error: function (xhr, status, error) {
            console.log(status);
        }
    });
}

function add_choice(questionid, campaignid) {
    $.ajax({
        type: 'POST',
        url: Routing.generate('addchoicepage'),
        contentType: 'application/x-www-form-urlencoded',
        data: {question_id: questionid},
        success: function (result, status, xhr) {
            var res = JSON.parse(result);
            $(".row" + questionid + " #dynamic_choice").append(res);
            is_activable(campaignid);
        },
        error: function (xhr, status, error) {
            console.log(status);
        }
    });
}
//here button_id = choiceid
function delete_choice(questionid, button_id, campaignid) {
    $.ajax({
        type: 'POST',
        url: Routing.generate('deletechoicepage'),
        contentType: 'application/x-www-form-urlencoded',
        data: {choice_id: button_id, flag: 'deletechoice'},
        success: function (result, status, xhr) {
            res = JSON.parse(result);
            if (res == 'success') {
                $(".row" + questionid + " .choice" + button_id + "").remove();
            }
            is_activable(campaignid);
        },
        error: function (xhr, status, error) {
            console.log(status);
        }
    });
}

$(document).ready(function () {
    var campaignid = $('div .campaign').attr('data-campaign-id');
    manage_order(campaignid);
    manage_criterion_order();
    //click on the first step(Questionaire)
    document.getElementById("defaultOpen").click();
    var last_date;
    //jquery sortable ui
    $("#dynamic_field").sortable({
        handle: '.handle',
        update: function (event, ui) {
            manage_order(campaignid);
        }
    });
    
    $(".responsetype").chosen({
        display_selected_options: false,               
        placeholder_text_multiple: 'Select responsetype',
        no_results_text: "Oops, no responsetype found!"
    });
    $(".thinktime").chosen({
       display_selected_options: false, 
       width: "100%",
       placeholder_text_multiple: 'Select thinktime',
       no_results_text: "Oops, no thinktime found!" 
    });
    $(".maxtake").chosen({
       display_selected_options: false, 
       width: "100%",
       placeholder_text_multiple: 'Select maxtake',
       no_results_text: "Oops, no maxtake found!" 
    });
    
    $("#dynamic_field").disableSelection();

    //load invitation & welcome
    $.ajax({
        type: 'POST',
        url: Routing.generate('loadmsgsanddeadlinepage'),
        contentType: 'application/x-www-form-urlencoded',
        data: {campaign_id: campaignid},
        success: function (result, status, xhr) {
            var res = JSON.parse(result);
                $('#answer_period').val(res['deadline']);
            //load the values of welcome and thank you msg
            $('textarea[name=txt_intro]').val(res['welcome']);
            $('textarea[name=txt_outro]').val(res['thankyou']);
            is_activable(campaignid);
        },
        error: function (xhr, status, error) {
            console.log(status);
        }
    });

    //load ispublic
    $.ajax({
        type: 'POST',
        url: Routing.generate('loadispublicpage'),
        contentType: 'application/x-www-form-urlencoded',
        data: {campaign_id: campaignid},
        success: function (result, status, xhr) {
            var res = JSON.parse(result);
            if(res==true){
                $('#ispublic').prop('checked', true);
            } else {
                $('#ispublic').prop('checked', false);
            }
            is_activable(campaignid);
        },
        error: function (xhr, status, error) {
            console.log(status);
        }
    });
    
    //edit ispublic
    $(document).on('change', "#ispublic", function () {
        var ispublic = $(this).prop('checked');
        
        if(ispublic==true){
            var value = 1;
        } else {
            var value = 0;
        }
        
        $.ajax({
            type: 'POST',
            url: Routing.generate('editispublicpage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {value: value, campaign_id: campaignid},
            success: function (result, status, xhr) {
                var res = JSON.parse(result);
                is_activable(campaignid);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });
    });
    
    //somesh    
    saved_date = $('#answer_date').val();
    saved_period = $('#answer_period').val();

    $('#public-link').focus(function (ev) {
        $(this).trigger('select');
    })

    $('.trigger-link').click(function (ev) {
        ev.preventDefault();

        $('.show-link').toggleClass('hidden');

        update_value('has_public', $('.has-link').hasClass('hidden') ? 1 : 0, campaignid);
    });

    //convert text field to calender field
//    $('#answer_date').datetimepicker({
//        lang: 'en',
//        format: 'd/m/Y',
//        dayOfWeekStart: 1,
//        step: 30,
//        minDate: 0,
//        timepicker: false,
//        closeOnDateSelect: true,
//        onChangeDateTime: function (dp, dtp) {
//            if (last_date != dtp.val() && dtp.val() != '') {
//                last_date = dtp.val();
//                check_candidates_impacted(last_date, function () {
//                    dtp.autoSaveNow(function () {
//                        return update_value('answer_date', last_date, campaignid);
//                    });
//                }, campaignid);
//            }
//        }
//    });

//    $('#answer_period').change(function (ev) {
//        //hide the calender field if deadline != untill, else display 
//        $('#answer_date').toggleClass('hidden', $(this).val() != 0);
//        //remove the calender fields value if deadline != untill
//        if ($(this).val() == 0) {
//            $('#answer_date').val('').trigger('focus');
//        }
//    });

    //somesh
    tinymce.init({
        selector: 'textarea.tiny-content',
        height: '400',
        language: 'en_GB',
        relative_urls: false,
        statusbar: false,
        menubar: false,
        plugins: [
            'autolink lists link image charmap',
            'contextmenu paste'
        ],
        browser_spellcheck: true,
        default_link_target: '_blank',
        link_title: false,
        toolbar: 'styleselect | bold italic | alignleft aligncenter alignright | bullist'
        , setup: function (editor) {
            editor.on('keyup', function (e) {
                var field_name = $(tinyMCE.activeEditor.getElement()).attr('name');
                var field_content = tinyMCE.activeEditor.getContent();
                $(tinyMCE.activeEditor.getElement()).autoSaveNow(function () {
                    update_value(field_name, field_content, campaignid);
                });
            });
        }
    });

    bindAjaxForms(campaignid);

    //get the html collection of all question elements 
    var questionelements = document.getElementsByClassName("questn");
    //loop through the collection
    for (var i = 0; i < questionelements.length; i++) {
        //to loop through the questionids use closure
        (function (i) {
            var questionid = questionelements[i].getAttribute('data-question-id');
            //load israndom
            $.ajax({
                type: 'POST',
                url: Routing.generate('loadquestionpage'),
                contentType: 'application/x-www-form-urlencoded',
                data: {question_id: questionid, flag: 'loadisrandom'},
                success: function (result, status, xhr) {
                    res = JSON.parse(result);
                    if (res === true) {
                        $('.row' + questionid).find('.random-btn').children().attr('src', '/noformcomponentproject/web/bundles/app/img/random-active.png');
                        $('.row' + questionid).find('.random-btn').children().addClass('random-active');
                        $('.row' + questionid).find('.random-btn').css("background-color", "#3C74B8");
                    } else if (res === false) {
                        $('.row' + questionid).find('.random-btn').children().attr('src', '/noformcomponentproject/web/bundles/app/img/random.png');
                        $('.row' + questionid).find('.random-btn').children().removeClass('random-active');
                        $('.row' + questionid).find('.random-btn').css("background-color", "#E3E5EE");
                    }
                    is_activable(campaignid);
                },
                error: function (xhr, status, error) {
                    console.log(status);
                }
            });

            //load video
            $.ajax({
                type: 'POST',
                url: Routing.generate('loadquestionpage'),
                contentType: 'application/x-www-form-urlencoded',
                data: {question_id: questionid, flag: 'loadvideo'},
                success: function (result, status, xhr) {
                    res = JSON.parse(result);

                    if (res['isvideo'] === true) {
                        var documentid = res['documentid'];
                        $('.videolink' + questionid).children().attr('src', '/noformcomponentproject/web/bundles/app/img/play-active.png');
                        $('.videolink' + questionid).children().css("top", "28px");
                        $('.videolink' + questionid).addClass('active');
                        $('.removevideo' + questionid).css("display", "initial");
                        $('.removevideo' + questionid).attr('data-document-id', documentid);
                        $('.videolink' + questionid).attr('data-document-id', documentid);
                        $('.videolink' + questionid).css("background-color", "#3C74B8");
                        $('.videolink' + questionid).attr('class', 'media-video show-edition-mode customvideoplay videolink' + questionid);

                        $('.videolinkcustom').click(function (e) {
                            e.preventDefault();
                            var questionid = $(this).attr('data-question-id');
                            //if clicked on the video play icon load video play modal
                            if ($(this).hasClass('customvideoplay')) {
                                var docid = $(this).attr('data-document-id');
                                $.ajax({
                                    type: 'POST',
                                    url: Routing.generate('videoplaypage'),
                                    contentType: 'application/x-www-form-urlencoded',
                                    data: {document_id: docid, question_id: questionid},
                                    success: function (result, status, xhr) {
                                        res = JSON.parse(result);
                                        $('.custom-video-play').append(res);
                                        $("#videoplayModal").modal();
                                        is_activable(campaignid);
                                    },
                                    error: function (xhr, status, error) {
                                        console.log(status);
                                    }
                                });
                            } else {
                                //else if clicked on the video upload icon, load the video upload modal
                                $.ajax({
                                    type: 'POST',
                                    url: Routing.generate('editquestionpage'),
                                    contentType: 'application/x-www-form-urlencoded',
                                    data: {question_id: questionid, flag: 'editvideo'},
                                    success: function (result, status, xhr) {
                                        res = JSON.parse(result);
                                        $('.custom-modal').append(res['html']);
                                        $("#videoModal").modal();
                                        $(this).find('.custom-modal').find('.insert-btn').attr('data-question-id', questionid);
                                        $(".video-div").removeClass('active');
                                        $(this).find('.custom-modal').find('.insert-btn').prop("disabled", true);
                                        is_activable(campaignid);
                                    },
                                    error: function (xhr, status, error) {
                                        console.log(status);
                                    }
                                });
                            }
                        });
                        //if clicked on remove video button
                        $('.removevideo' + questionid).click(function () {
                            var docid = $(this).attr('data-document-id');
                            $.ajax({
                                type: 'POST',
                                url: Routing.generate('videoremovepage'),
                                contentType: 'application/x-www-form-urlencoded',
                                data: {question_id: questionid, document_id: docid, flag: 'removevideo'},
                                success: function (result, status, xhr) {
                                    res = JSON.parse(result);
                                    if (res == 'success') {
                                        $('.videolink' + questionid).children().attr('src', '/noformcomponentproject/web/bundles/app/img/play.png');
                                        $('.videolink' + questionid).children().css("top", "17px");
                                        $('.videolink' + questionid).removeClass('active');
                                        $('.removevideo' + questionid).css("display", "none");
                                        $('.videolink' + questionid).css("background-color", "#E3E5EE");
                                        $('.videolink' + questionid).removeClass('customvideoplay');
                                    }
                                    is_activable(campaignid);
                                },
                                error: function (xhr, status, error) {
                                    console.log(status);
                                }
                            });
                        });
                    } else if (res['isvideo'] === false) {
                        console.log('There is no video');
                    }
                    is_activable(campaignid);
                },
                error: function (xhr, status, error) {
                    console.log(status);
                }
            }); //load video ends here

            //load question name
            $.ajax({
                type: 'POST',
                url: Routing.generate('loadquestionpage'),
                contentType: 'application/x-www-form-urlencoded',
                data: {question_id: questionid, flag: 'loadquestionname'},
                success: function (result, status, xhr) {
                    res = JSON.parse(result);
                    $('.row' + questionid).find('.question_name').val(res);
                    is_activable(campaignid);
                },
                error: function (xhr, status, error) {
                    console.log(status);
                }
            });

            //load responsetype
            $.ajax({
                type: 'POST',
                url: Routing.generate('loadquestionpage'),
                contentType: 'application/x-www-form-urlencoded',
                data: {question_id: questionid, flag: 'loadresponsetype'},
                success: function (result, status, xhr) {
                    res = JSON.parse(result);
                    var value = res['responsetype'];
                    $('.row' + questionid).find('.responsetype').val(res['responsetype']).trigger('chosen:updated');
                    //if responsetype is choice
                    if (value == 4) {
                        $(".row" + questionid + " .maxtake").remove();
                        $(".row" + questionid + " .choicewidget").append(res['html']);

                        //load hasmultiple
                        $.ajax({
                            type: 'POST',
                            url: Routing.generate('loadquestionpage'),
                            contentType: 'application/x-www-form-urlencoded',
                            data: {question_id: questionid, flag: 'loadhasmultiple'},
                            success: function (result, status, xhr) {
                                res = JSON.parse(result);
                                if (res == 1) {
                                    $('.row' + questionid).find('.hasmultiple').prop("checked", true);
                                } else {
                                    $('.row' + questionid).find('.hasmultiple').prop("checked", false);
                                }
                                is_activable(campaignid);
                            },
                            error: function (xhr, status, error) {
                                console.log(status);
                            }
                        });
                        //load choices same as we did for load questions
                        var choiceelements = document.getElementsByClassName("choice");

                        for (var j = 0; j < choiceelements.length; j++) {
                            (function (j) {
                                var choiceid = choiceelements[j].getAttribute('data-id');
                                $.ajax({
                                    type: 'POST',
                                    url: Routing.generate('loadchoicepage'),
                                    contentType: 'application/x-www-form-urlencoded',
                                    data: {choice_id: choiceid},
                                    success: function (result, status, xhr) {
                                        res = JSON.parse(result);
                                        $('.choice' + choiceid).find('.choice').val(res);
                                        is_activable(campaignid);
                                    },
                                    error: function (xhr, status, error) {
                                        console.log(status);
                                    }
                                });

                            })(j);
                        }

                        //choice form(hasmultiple, addchoice etc.) edit after load
                        //choice value
                        $(document).on('input', ".row" + questionid + " .choice", function () {
                            var value = $(this).val();
                            var choiceid = $(this).attr("data-id");
                            edit_choice_value(choiceid, value, campaignid);
                        });

                        //hasmultiple answer
                        $(document).on('click', ".row" + questionid + " .hasmultiple", function () {
                            var value = $(this).attr("value");
                            edit_hasmultiple(questionid, value, campaignid);
                        });

                        //add choice
                        $(".row" + questionid + " #addoption").click(function (e) {
                            e.preventDefault();
                            add_choice(questionid, campaignid);
                        });

                        //delete choice
                        $(document).on('click', ".row" + questionid + " .btn_remove_choice", function (e) {
                            e.preventDefault();
                            var button_id = $(this).attr("id");
                            delete_choice(questionid, button_id, campaignid);
                        }); //choice form edit after load ends here

                    } //responsetype choice ends here

                    //if responsetype is video
                    else if (value == 3) {
                        $(".row" + questionid + " .choicewrap").remove();
                        $(".row" + questionid + " .maxtakewidget").append(res['html']);
                        //load maxtake value
                        $.ajax({
                            type: 'POST',
                            url: Routing.generate('loadquestionpage'),
                            contentType: 'application/x-www-form-urlencoded',
                            data: {question_id: questionid, flag: 'loadmaxtakevalue'},
                            success: function (result, status, xhr) {
                                res = JSON.parse(result);
                                $('.row' + questionid).find('.maxtake').val(res).trigger('chosen:updated');
                                is_activable(campaignid);
                            },
                            error: function (xhr, status, error) {
                                console.log(status);
                            }
                        });
                    } //responsetype video ends here
                    is_activable(campaignid);
                },
                error: function (xhr, status, error) {
                    console.log(status);
                }
            });// load responsetype ends here

            //load thinktime
            $.ajax({
                type: 'POST',
                url: Routing.generate('loadquestionpage'),
                contentType: 'application/x-www-form-urlencoded',
                data: {question_id: questionid, flag: 'loadthinktime'},
                success: function (result, status, xhr) {
                    res = JSON.parse(result);
                    $(".row" + questionid + " .thinktime").val(res).trigger('chosen:updated');
                    is_activable(campaignid);
                },
                error: function (xhr, status, error) {
                    console.log(status);
                }
            });

            //load slider
            $.ajax({
                type: 'POST',
                url: Routing.generate('loadquestionpage'),
                contentType: 'application/x-www-form-urlencoded',
                data: {question_id: questionid, flag: 'loadslider'},
                success: function (result, status, xhr) {
                    var res = JSON.parse(result);
                    $(".row" + questionid + " #slider-range").slider({
                        range: true,
                        min: 10,
                        max: 300,
                        values: [10, 60],
                        slide: function (event, ui) {
                            $(".row" + questionid + " #minduration").val(ui.values[ 0 ]);
                            $(".row" + questionid + " #maxduration").val(ui.values[ 1 ]);
                        },
                        change: function (event, ui) {
                            //edit minduration
                            var minvalue = ui.values[ 0 ];
                            $.ajax({
                                type: 'POST',
                                url: Routing.generate('editquestionpage'),
                                contentType: 'application/x-www-form-urlencoded',
                                data: {question_id: questionid, value: minvalue, flag: 'editminduration'},
                                success: function (result, status, xhr) {
                                    res = JSON.parse(result);
                                    is_activable(campaignid);
                                },
                                error: function (xhr, status, error) {
                                    console.log(status);
                                }
                            });


                            //edit maxduration
                            var maxvalue = ui.values[ 1 ];
                            $.ajax({
                                type: 'POST',
                                url: Routing.generate('editquestionpage'),
                                contentType: 'application/x-www-form-urlencoded',
                                data: {question_id: questionid, value: maxvalue, flag: 'editmaxduration'},
                                success: function (result, status, xhr) {
                                    res = JSON.parse(result);
                                    is_activable(campaignid);
                                },
                                error: function (xhr, status, error) {
                                    console.log(status);
                                }
                            });

                        }
                    });
                    //update the min and max values on the slider
                    $(".row" + questionid + " #slider-range").slider("values", 0, res['min']);
                    $(".row" + questionid + " #slider-range").slider("values", 1, res['max']);
                    //update the min and max values on the text fields
                    $(".row" + questionid + " #minduration").val(res['min']);
                    $(".row" + questionid + " #maxduration").val(res['max']);

                    is_activable(campaignid);
                },
                error: function (xhr, status, error) {
                    console.log(status);
                }
            });
        })(i); // closure ends here
    }
    ; //question elements for loop ends here

    //add question
    $('#add').click(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: Routing.generate('addquestionpage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {campaign_id : campaignid},
            success: function (result, status, xhr) {
                var res = JSON.parse(result);
                var questionid = res['questionid'];
                $('#dynamic_field').append(res['html']);
                manage_order(campaignid);
                $(".row" + questionid + " #slider-range").slider({
                    range: true,
                    min: 10,
                    max: 300,
                    values: [10, 60],
                    slide: function (event, ui) {
                        $(".row" + questionid + " #minduration").val(ui.values[ 0 ]);
                        $(".row" + questionid + " #maxduration").val(ui.values[ 1 ]);
                    },
                    change: function (event, ui) {
                        //minduration
                        var value = ui.values[ 0 ];
                        $.ajax({
                            type: 'POST',
                            url: Routing.generate('editquestionpage'),
                            contentType: 'application/x-www-form-urlencoded',
                            data: {question_id: questionid, value: value, flag: 'editminduration'},
                            success: function (result, status, xhr) {
                                res = JSON.parse(result);
                                is_activable(campaignid);
                            },
                            error: function (xhr, status, error) {
                                console.log(status);
                            }
                        });


                        //maxduration
                        var value = ui.values[ 1 ];
                        $.ajax({
                            type: 'POST',
                            url: Routing.generate('editquestionpage'),
                            contentType: 'application/x-www-form-urlencoded',
                            data: {question_id: questionid, value: value, flag: 'editmaxduration'},
                            success: function (result, status, xhr) {
                                res = JSON.parse(result);
                                is_activable(campaignid);
                            },
                            error: function (xhr, status, error) {
                                console.log(status);
                            }
                        });

                    }
                });

                $(".row" + questionid + " #minduration").val($(".row" + questionid + " #slider-range").slider("values", 0));
                $(".row" + questionid + " #maxduration").val($(".row" + questionid + " #slider-range").slider("values", 1));

                $('.videolinkcustom').click(function (e) {
                    e.preventDefault();
                    var questionid = $(this).attr('data-question-id');
                    //if clicked on the video play icon load video play modal 
                    if ($(this).hasClass('customvideoplay')) {
                        var docid = $(this).attr('data-document-id');
                        $.ajax({
                            type: 'POST',
                            url: Routing.generate('videoplaypage'),
                            contentType: 'application/x-www-form-urlencoded',
                            data: {document_id: docid, question_id: questionid},
                            success: function (result, status, xhr) {
                                res = JSON.parse(result);
                                $('.custom-video-play').append(res);
                                $("#videoplayModal").modal();
                                is_activable(campaignid);
                            },
                            error: function (xhr, status, error) {
                                console.log(status);
                            }
                        });
                    } else {
                        //else if clicked on the video upload icon load video upload modal
                        $.ajax({
                            type: 'POST',
                            url: Routing.generate('editquestionpage'),
                            contentType: 'application/x-www-form-urlencoded',
                            data: {question_id: questionid, flag: 'editvideo'},
                            success: function (result, status, xhr) {
                                res = JSON.parse(result);
                                $('.custom-modal').append(res['html']);
                                $("#videoModal").modal();
                                $(this).find('.custom-modal').find('.insert-btn').attr('data-question-id', questionid);
                                $(".video-div").removeClass('active');
                                $(this).find('.custom-modal').find('.insert-btn').prop("disabled", true);
                                is_activable(campaignid);
                            },
                            error: function (xhr, status, error) {
                                console.log(status);
                            }
                        });
                    }
                });
                is_activable(campaignid);
                
                $(".responsetype").chosen({
                    display_selected_options: false,               
                    placeholder_text_multiple: 'Select responsetype',
                    no_results_text: "Oops, no responsetype found!"
                });
                $(".thinktime").chosen({
                    display_selected_options: false, 
                    width: "100%",
                    placeholder_text_multiple: 'Select thinktime',
                    no_results_text: "Oops, no thinktime found!" 
                });
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });
    }); //add question ends here

    //video upload/play
    $('.videolinkcustom').click(function (e) {
        e.preventDefault();
        var questionid = $(this).attr('data-question-id');
        if ($(this).hasClass('customvideoplay')) {
            var docid = $(this).attr('data-document-id');
            $.ajax({
                type: 'POST',
                url: Routing.generate('videoplaypage'),
                contentType: 'application/x-www-form-urlencoded',
                data: {document_id: docid, question_id: questionid},
                success: function (result, status, xhr) {
                    res = JSON.parse(result);
                    $('.custom-video-play').append(res);
                    $("#videoplayModal").modal();
                    is_activable(campaignid);
                },
                error: function (xhr, status, error) {
                    console.log(status);
                }
            });
        } else {
            $.ajax({
                type: 'POST',
                url: Routing.generate('editquestionpage'),
                contentType: 'application/x-www-form-urlencoded',
                data: {question_id: questionid, flag: 'editvideo'},
                success: function (result, status, xhr) {
                    res = JSON.parse(result);
                    $('.custom-modal').append(res['html']);
                    $("#videoModal").modal();
                    $(this).find('.custom-modal').find('.insert-btn').attr('data-question-id', questionid);
                    $(".video-div").removeClass('active');
                    $(this).find('.custom-modal').find('.insert-btn').prop("disabled", true);
                    is_activable(campaignid);
                },
                error: function (xhr, status, error) {
                    console.log(status);
                }
            });
        }
    });

    //remove question
    $(document).on('click', '.btn_remove', function (e) {
        e.preventDefault();
        var button_id = $(this).attr("id");
        $.ajax({
            type: 'POST',
            url: Routing.generate('deletequestionpage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {question_id: button_id},
            success: function (result, status, xhr) {
                res = JSON.parse(result);
                if (res == 'success') {
                    $("#dynamic_field .row" + button_id).remove();
                }
                manage_order(campaignid);
                is_activable(campaignid);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });

    });

    //edit israndom
    $(document).on('click', '.random-btn', function () {
        if (!$(this).children().hasClass('random-active')) {
            $(this).children().attr('src', '/noformcomponentproject/web/bundles/app/img/random-active.png');
            $(this).children().addClass('random-active');
            $(this).css("background-color", "#3C74B8");
            var value = 1;
        } else {
            $(this).children().attr('src', '/noformcomponentproject/web/bundles/app/img/random.png');
            $(this).children().removeClass('random-active');
            $(this).css("background-color", "#E3E5EE");
            var value = 0;
        }

        var questionid = $(this).attr("data-id");
        $.ajax({
            type: 'POST',
            url: Routing.generate('editisrandom'),
            contentType: 'application/x-www-form-urlencoded',
            data: {question_id: questionid, value: value},
            success: function (result, status, xhr) {
                res = JSON.parse(result);
                is_activable(campaignid);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });

    });

    //edit question name
    $(document).on('input', '.question_name', function () {
        var value = $(this).val();
        var questionid = $(this).attr("data-id");
        $.ajax({
            type: 'POST',
            url: Routing.generate('editquestionpage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {question_id: questionid, value: value, flag: 'editquestionname'},
            success: function (result, status, xhr) {
                res = JSON.parse(result);
                is_activable(campaignid);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });
    });

    //edit responsetype
    $(document).on('change', '.responsetype', function () {
        var value = $(this).val();
        var questionid = $(this).attr("data-id");
        $.ajax({
            type: 'POST',
            url: Routing.generate('editquestionpage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {question_id: questionid, value: value, flag: 'editresponsetype'},
            success: function (result, status, xhr) {
                res = JSON.parse(result);
                if (value == 4) {
                    $(".row" + questionid + " .maxtake").remove();
                    $(".row" + questionid + " .choicewidget").append(res);

                    //choice value
                    $(document).on('input', ".row" + questionid + " .choice", function () {
                        var value = $(this).val();
                        var choiceid = $(this).attr("data-id");
                        edit_choice_value(choiceid, value, campaignid);
                    });
                    //hasmultiple answer
                    $(document).on('click', ".row" + questionid + " .hasmultiple", function () {
                        var value = $(this).attr("value");
                        edit_hasmultiple(questionid, value, campaignid);
                    });
                    //add choice
                    $(".row" + questionid + " #addoption").click(function (e) {
                        e.preventDefault();
                        add_choice(questionid, campaignid);
                    });
                    //delete choice
                    $(document).on('click', ".row" + questionid + " .btn_remove_choice", function (e) {
                        e.preventDefault();
                        var button_id = $(this).attr("id");
                        delete_choice(questionid, button_id, campaignid);
                    });
                } else if (value == 3) {
                    //if responsetype is video
                    $(".row" + questionid + " .choicewrap").remove();
                    $(".row" + questionid + " .maxtakewidget").append(res);
                } else {
                    //if responsetype is text or date
                    //delete all choices and choice form and maxtake widget
                    $.ajax({
                        type: 'POST',
                        url: Routing.generate('deletechoicepage'),
                        contentType: 'application/x-www-form-urlencoded',
                        data: {question_id: questionid, flag: 'deleteallchoices'},
                        success: function (result, status, xhr) {
                            res = JSON.parse(result);
                            if (res == 'success') {
                                $(".row" + questionid + " .choicewrap").remove();
                                $(".row" + questionid + " .maxtake").remove();
                            }
                        },
                        error: function (xhr, status, error) {
                            console.log(status);
                        }
                    });
                }
                is_activable(campaignid);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });

    }); //edit responsetype ends here

    //edit minduration
    $(document).on('input', '#minduration', function () {
        var value = $(this).val();
        var questionid = $(this).attr("data-id");
        $.ajax({
            type: 'POST',
            url: Routing.generate('editquestionpage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {question_id: questionid, value: value, flag: 'editminduration'},
            success: function (result, status, xhr) {
                res = JSON.parse(result);
                is_activable(campaignid);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });

    });

    //edit maxduration
    $(document).on('input', '#maxduration', function () {
        var value = $(this).val();
        var questionid = $(this).attr("data-id");
        $.ajax({
            type: 'POST',
            url: Routing.generate('editquestionpage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {question_id: questionid, value: value, flag: 'editmaxduration'},
            success: function (result, status, xhr) {
                res = JSON.parse(result);
                is_activable(campaignid);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });

    });

    //edit thinktime
    $(document).on('change', '.thinktime', function () {
        var value = $(this).val();
        var questionid = $(this).attr("data-id");
        $.ajax({
            type: 'POST',
            url: Routing.generate('editquestionpage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {question_id: questionid, value: value, flag: 'editthinktime'},
            success: function (result, status, xhr) {
                res = JSON.parse(result);
                is_activable(campaignid);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });

    });

    //edit maxtake
    $(document).on('change', '.maxtake', function () {
        var value = $(this).val();
        var questionid = $(this).attr("data-id");
        $.ajax({
            type: 'POST',
            url: Routing.generate('editquestionpage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {question_id: questionid, value: value, flag: 'editmaxtake'},
            success: function (result, status, xhr) {
                res = JSON.parse(result);
                is_activable(campaignid);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });

    });


    //evaluation criteria   
    $('.add-criteria').toggleClass('hidden', $('.criterias .criteria').length >= 10);
    //add criterion
    $('.add-criteria').click(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: Routing.generate('addcriterionpage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {campaign_id : campaignid},
            success: function (result, status, xhr) {
                var res = JSON.parse(result);
                $('.criterias').append(res);
                $('.add-criteria').toggleClass('hidden', $('.criterias .criteria').length >= 10);
                manage_criterion_order();
                is_activable(campaignid);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });
    });

    //remove criterion
    $(document).on('click', '.remove_criterion', function (e) {
        e.preventDefault();
        var button_id = $(this).attr("id");
        $.ajax({
            type: 'POST',
            url: Routing.generate('deletecriterionpage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {criterion_id: button_id},
            success: function (result, status, xhr) {
                res = JSON.parse(result);
                if (res == 'success') {
                    $(".criterias .row" + button_id).remove();
                    $('.add-criteria').toggleClass('hidden', $('.criterias .criteria').length >= 10);
                }
                manage_criterion_order();
                is_activable(campaignid);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });

    });

    //edit criterion
    $(document).on('input', '.criteria-label', function () {
        var value = $(this).val();
        var criterionid = $(this).attr("data-criterion-id");
        $.ajax({
            type: 'POST',
            url: Routing.generate('editcriterionpage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {criterion_id: criterionid, value: value},
            success: function (result, status, xhr) {
                res = JSON.parse(result);
                is_activable(campaignid);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });

    });

});