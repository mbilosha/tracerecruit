//validation check to activate or deactivate the submit button
function is_activable(campaignid) {
    $.ajax({
        type: 'POST',
        url: Routing.generate('isactivable'),
        contentType: 'application/x-www-form-urlencoded',
        data: {campaign_id: campaignid},
        success: function (result, status, xhr) {
            res = JSON.parse(result);
            if (res == 'success') {
                $('.activate').hide().prev("input[disabled]").prop("disabled", false);
            } else if (res == 'error') {
                $('.submit').prop("disabled", true);
            }
        },
        error: function (xhr, status, error) {
            console.log(status);
        }
    });
}

$(document).ready(function () {
    var campaignid = $('.table-responsive').attr('data-campaign-id');
    is_activable(campaignid);
    
    $('.access_radio').click(function () {
        var value = $(this).val();
        var campaignclientid = $(this).attr('data-campaignclient-id');
        $.ajax({
            type: 'POST',
            url: Routing.generate('editvisibilitypage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {campaignclient_id: campaignclientid, value: value},
            success: function (result, status, xhr) {
                var res = JSON.parse(result);
                is_activable(campaignid);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });
    });

    $('.access_evaluate').click(function () {
        var value = $(this).val();
        var campaignclientid = $(this).attr('data-campaignclient-id');
        $.ajax({
            type: 'POST',
            url: Routing.generate('editevaluationpage'),
            contentType: 'application/x-www-form-urlencoded',
            data: {campaignclient_id: campaignclientid, value: value},
            success: function (result, status, xhr) {
                var res = JSON.parse(result);
                is_activable(campaignid);
            },
            error: function (xhr, status, error) {
                console.log(status);
            }
        });
    });
});