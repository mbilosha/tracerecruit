<?php

namespace TraceBundle\Service;

use Doctrine\ORM\EntityManager;
use TraceBundle\Entity\Client;

class TenantManager {

    protected $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
 
    public function getTenantId(Client $client){
        
        $tenantid = $client->getTenant()->getId();
        return $tenantid;
    }
}