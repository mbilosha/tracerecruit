<?php

namespace TraceBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class GetUniqueToken {

    protected $em;
    protected $container;

    public function __construct(EntityManager $em, Container $container) {
        $this->em = $em;
        $this->container = $container;
    }

    public function getToken() {

        $i = 0;
        $placeholder = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
        $chars = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
                       'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'a', 'b', 'c', 'd');
        $month = date('n');
        for ($i = 0; $i < 4; $i++) {
            $token = bin2hex(openssl_random_pseudo_bytes(3));
            $replace[$month] = array_slice($chars, $month, 16);
            $uniquetoken = str_replace($placeholder, $replace[$month], $token);
            $applicant = $this->em->getRepository('TraceBundle:Applicant')->findOneBy(array('uniqueinvitecode' => $uniquetoken));

            if ($applicant && $i < 3) {
                continue;
            } else if ($applicant && $i == 3) {
                return false;
            }
            return $uniquetoken;
        }
    }

}
