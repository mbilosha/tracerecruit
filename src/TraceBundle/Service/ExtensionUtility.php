<?php

namespace TraceBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use TraceBundle\Entity\Applicant;
use DateTimeImmutable;

class ExtensionUtility {

    protected $em;
    protected $container;
    protected $val;

    public function __construct(EntityManager $em, Container $container) {
        $this->em = $em;
        $this->container = $container;
    }

    public function getExtension(Applicant $applicant) {
        $currentdate = new DateTimeImmutable();
        $expirydate = new DateTimeImmutable($applicant->getExpirydate()->format('Y-m-d H:i:s'));
        $extensionlastdate = $expirydate->modify('+15 days');

        if($applicant->getIsextensionrequested() == false && $applicant->getIsextended() == null && $expirydate > $currentdate){
            //continue interview
            $val = 1;
        }
        elseif($applicant->getIsextensionrequested() == false && $expirydate < $currentdate && $extensionlastdate > $currentdate){
//            show the extension request form
//	on submission update isextensionrequested to true
//	update extensionrequesttime to current time
            $val = 2;
        }
        elseif($applicant->getIsextensionrequested() == false && $expirydate < $currentdate && $extensionlastdate < $currentdate){
//            do not show the extension request form
//            instead show a message saying your interview has expired
            $val = 6;
        }
        elseif($applicant->getIsextensionrequested() == true && $applicant->getIsextended() == null && $expirydate < $currentdate){
            //show message saying your extension request is pending
            $val = 3;
        }
        elseif($applicant->getIsextensionrequested() == true && $applicant->getIsextended() == false){
            //show message saying your extension request has been rejected
            $val = 4;
        }
        elseif($applicant->getIsextensionrequested() == true && $applicant->getIsextended() == true && $expirydate < $currentdate){
            //you will not be allowed to continue
            $val = 5;
        }
        elseif($applicant->getIsextensionrequested() == true && $applicant->getIsextended() == true && $expirydate > $currentdate){
            //continue interview
            $val = 1;
        }
        
        return $val;
    }

}
