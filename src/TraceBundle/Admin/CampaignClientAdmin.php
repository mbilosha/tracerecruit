<?php

namespace TraceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use TraceBundle\Entity\Client;
use TraceBundle\Entity\Campaign;
use TraceBundle\Entity\CampaignClient;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class CampaignClientAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                    ->tab('Original fields')
                    ->with('Original fields', ['class' => 'col-md-9'])
                        ->add('visibility', IntegerType::class)
                        ->add('evaluation', CheckboxType::class)
                    ->end()
                    ->end();
        $formMapper
                    ->tab('Related Entities')
                    ->with('Related Entities', ['class' => 'col-md-9'])
                        ->add('client', EntityType::class, [
                            'class' => Client::class,
                            'choice_label' => 'email',
                        ])
                        ->add('campaign', EntityType::class, [
                            'class' => Campaign::class,
                            'choice_label' => 'campaigntitle',
                        ])
                    ->end()
                    ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper->add('visibility')
                       ->add('evaluation')
                       ->add('client', null, [], EntityType::class, [
                            'class'    => Client::class,
                            'choice_label' => 'email',
                        ]) 
                       ->add('campaign', null, [], EntityType::class, [
                            'class'    => Campaign::class,
                            'choice_label' => 'campaigntitle',
                        ]); 
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                    ->add('visibility')
                    ->add('evaluation')
                    ->addIdentifier('client.email')
                    ->addIdentifier('campaign.campaigntitle');
    }
    
    public function toString($object) {
        return $object instanceof CampaignClient ? $object->getVisibility() : 'CampaignClient';
    }

}
