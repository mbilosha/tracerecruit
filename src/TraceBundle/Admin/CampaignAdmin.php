<?php

namespace TraceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use TraceBundle\Entity\Campaign;
use TraceBundle\Entity\Language;
use TraceBundle\Entity\Tenant;
use TraceBundle\Entity\Client;
use TraceBundle\Entity\Campaignstatus;;

class CampaignAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                    ->tab('Original fields')
                    ->with('Original fields', ['class' => 'col-md-9'])
                        ->add('campaigntitle', TextType::class)
                        ->add('location', TextType::class)
                        ->add('jobreference', TextType::class)
                        ->add('hasintro', CheckboxType::class)
                        ->add('hasoutro', CheckboxType::class)
                        ->add('deadlinetoanswer', IntegerType::class)
                        ->add('ispublic', CheckboxType::class)
                        ->add('prerecwelcomemsg', TextareaType::class)
                        ->add('description', TextareaType::class)
                        ->add('prerecthankyoumsg', TextareaType::class)
                        ->add('livewelcomemsg', TextareaType::class)
                        ->add('livethankyoumsg', TextareaType::class)
                        ->add('isprerecorded', CheckboxType::class)
                        ->add('islive', CheckboxType::class)
                    ->end()
                    ->end();
        $formMapper
                    ->tab('Related Entities')
                    ->with('Related Entities', ['class' => 'col-md-9'])
                        ->add('language', EntityType::class, [
                            'class' => Language::class,
                            'choice_label' => 'value',
                        ])
                        ->add('tenant', EntityType::class, [
                            'class' => Tenant::class,
                            'choice_label' => 'name',
                        ])
                        ->add('campaignstatus', EntityType::class, [
                            'class' => Campaignstatus::class,
                            'choice_label' => 'value',
                        ])
                        ->add('client', EntityType::class, [
                            'class' => Client::class,
                            'choice_label' => 'email',
                        ])
                    ->end()
                    ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper->add('campaigntitle')
                       ->add('location')
                       ->add('jobreference')
                       ->add('isprerecorded')
                       ->add('islive')
                       ->add('language', null, [], EntityType::class, [
                            'class'    => Language::class,
                            'choice_label' => 'value',
                        ]) 
                       ->add('tenant', null, [], EntityType::class, [
                            'class'    => Tenant::class,
                            'choice_label' => 'name',
                        ]) 
                       ->add('campaignstatus', null, [], EntityType::class, [
                            'class'    => Campaignstatus::class,
                            'choice_label' => 'value',
                        ]) 
                       ->add('client', null, [], EntityType::class, [
                            'class'    => Client::class,
                            'choice_label' => 'email',
                        ]); 
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                    ->addIdentifier('campaigntitle')
                    ->addIdentifier('location')
                    ->add('description')
                    ->add('campaignstatus.value');
    }
    
    public function toString($object) {
        return $object instanceof Campaign ? $object->getCampaigntitle() : 'Campaign';
    }

}
