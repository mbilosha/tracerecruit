<?php

namespace TraceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use TraceBundle\Entity\Campaign;
use TraceBundle\Entity\Campaignliveresponse;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class CampaignliveresponseAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                    ->tab('Original fields')
                    ->with('Original fields', ['class' => 'col-md-9'])
                        ->add('liveurl', UrlType::class)
                        ->add('interviewduration', IntegerType::class)
                        ->add('interviewstarttime', DateTimeType::class)
                        ->add('interviewendtime', DateTimeType::class)
                    ->end()
                    ->end();
        $formMapper
                    ->tab('Related Entities')
                    ->with('Related Entities', ['class' => 'col-md-9'])
                        ->add('campaign', EntityType::class, [
                            'class' => Campaign::class,
                            'choice_label' => 'campaigntitle',
                        ])
                    ->end()
                    ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                       ->add('liveurl')
                       ->add('campaign', null, [], EntityType::class, [
                            'class'    => Campaign::class,
                            'choice_label' => 'campaigntitle',
                        ]); 
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                    ->add('liveurl')
                    ->add('interviewduration')
                    ->addIdentifier('campaign.campaigntitle');
    }
    
    public function toString($object) {
        return $object instanceof Campaignliveresponse ? $object->getLiveurl() : 'Campaignliveresponse';
    }

}
