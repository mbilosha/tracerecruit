<?php

namespace TraceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use TraceBundle\Entity\Salutation;
use TraceBundle\Entity\Tenant;
use TraceBundle\Entity\Client;

class ClientAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                    ->tab('Original fields')
                    ->with('Original fields', ['class' => 'col-md-9'])
                        ->add('firstname', TextType::class)
                        ->add('lastname', TextType::class)
                        ->add('username', TextType::class)
                        ->add('plainPassword', TextType::class)
                        ->add('role', TextType::class)
                        ->add('email', EmailType::class)
                        ->add('isenabled', CheckboxType::class)
                        ->add('isblacklisted', CheckboxType::class)
                        ->add('blacklistreason', TextType::class)
                        ->add('profilepicurl', UrlType::class)
                        ->add('createdon', DateTimeType::class)
                        ->add('updatedon', DateTimeType::class)
                    ->end()
                    ->end();
        $formMapper
                    ->tab('Related Entities')
                    ->with('Related Entities', ['class' => 'col-md-9'])
                        ->add('salutation', EntityType::class, [
                            'class' => Salutation::class,
                            'choice_label' => 'value',
                        ])
                        ->add('tenant', EntityType::class, [
                            'class' => Tenant::class,
                            'choice_label' => 'name',
                        ])
                    ->end()
                    ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper->add('email')
                       ->add('salutation', null, [], EntityType::class, [
                            'class'    => Salutation::class,
                            'choice_label' => 'value',
                        ]) 
                       ->add('tenant', null, [], EntityType::class, [
                            'class'    => Tenant::class,
                            'choice_label' => 'name',
                        ]); 
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                    ->add('salutation.value')
                    ->add('firstname')
                    ->add('lastname')
                    ->addIdentifier('email')
                    ->add('tenant.name');
    }
    
    public function toString($object) {
        return $object instanceof Client ? $object->getFirstname() : 'Client';
    }

}
