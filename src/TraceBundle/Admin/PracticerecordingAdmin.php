<?php

namespace TraceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use TraceBundle\Entity\Applicant;
use TraceBundle\Entity\Practicerecording;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class PracticerecordingAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                    ->tab('Original fields')
                    ->with('Original fields', ['class' => 'col-md-9'])
                        ->add('payload', TextType::class)
                        ->add('url', UrlType::class)
                        ->add('createdon', DateTimeType::class)
                    ->end()
                    ->end();
        $formMapper
                    ->tab('Related Entities')
                    ->with('Related Entities', ['class' => 'col-md-9'])
                        ->add('applicant', EntityType::class, [
                            'class' => Applicant::class,
                            'choice_label' => 'email',
                        ])
                    ->end()
                    ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                       ->add('payload')
                       ->add('url')
                       ->add('createdon')
                       ->add('applicant', null, [], EntityType::class, [
                            'class'    => Applicant::class,
                            'choice_label' => 'email',
                        ]); 
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                    ->addIdentifier('url')
                    ->add('payload')
                    ->add('createdon')
                    ->addIdentifier('applicant.email');
    }
    
    public function toString($object) {
        return $object instanceof Practicerecording ? $object->getUrl() : 'Practicerecording';
    }

}
