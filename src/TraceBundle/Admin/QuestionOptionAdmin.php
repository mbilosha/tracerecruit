<?php

namespace TraceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use TraceBundle\Entity\Question;
use TraceBundle\Entity\QuestionOption;

class QuestionOptionAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                    ->tab('Original fields')
                    ->with('Original fields', ['class' => 'col-md-9'])
                        ->add('value', TextType::class)
                    ->end()
                    ->end();
        $formMapper
                    ->tab('Related Entities')
                    ->with('Related Entities', ['class' => 'col-md-9'])
                        ->add('question', EntityType::class, [
                            'class' => Question::class,
                            'choice_label' => 'question',
                        ])
                    ->end()
                    ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper->add('value')
                       ->add('question', null, [], EntityType::class, [
                            'class'    => Question::class,
                            'choice_label' => 'question',
                        ]); 
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                    ->addIdentifier('value')
                    ->add('question.question');
    }
    
    public function toString($object) {
        return $object instanceof QuestionOption ? $object->getValue() : 'QuestionOption';
    }

}
