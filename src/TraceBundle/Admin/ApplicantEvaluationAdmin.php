<?php

namespace TraceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use TraceBundle\Entity\Applicant;
use TraceBundle\Entity\Evaluationcriteria;
use TraceBundle\Entity\ApplicantEvaluation;

class ApplicantEvaluationAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                    ->tab('Original fields')
                    ->with('Original fields', ['class' => 'col-md-9'])
                        ->add('value', NumberType::class)
                    ->end()
                    ->end();
        $formMapper
                    ->tab('Related Entities')
                    ->with('Related Entities', ['class' => 'col-md-9'])
                        ->add('applicant', EntityType::class, [
                            'class' => Applicant::class,
                            'choice_label' => 'firstname',
                        ])
                        ->add('evaluationcriterion', EntityType::class, [
                            'class' => Evaluationcriteria::class,
                            'choice_label' => 'criterion',
                        ])
                    ->end()
                    ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper->add('value')
                       ->add('applicant', null, [], EntityType::class, [
                            'class'    => Applicant::class,
                            'choice_label' => 'firstname',
                        ]) 
                       ->add('evaluationcriterion', null, [], EntityType::class, [
                            'class'    => Evaluationcriteria::class,
                            'choice_label' => 'criterion',
                        ]); 
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                    ->add('value')
                    ->addIdentifier('applicant.uniqueinvitecode')
                    ->add('evaluationcriterion.criterion');
    }
    
    public function toString($object) {
        return $object instanceof ApplicantEvaluation ? $object->getValue() : 'ApplicantEvaluation';
    }

}
