<?php

namespace TraceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use TraceBundle\Entity\Tenant;
use TraceBundle\Entity\Campaign;
use TraceBundle\Entity\Evaluationcriteria;

class EvaluationcriteriaAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                    ->tab('Original fields')
                    ->with('Original fields', ['class' => 'col-md-9'])
                        ->add('criterion', TextType::class)
                    ->end()
                    ->end();
        $formMapper
                    ->tab('Related Entities')
                    ->with('Related Entities', ['class' => 'col-md-9'])
                        ->add('tenant', EntityType::class, [
                            'class' => Tenant::class,
                            'choice_label' => 'name',
                        ])
                        ->add('campaign', EntityType::class, [
                            'class' => Campaign::class,
                            'choice_label' => 'cmpaigntitle',
                        ])
                    ->end()
                    ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper->add('criterion')
                       ->add('tenant', null, [], EntityType::class, [
                            'class'    => Tenant::class,
                            'choice_label' => 'name',
                        ]) 
                       ->add('campaign', null, [], EntityType::class, [
                            'class'    => Campaign::class,
                            'choice_label' => 'cmpaigntitle',
                        ]); 
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                    ->addIdentifier('criterion')
                    ->add('tenant.name')
                    ->add('campaign.cmpaigntitle');
    }
    
    public function toString($object) {
        return $object instanceof Evaluationcriteria ? $object->getCriterion() : 'Evaluationcriteria';
    }

}
