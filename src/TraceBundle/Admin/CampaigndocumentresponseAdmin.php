<?php

namespace TraceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use TraceBundle\Entity\Campaigndocumentresponse;
use TraceBundle\Entity\Campaigndocument;
use TraceBundle\Entity\Applicant;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class CampaigndocumentresponseAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                    ->tab('Original fields')
                    ->with('Original fields', ['class' => 'col-md-9'])
                        ->add('value', TextType::class)
                        ->add('filename', TextType::class)
                        ->add('isuploaded', CheckboxType::class)
                    ->end()
                    ->end();
        $formMapper
                    ->tab('Related Entities')
                    ->with('Related Entities', ['class' => 'col-md-9'])
                        ->add('campaigndocument', EntityType::class, [
                            'class' => Campaigndocument::class,
                            'choice_label' => 'value',
                        ])
                        ->add('applicant', EntityType::class, [
                            'class' => Applicant::class,
                            'choice_label' => 'email',
                        ])
                    ->end()
                    ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                       ->add('value')
                       ->add('isuploaded')
                       ->add('campaigndocument', null, [], EntityType::class, [
                            'class'    => Campaigndocument::class,
                            'choice_label' => 'value',
                        ]) 
                       ->add('applicant', null, [], EntityType::class, [
                            'class'    => Applicant::class,
                            'choice_label' => 'email',
                        ]); 
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                    ->addIdentifier('value')
                    ->add('campaigndocument.value')
                    ->add('applicant.email');
    }
    
    public function toString($object) {
        return $object instanceof Campaigndocumentresponse ? $object->getValue() : 'Campaigndocumentresponse';
    }

}
