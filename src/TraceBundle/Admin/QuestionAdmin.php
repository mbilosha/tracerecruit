<?php

namespace TraceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use TraceBundle\Entity\Campaign;
use TraceBundle\Entity\Tenant;
use TraceBundle\Entity\Question;

class QuestionAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                    ->tab('Original fields')
                    ->with('Original fields', ['class' => 'col-md-9'])
                        ->add('question', TextareaType::class)
                        ->add('isvideo', CheckboxType::class)
                        ->add('isimage', CheckboxType::class)
                        ->add('sortorder', IntegerType::class)
                        ->add('israndom', CheckboxType::class)
                        ->add('minduration', IntegerType::class)
                        ->add('maxduration', IntegerType::class)
                        ->add('thinktime', IntegerType::class)
                        ->add('maxtake', IntegerType::class)
                        ->add('responsetype', IntegerType::class)
                        ->add('hasmultipleanswer', CheckboxType::class)
                    ->end()
                    ->end();
        $formMapper
                    ->tab('Related Entities')
                    ->with('Related Entities', ['class' => 'col-md-9'])
                        ->add('campaign', EntityType::class, [
                            'class' => Campaign::class,
                            'choice_label' => 'campaigntitle',
                        ])
                        ->add('tenant', EntityType::class, [
                            'class' => Tenant::class,
                            'choice_label' => 'name',
                        ])
                    ->end()
                    ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                       ->add('isvideo')
                       ->add('isimage')
                       ->add('maxtake')
                       ->add('responsetype')
                       ->add('hasmultipleanswer')
                       ->add('campaign', null, [], EntityType::class, [
                            'class'    => Campaign::class,
                            'choice_label' => 'campaigntitle',
                        ]) 
                       ->add('tenant', null, [], EntityType::class, [
                            'class'    => Tenant::class,
                            'choice_label' => 'name',
                        ]); 
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                    ->addIdentifier('question')
                    ->add('isvideo')
                    ->add('isimage')
                    ->add('responsetype')
                    ->add('campaign.campaigntitle')
                    ->add('tenant.name');
    }
    
    public function toString($object) {
        return $object instanceof Question ? $object->getQuestion() : 'Question';
    }

}
