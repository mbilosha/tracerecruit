<?php

namespace TraceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use TraceBundle\Entity\Client;
use TraceBundle\Entity\Campaign;
use TraceBundle\Entity\Campaignsharing;

class CampaignsharingAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                    ->tab('Original fields')
                    ->with('Original fields', ['class' => 'col-md-9'])
                        ->add('isvisibletoall', CheckboxType::class)
                        ->add('canevaluate', CheckboxType::class)
                    ->end()
                    ->end();
        $formMapper
                    ->tab('Related Entities')
                    ->with('Related Entities', ['class' => 'col-md-9'])
                        ->add('client', EntityType::class, [
                            'class' => Client::class,
                            'choice_label' => 'email',
                        ])
                        ->add('campaign', EntityType::class, [
                            'class' => Campaign::class,
                            'choice_label' => 'campaigntitle',
                        ])
                    ->end()
                    ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper->add('isvisibletoall')
                       ->add('canevaluate')
                       ->add('client', null, [], EntityType::class, [
                            'class'    => Client::class,
                            'choice_label' => 'email',
                        ]) 
                       ->add('campaign', null, [], EntityType::class, [
                            'class'    => Campaign::class,
                            'choice_label' => 'campaigntitle',
                        ]); 
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                    ->add('isvisibletoall')
                    ->add('canevaluate')
                    ->addIdentifier('client.email')
                    ->addIdentifier('campaign.campaigntitle');
    }
    
    public function toString($object) {
        return $object instanceof Campaignsharing ? $object->getIsvisibletoall() : 'Campaignsharing';
    }

}
