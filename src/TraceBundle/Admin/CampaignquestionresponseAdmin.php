<?php

namespace TraceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use TraceBundle\Entity\Question;
use TraceBundle\Entity\Applicant;
use TraceBundle\Entity\Campaignquestionresponse;

class CampaignquestionresponseAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                    ->tab('Original fields')
                    ->with('Original fields', ['class' => 'col-md-9'])
                        ->add('value', TextType::class)
                        ->add('isanswered', CheckboxType::class)
                        ->add('isviewed', CheckboxType::class)
                    ->end()
                    ->end();
        $formMapper
                    ->tab('Related Entities')
                    ->with('Related Entities', ['class' => 'col-md-9'])
                        ->add('question', EntityType::class, [
                            'class' => Question::class,
                            'choice_label' => 'question',
                        ])
                        ->add('applicant', EntityType::class, [
                            'class' => Applicant::class,
                            'choice_label' => 'firstname',
                        ])
                    ->end()
                    ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper->add('isviewed')
                       ->add('isanswered')
                       ->add('question', null, [], EntityType::class, [
                            'class'    => Question::class,
                            'choice_label' => 'question',
                        ]) 
                       ->add('applicant', null, [], EntityType::class, [
                            'class'    => Applicant::class,
                            'choice_label' => 'uniqueinvitecode',
                        ]); 
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                    ->addIdentifier('value')
                    ->add('isanswered')
                    ->add('isviewed')
                    ->addIdentifier('question.question')
                    ->add('applicant.firstname');
    }
    
    public function toString($object) {
        return $object instanceof Campaignquestionresponse ? $object->getValue() : 'Campaignquestionresponse';
    }

}
