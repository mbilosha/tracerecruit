<?php

namespace TraceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use TraceBundle\Entity\Applicant;
use TraceBundle\Entity\Comment;
use TraceBundle\Entity\Client;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class CommentAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                    ->tab('Original fields')
                    ->with('Original fields', ['class' => 'col-md-9'])
                        ->add('value', TextType::class)
                        ->add('commentedon', DateTimeType::class)
                    ->end()
                    ->end();
        $formMapper
                    ->tab('Related Entities')
                    ->with('Related Entities', ['class' => 'col-md-9'])
                        ->add('client', EntityType::class, [
                            'class' => Client::class,
                            'choice_label' => 'email',
                        ])
                        ->add('applicant', EntityType::class, [
                            'class' => Applicant::class,
                            'choice_label' => 'email',
                        ])
                    ->end()
                    ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                       ->add('value')
                       ->add('commentedon')
                       ->add('client', null, [], EntityType::class, [
                            'class'    => Client::class,
                            'choice_label' => 'email',
                        ]) 
                       ->add('applicant', null, [], EntityType::class, [
                            'class'    => Applicant::class,
                            'choice_label' => 'email',
                        ]); 
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                    ->addIdentifier('value')
                    ->add('commentedon')
                    ->add('client.email')
                    ->add('applicant.email');
    }
    
    public function toString($object) {
        return $object instanceof Comment ? $object->getValue() : 'Comment';
    }

}
