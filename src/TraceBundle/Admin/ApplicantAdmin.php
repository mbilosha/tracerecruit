<?php

namespace TraceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use TraceBundle\Entity\Campaign;
use TraceBundle\Entity\Tenant;
use TraceBundle\Entity\Applicantstatus;
use TraceBundle\Entity\Interviewtype;
use TraceBundle\Entity\Applicant;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class ApplicantAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                    ->tab('Original fields')
                    ->with('Original fields', ['class' => 'col-md-9'])
                        ->add('firstname', TextType::class)
                        ->add('lastname', TextType::class)
                        ->add('email', EmailType::class)
                        ->add('phonenumber', TelType::class)
                        ->add('invitedate', DateTimeType::class)
                        ->add('inviteurl', UrlType::class)
                        ->add('uniqueinvitecode', TextType::class)
                        ->add('expirydate', DateTimeType::class)
                        ->add('extensionrequesttime', DateTimeType::class)
                        ->add('isextended', CheckboxType::class)
                        ->add('isextensionrequested', CheckboxType::class)
                        ->add('isuniquecodeerror', CheckboxType::class)
                        ->add('allowduplicate', CheckboxType::class)
                        ->add('statusupdatedon', DateTimeType::class)
                        ->add('createdon', DateTimeType::class)
                        ->add('updatedon', DateTimeType::class)
                        ->add('isansweredall', CheckboxType::class)
                        ->add('isuploadedalldocs', CheckboxType::class)
                        ->add('reason', TextareaType::class)
                        ->add('averagerating', NumberType::class)
                        ->add('liveinterviewduration', IntegerType::class)
                    ->end()
                    ->end();
        $formMapper
                    ->tab('Related Entities')
                    ->with('Related Entities', ['class' => 'col-md-9'])
                        ->add('campaign', EntityType::class, [
                            'class' => Campaign::class,
                            'choice_label' => 'campaigntitle',
                        ])
                        ->add('tenant', EntityType::class, [
                            'class' => Tenant::class,
                            'choice_label' => 'name',
                        ])
                        ->add('applicantstatus', EntityType::class, [
                            'class' => Applicantstatus::class,
                            'choice_label' => 'value',
                        ])
                        ->add('interviewtype', EntityType::class, [
                            'class' => Interviewtype::class,
                            'choice_label' => 'value',
                        ])
                    ->end()
                    ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper->add('email')
                       ->add('campaign', null, [], EntityType::class, [
                            'class'    => Campaign::class,
                            'choice_label' => 'campaigntitle',
                        ]) 
                       ->add('tenant', null, [], EntityType::class, [
                            'class'    => Tenant::class,
                            'choice_label' => 'name',
                        ]) 
                       ->add('applicantstatus', null, [], EntityType::class, [
                            'class'    => Applicantstatus::class,
                            'choice_label' => 'value',
                        ]) 
                       ->add('interviewtype', null, [], EntityType::class, [
                            'class'    => Interviewtype::class,
                            'choice_label' => 'value',
                        ]); 
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                    ->addIdentifier('firstname')
                    ->add('lastname')
                    ->add('email')
                    ->add('interviewtype.value');
    }
    
    public function toString($object) {
        return $object instanceof Applicant ? $object->getFirstname() : 'Applicant';
    }

}
