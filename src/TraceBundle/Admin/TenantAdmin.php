<?php

namespace TraceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use TraceBundle\Entity\Language;
use TraceBundle\Entity\Tenant;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class TenantAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                    ->tab('Original fields')
                    ->with('Original fields', ['class' => 'col-md-9'])
                        ->add('name', TextType::class)
                        ->add('officialwebsiteurl', UrlType::class)
                        ->add('tenantsubdomainurl', UrlType::class)
                        ->add('logourl', UrlType::class)
                        ->add('createdon', DateTimeType::class)
                        ->add('updatedon', DateTimeType::class)
                    ->end()
                    ->end();
        $formMapper
                    ->tab('Related Entities')
                    ->with('Related Entities', ['class' => 'col-md-9'])
                        ->add('language', EntityType::class, [
                            'class' => Language::class,
                            'choice_label' => 'value',
                        ])
                    ->end()
                    ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                       ->add('name')
                       ->add('createdon')
                       ->add('updatedon')
                       ->add('language', null, [], EntityType::class, [
                            'class'    => Language::class,
                            'choice_label' => 'value',
                        ]);
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                    ->addIdentifier('name')
                    ->add('officialwebsiteurl')
                    ->add('tenantsubdomainurl')
                    ->add('logourl')
                    ->add('language.value');
    }
    
    public function toString($object) {
        return $object instanceof Tenant ? $object->getName() : 'Tenant';
    }

}
