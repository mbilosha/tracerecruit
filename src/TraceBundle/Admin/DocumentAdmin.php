<?php

namespace TraceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use TraceBundle\Entity\Tenant;
use TraceBundle\Entity\Documenttype;
use TraceBundle\Entity\Document;

class DocumentAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                    ->tab('Original fields')
                    ->with('Original fields', ['class' => 'col-md-9'])
                        ->add('documentname', TextType::class)
                    ->end()
                    ->end();
        $formMapper
                    ->tab('Related Entities')
                    ->with('Related Entities', ['class' => 'col-md-9'])
                        ->add('tenant', EntityType::class, [
                            'class' => Tenant::class,
                            'choice_label' => 'name',
                        ])
                        ->add('documenttype', EntityType::class, [
                            'class' => Documenttype::class,
                            'choice_label' => 'value',
                        ])
                    ->end()
                    ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper->add('documentname')
                       ->add('tenant', null, [], EntityType::class, [
                            'class'    => Tenant::class,
                            'choice_label' => 'name',
                        ]) 
                       ->add('documenttype', null, [], EntityType::class, [
                            'class'    => Documenttype::class,
                            'choice_label' => 'value',
                        ]); 
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                    ->addIdentifier('documentname')
                    ->add('tenant.name')
                    ->add('documenttype.value');
    }
    
    public function toString($object) {
        return $object instanceof Document ? $object->getDocumentname() : 'Document';
    }

}
