<?php

namespace TraceBundle\Tests\Controller\Api;

use GuzzleHttp\Client;
use TraceBundle\Test\ApiTestCase;

class RegistrationControllerTest extends ApiTestCase {

    public function testPOSTRegister() {
        $client = new Client(['base_uri' => "http://localhost:8000",
            'defaults' => [
                'exceptions' => false
                ]
            ]);

        $firstname = 'sachin' . rand(0, 999);
        $data = array(
            'firstname' => $firstname,
            'lastname' => 'jain',
            'email' => 'sachin' . rand(0, 999) . 'k2@gmail.com',
            'username' => 'sachin2k2',
            'plainPassword' => '1234',
        );
        
        $response = $client->post('/api/tokens',[
            'auth' => ['raj2k2', 1234]
        ]);
        
        $tokenres = json_decode($response->getBody());
        $token = $tokenres->token;
//        echo $token;die;
        
        $response = $client->post('/api/register', [
            'body' => json_encode($data),
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ],  
        ]);
        
        $this->assertEquals(201, $response->getStatusCode());
    }
    
    public function testGETTenant() {
        $client = new Client(['base_uri' => "http://localhost:8000",
            'defaults' => [
                'exceptions' => false
                ]
            ]);

        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1NDM0NzY0NTMsInVzZXJuYW1lIjoicmFqMmsyIiwiZXhwIjoxNTQ0NzcyNDUzfQ.VaggwV7t0IFiJ3w2G9qs2lrRkY-jMmELBn4yw37KWO78UK_pjWg6Jna5Twjm70q6pRVOEbvBqcf5nfLqs0lx9shE-ZizZCYlX9LpA5rR4qGdLpBkApRvBc0xFMkBU_DKkeJoUQqxL8XDeQkcrLtNNSDTPztywQ_xt7rvE09WsvB3tD80_ev9ODPMbLoBxEyAt0AuDa5aht59URtfCjy716AxiO_kBvefRge9i6xn4cV9zYS1W9qsDV87wRCG5eLjdPk3i8XQdEhyRma3zoNa5ppzNn3_BILp7UI_Nw3kDa_xQdUUhUGj9i8R5FReSHxqTKTFIZNYuuRBbIxsElys2o5hF96NSMVDZIw-MFVAnuodyd7_Awu7goVSbuNenAISWRpl9vn6pKTDvJLTl9YFBf5PVLVn_E2_Z92Q2NzwuEZ8wRWnYgGHPZEfCLwK4KtRYEOMqfKZSJO6JzHuV4_40HBYPSc0f4sDcywuHF5heoB67VUoM89qyjKhmfgny3ZaqMWmYpONFr7Q6ttrU_BKk-nfMH9yh76440nE3idwqPimjDckiRZZcqVJR2YeexQUxjRj1nokxTQ4EGsz_m-Qw0ocGVaraiFijQPRl6jxkw_8wf-yUYAvBxRLOmJeIaonnida9LqjZOo8d8nNEfFGCDD2zJcJyLdpEnYrulvRdfQ';
        
        $response = $client->post('/api/gettenant', [
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ],  
        ]);
        
        $this->assertEquals(200, $response->getStatusCode());
//        $this->debugResponse($response);
    }
    
    public function testPOSTValidationError() {
        $client = new Client(['base_uri' => "http://localhost:8000",
            'defaults' => [
                'exceptions' => false
                ]
            ]);
        
        $data = array(
            'lastname' => 'jain',
            'email' => 'sachin' . rand(0, 999) . 'k2@gmail.com',
            'username' => 'sachin2k2',
            'plainPassword' => '1234',
        );
        
        $response = $client->post('/api/tokens',[
            'auth' => ['raj2k2', 1234]
        ]);
        
        $tokenres = json_decode($response->getBody());
        $token = $tokenres->token;
//        echo $token;die;
        $response = $client->post('/api/register', [
            'body' => json_encode($data),
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ],  
        ]);
        
//        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals(200, $response->getStatusCode());
        
    }
}
