<?php

namespace TraceBundle\Listener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class LanguageListener {

    private $container;

    public function __construct(ContainerInterface $containerInterface) {
        $this->container = $containerInterface;
    }

    public function setLocale(GetResponseEvent $event) {
        $request = $event->getRequest();
        
        if ($request->attributes->get('_route') === '_wdt') {
            return;
        }

        if ($request->attributes->get('_route') === '_profiler') {
            return;
        }

        if ($request->attributes->get('_route') === 'fos_js_routing_js') {
            return;
        }
 
        if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }
        
//        var_dump($this->container->get('security.authorization_checker'));
        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
//            var_dump('hello');exit;
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            
            $userLocale = $user->getTenant()->getLanguage()->getValue();
            $tenantid = $this->container->get('tenant_manager')->getTenantId($user);
            $request->attributes->set('tenantid', $tenantid);
                   
            if ($userLocale) {
                $request->setLocale($userLocale);
                $translator = $this->container->get('translator');
                $translator->setLocale($userLocale);
            }
        }
    }

}
