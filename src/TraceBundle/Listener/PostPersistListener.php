<?php

namespace TraceBundle\Listener;

use TraceBundle\Entity\Campaign;
use TraceBundle\Entity\Applicant;
use TraceBundle\Entity\Campaignquestionresponse;
use TraceBundle\Entity\Campaigndocumentresponse;
use TraceBundle\Entity\CampaignClient;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\Event\LifecycleEventArgs;
use TraceBundle\Constant\TraceConstants;

class PostPersistListener {

    protected $container;
    
    /**
     * The post persist listener class.
     * 
     * @param Container $container
     * @param EntityManager $em
     */
    public function __construct(Container $container) {
        $this->container = $container;
    }

    /**
     *  This function adds all the clients for the current tenant to the campaign_client table.
     */
    private function addColleagues(LifecycleEventArgs $args,Campaign $campaign) {
        $em = $args->getEntityManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $tenant = $user->getTenant();
        $clients = $tenant->getClients();
        foreach($clients as $client){
            $campaignclient = new CampaignClient();
            $roles = $client->getRoles();
            $campaignclient->setClient($client);
            $campaignclient->setCampaign($campaign);
            if($roles[0] == 'ROLE_ADMIN' || ($roles[0] == 'ROLE_RECRUITER' && $client == $campaign->getClient())){    
                $campaignclient->setVisibility(TraceConstants::CAMPAIGN_VISIBILITY_ALL);
                $campaignclient->setEvaluation(TraceConstants::CAMPAIGN_EVALUATION_YES);
            } else {
                $campaignclient->setVisibility(TraceConstants::CAMPAIGN_VISIBILITY_NONE);
                $campaignclient->setEvaluation(TraceConstants::CAMPAIGN_EVALUATION_NO);
            }
            $em->persist($campaignclient);
            $em->flush();
        }
    }

//    public function addDefaultResponses(LifecycleEventArgs $args,Applicant $applicant) {
//        $em = $args->getEntityManager();
//        $campaignquestions = $applicant->getCampaign()->getQuestions();
//        foreach ($campaignquestions as $campaignquestion){
//            $campaignquestionresponse = new Campaignquestionresponse();
//            $campaignquestionresponse->setValue(null);
//            $campaignquestionresponse->setIsanswered(false);
//            $campaignquestionresponse->setIsviewed(false);
//            $campaignquestionresponse->setQuestion($campaignquestion);
//            $campaignquestionresponse->setApplicant($applicant);
//            $em->persist($campaignquestionresponse);
//            $em->flush();
//        }
//        $campaigndocuments = $applicant->getCampaign()->getCampaigndocuments();
//        foreach ($campaigndocuments as $campaigndocument){
//            $campaigndocumentresponse = new Campaigndocumentresponse();
//            $campaigndocumentresponse->setValue(null);
//            $campaigndocumentresponse->setIsuploaded(false);
//            $campaigndocumentresponse->setCampaigndocument($campaigndocument);
//            $campaigndocumentresponse->setApplicant($applicant);
//            $em->persist($campaigndocumentresponse);
//            $em->flush();
//        }
//    }
    
    /**
     *  The post persist operation is invoked for all insert operations to the entities.This method  is used to 
     *  add some events while inserting into the Lead and Candidate entities.
     *  
     * @param LifecycleEventArgs $args 
     */
    public function postPersist(LifecycleEventArgs $args) {
        
        $entity = $args->getEntity();

        if ($entity instanceof Campaign) {
            $this->addColleagues($args, $entity);
        }
        
//        if ($entity instanceof Applicant) {
//            $this->addDefaultResponses($args, $entity);
//        }
    }

}