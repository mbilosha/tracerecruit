<?php

namespace TraceBundle\DataFixtures\ORM;

use TraceBundle\Entity\Applicantstatus;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ApplicantstatusFixtures extends Fixture {

    public function load(ObjectManager $manager) {
        $applicantstatus = ['In progress', 'Shortlisted', 'Waitlisted', 'Archived', 
            'Rejected', 'Cancelled', 'Completed', 'Questionaire completed',
            'To be validated', 'To be manually verified', 'Pending evaluation'];
        
        for ($i = 0; $i < sizeof($applicantstatus); $i++) {
            $applicantstatusAdmin = new Applicantstatus();
            $applicantstatusAdmin->setValue($applicantstatus[$i]);
            $manager->persist($applicantstatusAdmin);
        }

        $manager->flush();
    }

}
