<?php

namespace TraceBundle\DataFixtures\ORM;

use TraceBundle\Entity\Campaignstatus;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CampaignstatusFixtures extends Fixture{
    public function load(ObjectManager $manager) {
        $campaignstatus = ['Active','Archived', 'Drafts'];
        
        for ($i = 0; $i < sizeof($campaignstatus); $i++) {
            $campaignstatusAdmin = new Campaignstatus();
            $campaignstatusAdmin->setValue($campaignstatus[$i]);
            $manager->persist($campaignstatusAdmin);
        }

        $manager->flush();
    }
}
