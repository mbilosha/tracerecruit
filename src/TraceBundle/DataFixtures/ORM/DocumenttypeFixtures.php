<?php

namespace TraceBundle\DataFixtures\ORM;

use TraceBundle\Entity\Documenttype;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class DocumenttypeFixtures extends Fixture {

    public function load(ObjectManager $manager) {
        $documenttype = ['video','image'];
        
        for ($i = 0; $i < sizeof($documenttype); $i++) {
            $documenttypeAdmin = new Documenttype();
            $documenttypeAdmin->setValue($documenttype[$i]);
            $manager->persist($documenttypeAdmin);
        }

        $manager->flush();
    }

}
