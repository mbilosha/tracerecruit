<?php

namespace TraceBundle\DataFixtures\ORM;

use TraceBundle\Entity\Salutation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class SalutationFixtures extends Fixture{
    
    public function load(ObjectManager $manager) {
        $salutation = ['Mr','Ms'];
        
        for ($i = 0; $i < sizeof($salutation); $i++) {
            $salutationAdmin = new Salutation();
            $salutationAdmin->setValue($salutation[$i]);
            $manager->persist($salutationAdmin);
        }

        $manager->flush();
    }
    
}
