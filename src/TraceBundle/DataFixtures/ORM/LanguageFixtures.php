<?php

namespace TraceBundle\DataFixtures\ORM;

use TraceBundle\Entity\Language;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LanguageFixtures extends Fixture {

    public function load(ObjectManager $manager) {
        $language = ['en','fr','it','es','pt'];
        
        for ($i = 0; $i < sizeof($language); $i++) {
            $languageAdmin = new Language();
            $languageAdmin->setValue($language[$i]);
            $manager->persist($languageAdmin);
        }

        $manager->flush();
    }

}
