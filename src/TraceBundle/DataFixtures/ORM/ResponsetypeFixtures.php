<?php

namespace TraceBundle\DataFixtures\ORM;

use TraceBundle\Entity\Responsetype;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ResponsetypeFixtures extends Fixture{
   
    public function load(ObjectManager $manager) {
        $responsetype = ['Video','Text'];
        
        for ($i = 0; $i < sizeof($responsetype); $i++) {
            $responsetypeAdmin = new Responsetype();
            $responsetypeAdmin->setValue($responsetype[$i]);
            $manager->persist($responsetypeAdmin);
        }

        $manager->flush();
    }
    
}
