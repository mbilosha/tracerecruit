<?php

namespace TraceBundle\DataFixtures\ORM;

use TraceBundle\Entity\Interviewtype;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class InterviewtypeFixtures extends Fixture {

    public function load(ObjectManager $manager) {
        $interviewtype = ['Pre-recorded','Live'];
        
        for ($i = 0; $i < sizeof($interviewtype); $i++) {
            $interviewtypeAdmin = new Interviewtype();
            $interviewtypeAdmin->setValue($interviewtype[$i]);
            $manager->persist($interviewtypeAdmin);
        }

        $manager->flush();
    }

}
