<?php

namespace TraceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TraceBundle\Entity\Applicant;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class AddApplicantType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        //Firstname
        $builder->add('firstname', TextType::class, array(
            'label' => 'Firstname',
            'required' => true,
            'attr' => array('class' => 'large_text'),
        ));

        //Lastname
        $builder->add('lastname', TextType::class, array(
            'label' => 'Lastname',
            'required' => true,
            'attr' => array('class' => 'large_text'),
        ));

        //Email Address
        $builder->add('email', EmailType::class, array(
            'attr' => array('class' => 'large_text'),
            'label' => 'Email',
            'required' => true,
//            'error_bubbling' => false,
        ));

        //Phonenumber
        $builder->add('phonenumber', TextType::class, array(
            'label' => 'Phone number',
            'attr' => array('class' => 'large_text'),
        ));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Applicant::class,
            'csrf_protection' => true,
            'validation_groups' => array('addapplicantpage'),
            'error_bubbling' => false,
        ));
    }

}
