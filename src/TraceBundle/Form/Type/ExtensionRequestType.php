<?php

namespace TraceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use TraceBundle\Entity\Applicant;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ExtensionRequestType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('reason', TextareaType::class, array(
            'required' => true,
            'attr' => array('style' => 'width: 400px; height: 200px', 'placeholder' => "Please describe the reason for extension..."),
        ));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Applicant::class,
            'validation_groups' => array('extensionrequest'),
        ));
    }

}
