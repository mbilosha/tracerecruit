<?php

namespace TraceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use TraceBundle\Entity\Question;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class QuestionType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder->add('israndom', ChoiceType::class, array(
            'choices' => array(
                'Random mode' => true,
            ),
            'expanded' => true,
            'multiple' => false,
//            'label' => false,
            'attr' => array('class' => 'large_text'),
            'required' => true,
        ));

        $builder->add('videoattachment', FileType::class, array(  
            "attr" => array("accept" => "http;https;ftp;video/mp4"),
            'required' => true,
            'data_class' => null,
        ));
        
        $builder->add('question', TextType::class, array(
            'attr' => array('class' => 'large_text'),
            'required' => true,
        ));
        
        $builder->add('imageattachment', FileType::class, array(  
            "attr" => array("accept" => "image/jpg;image/png;image/jpeg;image/gif"),
            'required' => true,
            'data_class' => null,
        ));
        
        $builder->add('responsetype', EntityType::class, array(
                    'class' => 'TraceBundle:Responsetype',
                    'choice_label' => 'value',
                    'attr' => array('class' => 'large_text'),
                    'required' => true,
                    'multiple' => false,
                    'expanded' => false,
                ));
        
        $builder->add('minduration', NumberType::class, array(
            'required' => true,
//            'attr' => array('class' => 'large_text'),
        ));
        
        $builder->add('maxduration', NumberType::class, array(
            'required' => true,
//            'attr' => array('class' => 'large_text'),
        ));
        
        $builder->add('thinktime', NumberType::class, array(
            'required' => true,
//            'attr' => array('class' => 'large_text'),
        ));
        
        $builder->add('maxtake', ChoiceType::class, array(
            'choices' => array(
                '1' => 1,
                '2' => 2,
            ),
            'expanded' => true,
            'multiple' => false,
            'attr' => array('class' => 'large_text'),
//            'label' => false,
        ));
        
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Question::class,
            'csrf_protection' => true,
            'validation_groups' => array('questions'),
        ));
    }
    
    public function getBlockPrefix() {
		return 'Question';   
	}
        
}
