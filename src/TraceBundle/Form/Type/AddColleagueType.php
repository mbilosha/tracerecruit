<?php

namespace TraceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;    
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TraceBundle\Entity\Client;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class AddColleagueType extends AbstractType {   
    
    public function buildForm(FormBuilderInterface $builder, array $options) { 
        
        //Firstname
        $builder->add('firstname', TextType::class, array(    
            'label' => 'Firstname',
            'required' => true,
            'attr' => array('class' => 'large_text'),
        ));
        
        //Lastname
        $builder->add('lastname', TextType::class, array(    
            'label' => 'Lastname',
            'required' => true,
            'attr' => array('class' => 'large_text'),
        ));  
        
        //Email Address
        $builder->add('email', EmailType::class, array(
            'attr' => array('class' => 'large_text'),
            'label' => 'Email',
            'required' => true,
            'error_bubbling' => false,
        ));
        
        //Roles
          
          $builder->add('role', ChoiceType::Class, array(              
              'choices' => array(
                    'Admin' => 'ROLE_ADMIN',
                    'Colleague' => 'ROLE_COLLEAGUE',
                    'Recruiter' => 'ROLE_RECRUITER',
                ),
                'expanded'   => true,
                'multiple' => false,
                'label'    => 'Roles',
                'required' => true,     
//                'mapped' => false,
            ));
          
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Client::class,
            'csrf_protection' => true,
            'validation_groups' => array('addcolleague'),
        )); 
    }
}
