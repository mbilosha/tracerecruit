<?php

namespace TraceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use TraceBundle\Entity\Campaign;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class CampaignType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

                $builder->add('campaigntitle', TextType::class, array(
                    'required' => true,
                ));

                $builder->add('description', TextareaType::class, array(
                    'required' => true,
                    'attr' => array('style' => 'width: 400px; height: 200px', 'placeholder' => "Please describe the campaign..."),
                ));

                $builder->add('location', TextType::class, array(
                    'required' => true,
                    'attr' => array('size'=>60,'id'=>"location_input", 'placeholder'=>"Enter a location", 'autocomplete'=>"on"),
                ));
                
                $builder->add('jobreference', TextType::class, array(
//                    'required' => true,
                ));

                $builder->add('language', EntityType::class, array(
                    'class' => 'TraceBundle:Language',
                    'choice_label' => 'value',
                    'required' => true,
                    'multiple' => false,
                    'expanded' => false,
                ));

                $builder->add('campaigndocuments', CollectionType::class, array(
                    'entry_type' => CampaigndocumentType::class,
                    'entry_options' => array('label' => false),
                    'allow_add' => true,
                    'by_reference' => false,
                    'allow_delete' => true,
                    'label' => 'Documents requested',
//                    'required' => true,
                ));
        }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Campaign::class,
        ));
    }
}
