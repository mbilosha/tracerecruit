<?php

namespace TraceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use TraceBundle\Entity\Campaign;
use TraceBundle\Entity\Campaigndocumentresponse;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Doctrine\ORM\EntityRepository;

class CampaignDocResponseType extends AbstractType {

    protected $campaign;

//    public function __construct(Campaign $campaign) {
//        $this->campaign = $campaign;
//    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $campaign = $options['campaign'];
        $campaignid = $campaign->getId();
        $builder->add('filename', EntityType::class, array(
            'class' => 'TraceBundle:Campaigndocument',
            'choice_label' => 'value',
            'query_builder' => function (EntityRepository $er) use ($campaignid) {
                return $er->createQueryBuilder('d')
                    ->where('d.campaign = :campaignid')
                    ->orderBy('d.id')
                    ->setParameter('campaignid',$campaignid);
            },
//            'choices' => $this->campaign->getCampaigndocuments(),
            'expanded' => false,
            'multiple' => false,
            'label' => 'Document name',
            'required' => true,
        ));
        
        $builder->add('attachment', FileType::class, array(  
            "attr" => array("accept" => "image/jpg;image/png;image/jpeg;image/gif;image/bmp;application/pdf;application/x-pdf"),
            'required' => true,
//            'data_class' => null,
        ));
        
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Campaigndocumentresponse::class,
            'csrf_protection' => false,
            'validation_groups' => array('campaigndocs'),
            'campaign' => array(),
        ));
    }

}