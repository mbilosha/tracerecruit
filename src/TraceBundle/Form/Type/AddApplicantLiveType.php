<?php

namespace TraceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TraceBundle\Entity\Applicant;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class AddApplicantLiveType extends AbstractType {

    protected $tenant;
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $this->tenant = $options['tenant']->getId();
        
        //Firstname
        $builder->add('firstname', TextType::class, array(
            'label' => 'Firstname',
            'required' => true,
            'attr' => array('class' => 'large_text'),
        ));

        //Lastname
        $builder->add('lastname', TextType::class, array(
            'label' => 'Lastname',
            'required' => true,
            'attr' => array('class' => 'large_text'),
        ));

        //Email Address
        $builder->add('email', EmailType::class, array(
            'attr' => array('class' => 'large_text'),
            'label' => 'Email',
            'required' => true,
//            'error_bubbling' => false,
        ));

        //Phonenumber
        $builder->add('phonenumber', TextType::class, array(
            'label' => 'Phone number',
            'attr' => array('class' => 'large_text'),
        ));
        
        //campaign title
        $builder->add('campaign', EntityType::class, array(
            'label' => 'Campaign',
            'class' => 'TraceBundle:Campaign',
            'choice_label' => 'campaigntitle',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('c')
                    ->where('c.tenant = :tenantid')
                    ->orderBy('c.id')
                    ->setParameter('tenantid',$this->tenant);
            },
            'expanded' => false,
            'multiple' => false,
        ));
        
        //interview date and time
        $builder->add('invitedate', DateTimeType::class, array(
            'label' => 'Interview time',
            'placeholder' => array(
                'year' => 'Year', 'month' => 'Month', 'day' => 'Day',
                'hour' => 'Hour', 'minute' => 'Minute', 'second' => 'Second',
            )
        )); 
        
        //interview duration
        $builder->add('liveinterviewduration', TextType::class, array(
            'label' => 'interview duration',
            'required' => true,
            'attr' => array('class' => 'large_text'),
        ));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Applicant::class,
            'csrf_protection' => true,
            'validation_groups' => array('addapplicantlivepage'),
            'tenant' => array(),
        ));
    }

}
