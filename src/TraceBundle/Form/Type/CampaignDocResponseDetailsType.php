<?php

namespace TraceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use TraceBundle\Entity\Campaign;
use TraceBundle\Entity\Campaigndocumentresponse;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class CampaignDocResponseDetailsType extends AbstractType {

    protected $applicant;
    public $idarray = array();
    
    

    public function buildForm(FormBuilderInterface $builder, array $options) {
//        $this->applicant = $options['applicant'];
//        var_dump($applicant);exit;
        $this->applicant = $options['applicant']->getId();
        $builder->add('filename', EntityType::class, array(
            'class' => 'TraceBundle:Campaigndocumentresponse',
            'choice_label' => 'value',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('r')
                    ->where('r.applicant = :applicantid')
                    ->orderBy('r.id')
                    ->setParameter('applicantid',$this->applicant);
            },
            'expanded' => true,
            'multiple' => false,
            'label' => false,        
        ));
        
        $builder->get('filename')->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            $data = $event->getForm()->getData();
            $form = $event->getForm()->getParent();
            $this->idarray = $data->getId();
//            var_dump($this->idarray);exit;
            $form->add('filename', EntityType::class, array(
                'class' => 'TraceBundle:Campaigndocumentresponse',
                'choice_label' => 'value',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('r')
                            ->where('r.applicant = :applicantid')
                            ->andWhere('r.id NOT IN (?1)')
                            ->setParameter('applicantid', $this->applicant)
                            ->setParameter(1, $this->idarray);
                },
                'expanded' => true,
                'multiple' => false,
                'label' => false,        
            ));
        })
        ->getForm();    
    }
    
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Campaigndocumentresponse::class,
            'csrf_protection' => false,
            'applicant' => array(),
        ));
    }

}

