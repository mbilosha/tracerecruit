<?php

namespace TraceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;    
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TraceBundle\Entity\Tenant;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class TenantType extends AbstractType{
    
    public function buildForm(FormBuilderInterface $builder, array $options) { 
                
        //Language Type
        $builder->add('language', EntityType::class, array(
            'class' => 'TraceBundle:Language',
            'choice_label' => 'value',
            'attr' => array('class' => 'large_text'),
            'required' => true,
            'multiple' => false,
            'expanded' => false,
        ));
        
        //Name
        $builder->add('name', TextType::class , array(
            'label' => 'Name',
            'attr' => array('class' => 'large_text'),
            'required' => true,
            'error_bubbling' => false,
        ));
        
        //Officialwebsiteurl
        $builder->add('officialwebsiteurl', TextType::class, array(    
            'label' => 'Officialwebsiteurl',
            'required' => true,
            'attr' => array('class' => 'large_text'),
        ));
        
        //Tenantsubdomainurl
//        $builder->add('tenantsubdomainurl', TextType::class, array(    
//            'label' => 'Tenantsubdomainurl',
//            'required' => true,
//            'attr' => array('class' => 'large_text'),
//        ));
        
        //Logourl
//        $builder->add('logourl', TextType::class, array(    
//            'label' => 'Logourl',
//            'required' => true,
//            'attr' => array('class' => 'large_text'),
//        ));  
                
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Tenant::class,
            'csrf_protection' => true,
            'validation_groups' => array('registrationpage'),
        )); 
    }
}
