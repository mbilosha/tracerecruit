<?php

namespace TraceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;    
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TraceBundle\Entity\Client;
use TraceBundle\Form\Type\TenantType;
use TraceBundle\Form\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class AdminRegistrationType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) { 
         
        //TenantType Form
        $builder->add('tenant', TenantType::class);
              
        //Salutation
//        $builder->add('salutation', TextType::class, array(    
//            'label' => 'Title',
//            'required' => true,
//            'attr' => array('class' => 'large_text'),
//        ));
        
        //Firstname
        $builder->add('firstname', TextType::class, array(    
            'label' => 'Firstname',
            'required' => true,
            'attr' => array('class' => 'large_text'),
        ));
        
        //Lastname
        $builder->add('lastname', TextType::class, array(    
            'label' => 'Lastname',
            'required' => true,
            'attr' => array('class' => 'large_text'),
        ));  
        
        //Email Address
        $builder->add('email', EmailType::class, array(
            'attr' => array('class' => 'large_text'),
            'label' => 'Email address',
            'required' => true,
            'error_bubbling' => false,
        ));
        
        //Username
        $builder->add('username', TextType::class , array(
            'label' => 'Username',
            'attr' => array('class' => 'large_text'),
            'required' => true,
            'error_bubbling' => false,
        ));
                     
        //Password
        $builder->add('plainPassword', RepeatedType::class, array(
            'type' => PasswordType::class,
            'first_name' => 'pass',
            'second_name' => 'confirm',
            'invalid_message' => 'Password not match',
            'options' => array('attr' => array('class' => 'password-field')),
            'required' => true,
            'error_bubbling' => false,
        ));
        
        
    }
 
    public function getParent() {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix() {
        return 'app_user_registration';
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Client::class,
            'csrf_protection' => true,
            'validation_groups' => array('registrationpage'),
        )); 
    }
}
