<?php

namespace TraceBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use TraceBundle\Entity\Salutation;
use TraceBundle\Entity\Tenant;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @UniqueEntity(fields="username",message="Username already exists in database",groups={"registrationpage"})
 * @UniqueEntity(fields="email",message = "Email already exists in database",groups={"registrationpage"})
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\ClientRepository")
 */
class Client extends BaseUser {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="Salutation", inversedBy="clients", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="salutation_id", referencedColumnName="id")
     */
    protected $salutation;

    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="Tenant", inversedBy="clients", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="tenant_id", referencedColumnName="id")
     */
    protected $tenant;

    /**
     * @var string
     * @Assert\NotBlank(message="Firstname cannot be blank", groups={"registrationpage","addcolleague"})
     * @Assert\Length(min=4,max=100,minMessage="Firstname should be minimum {{ limit }} characters", maxMessage="Your Firstname cannot be longer than {{ limit }} characters",groups={"registrationpage"}
     * )
     * @Assert\Regex(
     *      pattern="/[^A-Za-zà-ÿÀ-Ÿ .\']/i",
     *      match=false,
     *      message="Firstname allows only a-z,A-Z and few special characters (. ')",groups={"registrationpage","addcolleague"})
     * @ORM\Column(name="firstname", type="string", length=100, nullable=false)
     */
    protected $firstname;

    /**
     * @var string
     * @Assert\NotBlank(message="Lastname cannot be blank", groups={"registrationpage","addcolleague"})
     * @Assert\Length(min=4,max=100,minMessage="Lastname should be minimum {{ limit }} characters", maxMessage="Your Lastname cannot be longer than {{ limit }} characters",groups={"registrationpage"}
     * )
     * @Assert\Regex(
     *      pattern="/[^A-Za-zà-ÿÀ-Ÿ .\']/i",
     *      match=false,
     *      message="Lastname allows only a-z,A-Z and few special characters (. ')",groups={"registrationpage","addcolleague"})
     * @ORM\Column(name="lastname", type="string", length=100, nullable=false)
     */
    protected $lastname;

    /**
     * 
     * @Assert\NotBlank( message = "Username cannot be blank", groups={"registrationpage"})
     * @Assert\Regex(
     *      pattern="/[^a-zA-Z0-9-_]/", 
     *      match = false,
     *      message = "Username allows only a-z,A-Z,0-9,_", groups={"registrationpage"})
     * @Assert\Length(min=4,max=30,minMessage="Username should be minimum {{ limit }} characters",maxMessage="Username cannot exceeds {{ limit }} characters",groups={"registrationpage"})
     */
    protected $username;

    /**
     * @Assert\Email(message = "Email not valid", groups={"registrationpage","addcolleague"})
     * @Assert\NotBlank(message = "Email cannot be blank", groups={"registrationpage","addcolleague"})
     * @Assert\Length(max=255,maxMessage="Email cannot exceeds 255 characters")
     */
    protected $email;

    /**
     * @Assert\NotBlank( message = "Password cannot be blank", groups={"registrationpage"})
     * @Assert\Length(min=4,max=30,minMessage="Password should be minimum {{ limit }} characters",maxMessage="Password cannot exceeds {{ limit }} characters",groups={"registrationpage"})
     */
    protected $plainPassword;
       
    /**
     * @Assert\NotBlank( message = "Role cannot be blank", groups={"addcolleague"})
     * @ORM\Column(name="role", type="string", nullable=true)
     */
    protected $role;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isenabled", type="boolean", nullable=true)
     */
    protected $isenabled;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isblacklisted", type="boolean", nullable=true)
     */
    protected $isblacklisted;

    /**
     * @var string
     *
     * @ORM\Column(name="blacklistreason", type="string", length=60, nullable=true)
     */
    protected $blacklistreason;

    /**
     * @var string
     *
     * @ORM\Column(name="profilepicurl", type="string", length=2083, nullable=true)
     */
    protected $profilepicurl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdon", type="datetime", nullable=true)
     */
    protected $createdon;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedon", type="datetime", nullable=true)
     */
    protected $updatedon;

    /**
     * @ORM\OneToMany(targetEntity="CampaignClient", mappedBy="client")
     */
    protected $campaignclients;

    /**
     * @ORM\OneToMany(targetEntity="Campaign", mappedBy="client")
     */
    protected $campaignsharings;
    
    /**
     * @ORM\OneToMany(targetEntity="Campaign", mappedBy="client")
     */
    protected $campaigns;
    
    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="client")
     */
    protected $comments;

    public function __construct() {
        parent::__construct();
        $this->campaignclients = new ArrayCollection();
        $this->campaignsharings = new ArrayCollection();
        $this->campaigns = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Client
     */
    public function setFirstname($firstname) {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname() {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return Client
     */
    public function setLastname($lastname) {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname() {
        return $this->lastname;
    }

    /**
     * Set isenabled
     *
     * @param boolean $isenabled
     *
     * @return Client
     */
    public function setIsenabled($isenabled) {
        $this->isenabled = $isenabled;

        return $this;
    }

    /**
     * Get isenabled
     *
     * @return boolean
     */
    public function getIsenabled() {
        return $this->isenabled;
    }

    /**
     * Set isblacklisted
     *
     * @param boolean $isblacklisted
     *
     * @return Client
     */
    public function setIsblacklisted($isblacklisted) {
        $this->isblacklisted = $isblacklisted;

        return $this;
    }

    /**
     * Get isblacklisted
     *
     * @return boolean
     */
    public function getIsblacklisted() {
        return $this->isblacklisted;
    }

    /**
     * Set blacklistreason
     *
     * @param string $blacklistreason
     *
     * @return Client
     */
    public function setBlacklistreason($blacklistreason) {
        $this->blacklistreason = $blacklistreason;

        return $this;
    }

    /**
     * Get blacklistreason
     *
     * @return string
     */
    public function getBlacklistreason() {
        return $this->blacklistreason;
    }

    /**
     * Set profilepicurl
     *
     * @param string $profilepicurl
     *
     * @return Client
     */
    public function setProfilepicurl($profilepicurl) {
        $this->profilepicurl = $profilepicurl;

        return $this;
    }

    /**
     * Get profilepicurl
     *
     * @return string
     */
    public function getProfilepicurl() {
        return $this->profilepicurl;
    }

    /**
     * Set createdon
     *
     * @param \DateTime $createdon
     *
     * @return Client
     */
    public function setCreatedon($createdon) {
        $this->createdon = $createdon;

        return $this;
    }

    /**
     * Get createdon
     *
     * @return \DateTime
     */
    public function getCreatedon() {
        return $this->createdon;
    }

    /**
     * Set updatedon
     *
     * @param \DateTime $updatedon
     *
     * @return Client
     */
    public function setUpdatedon($updatedon) {
        $this->updatedon = $updatedon;

        return $this;
    }

    /**
     * Get updatedon
     *
     * @return \DateTime
     */
    public function getUpdatedon() {
        return $this->updatedon;
    }

    /**
     * Set salutation
     *
     * @param \TraceBundle\Entity\Salutation $salutation
     *
     * @return Client
     */
    public function setSalutation(\TraceBundle\Entity\Salutation $salutation = null) {
        $this->salutation = $salutation;

        return $this;
    }

    /**
     * Get salutation
     *
     * @return \TraceBundle\Entity\Salutation
     */
    public function getSalutation() {
        return $this->salutation;
    }

    /**
     * Set tenant
     *
     * @param \TraceBundle\Entity\Tenant $tenant
     *
     * @return Client
     */
    public function setTenant(\TraceBundle\Entity\Tenant $tenant = null) {
        $this->tenant = $tenant;

        return $this;
    }

    /**
     * Get tenant
     *
     * @return \TraceBundle\Entity\Tenant
     */
    public function getTenant() {
        return $this->tenant;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return Client
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Add campaignsharing
     *
     * @param \TraceBundle\Entity\Campaign $campaignsharing
     *
     * @return Client
     */
    public function addCampaignsharing(\TraceBundle\Entity\Campaign $campaignsharing)
    {
        $this->campaignsharings[] = $campaignsharing;

        return $this;
    }

    /**
     * Remove campaignsharing
     *
     * @param \TraceBundle\Entity\Campaign $campaignsharing
     */
    public function removeCampaignsharing(\TraceBundle\Entity\Campaign $campaignsharing)
    {
        $this->campaignsharings->removeElement($campaignsharing);
    }

    /**
     * Get campaignsharings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaignsharings()
    {
        return $this->campaignsharings;
    }

    /**
     * Add campaignclient
     *
     * @param \TraceBundle\Entity\CampaignClient $campaignclient
     *
     * @return Client
     */
    public function addCampaignclient(\TraceBundle\Entity\CampaignClient $campaignclient)
    {
        $this->campaignclients[] = $campaignclient;

        return $this;
    }

    /**
     * Remove campaignclient
     *
     * @param \TraceBundle\Entity\CampaignClient $campaignclient
     */
    public function removeCampaignclient(\TraceBundle\Entity\CampaignClient $campaignclient)
    {
        $this->campaignclients->removeElement($campaignclient);
    }

    /**
     * Get campaignclients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaignclients()
    {
        return $this->campaignclients;
    }

    /**
     * Add campaign
     *
     * @param \TraceBundle\Entity\Campaign $campaign
     *
     * @return Client
     */
    public function addCampaign(\TraceBundle\Entity\Campaign $campaign)
    {
        $this->campaigns[] = $campaign;

        return $this;
    }

    /**
     * Remove campaign
     *
     * @param \TraceBundle\Entity\Campaign $campaign
     */
    public function removeCampaign(\TraceBundle\Entity\Campaign $campaign)
    {
        $this->campaigns->removeElement($campaign);
    }

    /**
     * Get campaigns
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }

    /**
     * Add comment
     *
     * @param \TraceBundle\Entity\Comment $comment
     *
     * @return Client
     */
    public function addComment(\TraceBundle\Entity\Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \TraceBundle\Entity\Comment $comment
     */
    public function removeComment(\TraceBundle\Entity\Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }
}
