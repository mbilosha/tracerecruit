<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
//use Doctrine\Common\Collections\ArrayCollection;
//use Symfony\Component\Validator\Constraints as Assert;

/**
 * CampaignClient
 *
 * @ORM\Table(name="campaign_client")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\CampaignClientRepository")
 */
class CampaignClient {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="campaignclients", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    protected $client;
    
    /**
     * @ORM\ManyToOne(targetEntity="Campaign", inversedBy="campaignclients", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    protected $campaign;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="visibility", type="integer", nullable=true)
     */
    protected $visibility;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="evaluation", type="boolean", nullable=true)
     */
    protected $evaluation;

    function __clone() {
        $this->id = null;
        $this->campaign = null;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set visibility
     *
     * @param integer $visibility
     *
     * @return CampaignClient
     */
    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * Get visibility
     *
     * @return integer
     */
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * Set evaluation
     *
     * @param boolean $evaluation
     *
     * @return CampaignClient
     */
    public function setEvaluation($evaluation)
    {
        $this->evaluation = $evaluation;

        return $this;
    }

    /**
     * Get evaluation
     *
     * @return boolean
     */
    public function getEvaluation()
    {
        return $this->evaluation;
    }

    /**
     * Set client
     *
     * @param \TraceBundle\Entity\Client $client
     *
     * @return CampaignClient
     */
    public function setClient(\TraceBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \TraceBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set campaign
     *
     * @param \TraceBundle\Entity\Campaign $campaign
     *
     * @return CampaignClient
     */
    public function setCampaign(\TraceBundle\Entity\Campaign $campaign = null)
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * Get campaign
     *
     * @return \TraceBundle\Entity\Campaign
     */
    public function getCampaign()
    {
        return $this->campaign;
    }
}
