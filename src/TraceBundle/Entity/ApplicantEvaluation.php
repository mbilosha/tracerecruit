<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ApplicantEvaluation
 *
 * @ORM\Table(name="applicantevaluation")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\ApplicantEvaluationRepository")
 */
class ApplicantEvaluation { 
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var float
     * @Assert\NotBlank(message="Value cannot be blank")
     * @ORM\Column(name="value", type="float", precision=6, scale=2, nullable=true)
     * @Assert\Range(
     *      min = 0.00,
     *      max = 5.00,
     *      minMessage = "The value cannot be less than {{ limit }}",
     *      maxMessage = "The value cannot be greater than {{ limit }}"
     * )
     */
    protected $value;
    
    /**
     * @ORM\ManyToOne(targetEntity="Applicant", inversedBy="applicantevaluations", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="applicant_id", referencedColumnName="id")
     */
    protected $applicant;

    /**
     * @ORM\ManyToOne(targetEntity="Evaluationcriteria", inversedBy="applicantevaluations", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="evaluationcriterion_id", referencedColumnName="id")
     */
    protected $evaluationcriterion;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param float $value
     *
     * @return ApplicantEvaluation
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set applicant
     *
     * @param \TraceBundle\Entity\Applicant $applicant
     *
     * @return ApplicantEvaluation
     */
    public function setApplicant(\TraceBundle\Entity\Applicant $applicant = null)
    {
        $this->applicant = $applicant;

        return $this;
    }

    /**
     * Get applicant
     *
     * @return \TraceBundle\Entity\Applicant
     */
    public function getApplicant()
    {
        return $this->applicant;
    }

    /**
     * Set evaluationcriterion
     *
     * @param \TraceBundle\Entity\Evaluationcriteria $evaluationcriterion
     *
     * @return ApplicantEvaluation
     */
    public function setEvaluationcriterion(\TraceBundle\Entity\Evaluationcriteria $evaluationcriterion = null)
    {
        $this->evaluationcriterion = $evaluationcriterion;

        return $this;
    }

    /**
     * Get evaluationcriterion
     *
     * @return \TraceBundle\Entity\Evaluationcriteria
     */
    public function getEvaluationcriterion()
    {
        return $this->evaluationcriterion;
    }
}
