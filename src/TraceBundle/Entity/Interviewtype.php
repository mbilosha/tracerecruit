<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Interviewtype
 *
 * @ORM\Table(name="interviewtype")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\InterviewtypeRepository")
 */
class Interviewtype {
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="value", type="string", length=60, nullable=true)
     */
    protected $value;
    
    /**
     * @ORM\OneToMany(targetEntity="Applicant", mappedBy="interviewtype")
     */
    protected $applicants;

    public function __construct() {
        $this->applicants = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Interviewtype
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Add applicant
     *
     * @param \TraceBundle\Entity\Applicant $applicant
     *
     * @return Interviewtype
     */
    public function addApplicant(\TraceBundle\Entity\Applicant $applicant)
    {
        $this->applicants[] = $applicant;

        return $this;
    }

    /**
     * Remove applicant
     *
     * @param \TraceBundle\Entity\Applicant $applicant
     */
    public function removeApplicant(\TraceBundle\Entity\Applicant $applicant)
    {
        $this->applicants->removeElement($applicant);
    }

    /**
     * Get applicants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApplicants()
    {
        return $this->applicants;
    }
}
