<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Campaignliveresponse
 *
 * @ORM\Table(name="campaignliveresponse")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\CampaignliveresponseRepository")
 */
class Campaignliveresponse { 
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="liveurl", type="string", length=2083, nullable=true)
     */
    protected $liveurl;
    
    /**
     * @var int
     *
     * @ORM\Column(name="interviewduration", type="integer", nullable=true)
     */
    protected $interviewduration;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="interviewstarttime", type="datetime", nullable=true)
     */
    protected $interviewstarttime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="interviewendtime", type="datetime", nullable=true)
     */
    protected $interviewendtime;

    /**
     * @ORM\ManyToOne(targetEntity="Campaign", inversedBy="campaignliveresponses", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    protected $campaign;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set liveurl
     *
     * @param string $liveurl
     *
     * @return Campaignliveresponse
     */
    public function setLiveurl($liveurl)
    {
        $this->liveurl = $liveurl;

        return $this;
    }

    /**
     * Get liveurl
     *
     * @return string
     */
    public function getLiveurl()
    {
        return $this->liveurl;
    }

    /**
     * Set interviewduration
     *
     * @param integer $interviewduration
     *
     * @return Campaignliveresponse
     */
    public function setInterviewduration($interviewduration)
    {
        $this->interviewduration = $interviewduration;

        return $this;
    }

    /**
     * Get interviewduration
     *
     * @return integer
     */
    public function getInterviewduration()
    {
        return $this->interviewduration;
    }

    /**
     * Set interviewstarttime
     *
     * @param \DateTime $interviewstarttime
     *
     * @return Campaignliveresponse
     */
    public function setInterviewstarttime($interviewstarttime)
    {
        $this->interviewstarttime = $interviewstarttime;

        return $this;
    }

    /**
     * Get interviewstarttime
     *
     * @return \DateTime
     */
    public function getInterviewstarttime()
    {
        return $this->interviewstarttime;
    }

    /**
     * Set interviewendtime
     *
     * @param \DateTime $interviewendtime
     *
     * @return Campaignliveresponse
     */
    public function setInterviewendtime($interviewendtime)
    {
        $this->interviewendtime = $interviewendtime;

        return $this;
    }

    /**
     * Get interviewendtime
     *
     * @return \DateTime
     */
    public function getInterviewendtime()
    {
        return $this->interviewendtime;
    }

    /**
     * Set campaign
     *
     * @param \TraceBundle\Entity\Campaign $campaign
     *
     * @return Campaignliveresponse
     */
    public function setCampaign(\TraceBundle\Entity\Campaign $campaign = null)
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * Get campaign
     *
     * @return \TraceBundle\Entity\Campaign
     */
    public function getCampaign()
    {
        return $this->campaign;
    }
}
