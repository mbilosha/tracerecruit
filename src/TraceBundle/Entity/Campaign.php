<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Campaign
 *
 * @ORM\Table(name="campaign")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\CampaignRepository")
 */
class Campaign {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Campaign Title cannot be blank", groups={"create_campaign"})
     * @ORM\Column(name="campaigntitle", type="string", length=400, nullable=true)
     */
    protected $campaigntitle;

    /**
     * @var string
     * @Assert\NotBlank(message="Location cannot be blank", groups={"create_campaign"})
     * @ORM\Column(name="location", type="string", length=100, nullable=true)
     */
    protected $location;
    
    /**
     * @var string
     * @ORM\Column(name="jobreference", type="string", length=60, nullable=true)
     */
    protected $jobreference;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="hasintro", type="boolean", nullable=true)
     */
    protected $hasintro;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="hasoutro", type="boolean", nullable=true)
     */
    protected $hasoutro;
    
    /**
     * @var int
     * @Assert\NotBlank(message="Deadline cannot be blank", groups={"create_campaign"})
     * @ORM\Column(name="deadlinetoanswer", type="integer", nullable=true)
     */
    protected $deadlinetoanswer;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="ispublic", type="boolean", nullable=true)
     */
    protected $ispublic=false;
    
    /**
     * @var string
     * @Assert\NotBlank(message="Welcome message cannot be blank", groups={"create_campaign"})
     * @ORM\Column(name="prerecwelcomemsg", type="text", nullable=true)
     */
    protected $prerecwelcomemsg;
    
    /**
     * @var string
     * @Assert\NotBlank(message="Description cannot be blank")
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;
    
    /**
     * @var string
     * @Assert\NotBlank(message="Thank you message cannot be blank", groups={"create_campaign"})
     * @ORM\Column(name="prerecthankyoumsg", type="text", nullable=true)
     */
    protected $prerecthankyoumsg;
    
    /**
     * @var string
     * @Assert\NotBlank(message="Welcome message cannot be blank", groups={"create_campaign"})
     * @ORM\Column(name="livewelcomemsg", type="text", nullable=true)
     */
    protected $livewelcomemsg;
    
    /**
     * @var string
     * @Assert\NotBlank(message="Thank you message cannot be blank", groups={"create_campaign"})
     * @ORM\Column(name="livethankyoumsg", type="text", nullable=true)
     */
    protected $livethankyoumsg;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="isprerecorded", type="boolean", nullable=true)
     */
    protected $isprerecorded=false;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="islive", type="boolean", nullable=true)
     */
    protected $islive=false;
    
    /**
     * @ORM\ManyToOne(targetEntity="Language", inversedBy="campaigns", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="applicantlanguage_id", referencedColumnName="id")
     */
    protected $language;
    
    /**
     * @ORM\ManyToOne(targetEntity="Campaignstatus", inversedBy="campaigns", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="campaignstatus_id", referencedColumnName="id")
     */
    protected $campaignstatus;
    
    /**
     * @ORM\ManyToOne(targetEntity="Tenant", inversedBy="campaigns", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="tenant_id", referencedColumnName="id")
     */
    protected $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="campaigns", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="createdby", referencedColumnName="id")
     */
    protected $client;
    
    /**
     * @ORM\OneToMany(targetEntity="Campaignsharing", mappedBy="campaign")
     */
    protected $campaignsharings;

    /**
     * @ORM\OneToMany(targetEntity="Evaluationcriteria", mappedBy="campaign", cascade={"persist","remove"})
     */
    protected $evaluationcriteria;
    
    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="Question", mappedBy="campaign", cascade={"persist","remove"})
     */
    protected $questions;
    
    /**
     * @ORM\OneToMany(targetEntity="Tag", mappedBy="campaign")
     */
    protected $tags;
    
    /**
     * @ORM\OneToMany(targetEntity="Campaigndocument", mappedBy="campaign")
     */
    protected $campaigndocuments;
    
    /**
     * @ORM\OneToMany(targetEntity="CampaignClient", mappedBy="campaign")
     */
    protected $campaignclients;
    
    /**
     * @ORM\OneToMany(targetEntity="Applicant", mappedBy="campaign")
     */
    protected $applicants;
    
    /**
     * @ORM\OneToMany(targetEntity="Campaignliveresponse", mappedBy="campaign")
     */
    protected $campaignliveresponses;
    
    public function __construct() {
        $this->campaignsharings = new ArrayCollection();
        $this->evaluationcriteria = new ArrayCollection();
        $this->questions = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->campaigndocuments = new ArrayCollection();
        $this->campaignclients = new ArrayCollection();
        $this->applicants = new ArrayCollection();
        $this->campaignliveresponses = new ArrayCollection();
    }

    function __clone() {
        $this->id = null;
//        $this->language = null;
        $this->campaignstatus = null;
//        $this->tenant = null;
        $this->client = null;
//        $this->campaignsharings = null;
        $this->evaluationcriteria = null;
        $this->questions = null;
//        $this->tags = null;
        $this->campaigndocuments = null;
        $this->campaignclients = null;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set campaigntitle
     *
     * @param string $campaigntitle
     *
     * @return Campaign
     */
    public function setCampaigntitle($campaigntitle)
    {
        $this->campaigntitle = $campaigntitle;

        return $this;
    }

    /**
     * Get campaigntitle
     *
     * @return string
     */
    public function getCampaigntitle()
    {
        return $this->campaigntitle;
    }

    /**
     * Set jobreference
     *
     * @param string $jobreference
     *
     * @return Campaign
     */
    public function setJobreference($jobreference)
    {
        $this->jobreference = $jobreference;

        return $this;
    }

    /**
     * Get jobreference
     *
     * @return string
     */
    public function getJobreference()
    {
        return $this->jobreference;
    }

    /**
     * Set hasintro
     *
     * @param boolean $hasintro
     *
     * @return Campaign
     */
    public function setHasintro($hasintro)
    {
        $this->hasintro = $hasintro;

        return $this;
    }

    /**
     * Get hasintro
     *
     * @return boolean
     */
    public function getHasintro()
    {
        return $this->hasintro;
    }

    /**
     * Set hasoutro
     *
     * @param boolean $hasoutro
     *
     * @return Campaign
     */
    public function setHasoutro($hasoutro)
    {
        $this->hasoutro = $hasoutro;

        return $this;
    }

    /**
     * Get hasoutro
     *
     * @return boolean
     */
    public function getHasoutro()
    {
        return $this->hasoutro;
    }

    /**
     * Set deadlinetoanswer
     *
     * @param integer $deadlinetoanswer
     *
     * @return Campaign
     */
    public function setDeadlinetoanswer($deadlinetoanswer)
    {
        $this->deadlinetoanswer = $deadlinetoanswer;

        return $this;
    }

    /**
     * Get deadlinetoanswer
     *
     * @return integer
     */
    public function getDeadlinetoanswer()
    {
        return $this->deadlinetoanswer;
    }

    /**
     * Set ispublic
     *
     * @param boolean $ispublic
     *
     * @return Campaign
     */
    public function setIspublic($ispublic)
    {
        $this->ispublic = $ispublic;

        return $this;
    }

    /**
     * Get ispublic
     *
     * @return boolean
     */
    public function getIspublic()
    {
        return $this->ispublic;
    }

    /**
     * Set prerecwelcomemsg
     *
     * @param string $prerecwelcomemsg
     *
     * @return Campaign
     */
    public function setPrerecwelcomemsg($prerecwelcomemsg)
    {
        $this->prerecwelcomemsg = $prerecwelcomemsg;

        return $this;
    }

    /**
     * Get prerecwelcomemsg
     *
     * @return string
     */
    public function getPrerecwelcomemsg()
    {
        return $this->prerecwelcomemsg;
    }

    /**
     * Set prerecthankyoumsg
     *
     * @param string $prerecthankyoumsg
     *
     * @return Campaign
     */
    public function setPrerecthankyoumsg($prerecthankyoumsg)
    {
        $this->prerecthankyoumsg = $prerecthankyoumsg;

        return $this;
    }

    /**
     * Get prerecthankyoumsg
     *
     * @return string
     */
    public function getPrerecthankyoumsg()
    {
        return $this->prerecthankyoumsg;
    }

    /**
     * Set livewelcomemsg
     *
     * @param string $livewelcomemsg
     *
     * @return Campaign
     */
    public function setLivewelcomemsg($livewelcomemsg)
    {
        $this->livewelcomemsg = $livewelcomemsg;

        return $this;
    }

    /**
     * Get livewelcomemsg
     *
     * @return string
     */
    public function getLivewelcomemsg()
    {
        return $this->livewelcomemsg;
    }

    /**
     * Set livethankyoumsg
     *
     * @param string $livethankyoumsg
     *
     * @return Campaign
     */
    public function setLivethankyoumsg($livethankyoumsg)
    {
        $this->livethankyoumsg = $livethankyoumsg;

        return $this;
    }

    /**
     * Get livethankyoumsg
     *
     * @return string
     */
    public function getLivethankyoumsg()
    {
        return $this->livethankyoumsg;
    }

    /**
     * Set isprerecorded
     *
     * @param boolean $isprerecorded
     *
     * @return Campaign
     */
    public function setIsprerecorded($isprerecorded)
    {
        $this->isprerecorded = $isprerecorded;

        return $this;
    }

    /**
     * Get isprerecorded
     *
     * @return boolean
     */
    public function getIsprerecorded()
    {
        return $this->isprerecorded;
    }

    /**
     * Set islive
     *
     * @param boolean $islive
     *
     * @return Campaign
     */
    public function setIslive($islive)
    {
        $this->islive = $islive;

        return $this;
    }

    /**
     * Get islive
     *
     * @return boolean
     */
    public function getIslive()
    {
        return $this->islive;
    }

    /**
     * Set language
     *
     * @param \TraceBundle\Entity\Language $language
     *
     * @return Campaign
     */
    public function setLanguage(\TraceBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \TraceBundle\Entity\Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set campaignstatus
     *
     * @param \TraceBundle\Entity\Campaignstatus $campaignstatus
     *
     * @return Campaign
     */
    public function setCampaignstatus(\TraceBundle\Entity\Campaignstatus $campaignstatus = null)
    {
        $this->campaignstatus = $campaignstatus;

        return $this;
    }

    /**
     * Get campaignstatus
     *
     * @return \TraceBundle\Entity\Campaignstatus
     */
    public function getCampaignstatus()
    {
        return $this->campaignstatus;
    }

    /**
     * Set tenant
     *
     * @param \TraceBundle\Entity\Tenant $tenant
     *
     * @return Campaign
     */
    public function setTenant(\TraceBundle\Entity\Tenant $tenant = null)
    {
        $this->tenant = $tenant;

        return $this;
    }

    /**
     * Get tenant
     *
     * @return \TraceBundle\Entity\Tenant
     */
    public function getTenant()
    {
        return $this->tenant;
    }

    /**
     * Add campaignsharing
     *
     * @param \TraceBundle\Entity\Campaignsharing $campaignsharing
     *
     * @return Campaign
     */
    public function addCampaignsharing(\TraceBundle\Entity\Campaignsharing $campaignsharing)
    {
        $this->campaignsharings[] = $campaignsharing;

        return $this;
    }

    /**
     * Remove campaignsharing
     *
     * @param \TraceBundle\Entity\Campaignsharing $campaignsharing
     */
    public function removeCampaignsharing(\TraceBundle\Entity\Campaignsharing $campaignsharing)
    {
        $this->campaignsharings->removeElement($campaignsharing);
    }

    /**
     * Get campaignsharings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaignsharings()
    {
        return $this->campaignsharings;
    }

    /**
     * Add evaluationcriterium
     *
     * @param \TraceBundle\Entity\Evaluationcriteria $evaluationcriterium
     *
     * @return Campaign
     */
    public function addEvaluationcriterium(\TraceBundle\Entity\Evaluationcriteria $evaluationcriterium)
    {
        $this->evaluationcriteria[] = $evaluationcriterium;

        return $this;
    }

    /**
     * Remove evaluationcriterium
     *
     * @param \TraceBundle\Entity\Evaluationcriteria $evaluationcriterium
     */
    public function removeEvaluationcriterium(\TraceBundle\Entity\Evaluationcriteria $evaluationcriterium)
    {
        $this->evaluationcriteria->removeElement($evaluationcriterium);
    }

    /**
     * Get evaluationcriteria
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvaluationcriteria()
    {
        return $this->evaluationcriteria;
    }

    /**
     * Add question
     *
     * @param \TraceBundle\Entity\Question $question
     *
     * @return Campaign
     */
    public function addQuestion(\TraceBundle\Entity\Question $question)
    {
        $this->questions[] = $question;

        return $this;
    }

    /**
     * Remove question
     *
     * @param \TraceBundle\Entity\Question $question
     */
    public function removeQuestion(\TraceBundle\Entity\Question $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Add tag
     *
     * @param \TraceBundle\Entity\Tag $tag
     *
     * @return Campaign
     */
    public function addTag(\TraceBundle\Entity\Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \TraceBundle\Entity\Tag $tag
     */
    public function removeTag(\TraceBundle\Entity\Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return Campaign
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Campaign
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add campaigndocument
     *
     * @param \TraceBundle\Entity\Campaigndocument $campaigndocument
     *
     * @return Campaign
     */
    public function addCampaigndocument(\TraceBundle\Entity\Campaigndocument $campaigndocument)
    {
        $this->campaigndocuments[] = $campaigndocument;

        return $this;
    }

    /**
     * Remove campaigndocument
     *
     * @param \TraceBundle\Entity\Campaigndocument $campaigndocument
     */
    public function removeCampaigndocument(\TraceBundle\Entity\Campaigndocument $campaigndocument)
    {
        $this->campaigndocuments->removeElement($campaigndocument);
    }

    /**
     * Get campaigndocuments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaigndocuments()
    {
        return $this->campaigndocuments;
    }

    /**
     * Add campaignclient
     *
     * @param \TraceBundle\Entity\CampaignClient $campaignclient
     *
     * @return Campaign
     */
    public function addCampaignclient(\TraceBundle\Entity\CampaignClient $campaignclient)
    {
        $this->campaignclients[] = $campaignclient;

        return $this;
    }

    /**
     * Remove campaignclient
     *
     * @param \TraceBundle\Entity\CampaignClient $campaignclient
     */
    public function removeCampaignclient(\TraceBundle\Entity\CampaignClient $campaignclient)
    {
        $this->campaignclients->removeElement($campaignclient);
    }

    /**
     * Get campaignclients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaignclients()
    {
        return $this->campaignclients;
    }

    /**
     * Set client
     *
     * @param \TraceBundle\Entity\Client $client
     *
     * @return Campaign
     */
    public function setClient(\TraceBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \TraceBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Add applicant
     *
     * @param \TraceBundle\Entity\Applicant $applicant
     *
     * @return Campaign
     */
    public function addApplicant(\TraceBundle\Entity\Applicant $applicant)
    {
        $this->applicants[] = $applicant;

        return $this;
    }

    /**
     * Remove applicant
     *
     * @param \TraceBundle\Entity\Applicant $applicant
     */
    public function removeApplicant(\TraceBundle\Entity\Applicant $applicant)
    {
        $this->applicants->removeElement($applicant);
    }

    /**
     * Get applicants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApplicants()
    {
        return $this->applicants;
    }

    /**
     * Add campaignliveresponse
     *
     * @param \TraceBundle\Entity\Campaignliveresponse $campaignliveresponse
     *
     * @return Campaign
     */
    public function addCampaignliveresponse(\TraceBundle\Entity\Campaignliveresponse $campaignliveresponse)
    {
        $this->campaignliveresponses[] = $campaignliveresponse;

        return $this;
    }

    /**
     * Remove campaignliveresponse
     *
     * @param \TraceBundle\Entity\Campaignliveresponse $campaignliveresponse
     */
    public function removeCampaignliveresponse(\TraceBundle\Entity\Campaignliveresponse $campaignliveresponse)
    {
        $this->campaignliveresponses->removeElement($campaignliveresponse);
    }

    /**
     * Get campaignliveresponses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaignliveresponses()
    {
        return $this->campaignliveresponses;
    }
}
