<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Campaigndocumentresponse
 *
 * @ORM\Table(name="campaigndocumentresponse")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\CampaigndocumentresponseRepository")
 */
class Campaigndocumentresponse {
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * filename with extension eg. passport.jpeg
     * @var string
     * @ORM\Column(name="value", type="string", nullable=true)
     */
    protected $value;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="isuploaded", type="boolean", nullable=true)
     */
    protected $isuploaded;
    
    /**
     * filename without extension eg. passport
     * @ORM\Column(type="string", name="filename", length=100)
     */
    public $filename;
    
    /**
     * @Assert\NotBlank(message="Attachment cannot be blank", groups={"campaigndocs"}) 
     * @Assert\File(maxSize="5M", maxSizeMessage="Maximum size for file is 5MB", groups={"campaigndocs"})
     * @Assert\File(mimeTypes={"image/jpg","image/png","image/jpeg","image/gif","image/bmp","application/pdf","application/x-pdf"}, mimeTypesMessage="File format not supported", groups={"campaigndocs"})
     */
    protected $attachment;
    
    /**
     * @ORM\ManyToOne(targetEntity="Campaigndocument", inversedBy="campaigndocumentresponses", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="campaigndocument_id", referencedColumnName="id")
     */
    protected $campaigndocument;

    /**
     * @ORM\ManyToOne(targetEntity="Applicant", inversedBy="campaigndocumentresponses", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="applicant_id", referencedColumnName="id")
     */
    protected $applicant;

    public function getAttachment() {
        return $this->attachment;
    }

    public function setAttachment($attachment) {
        $this->attachment = $attachment;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Campaigndocumentresponse
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set campaigndocument
     *
     * @param \TraceBundle\Entity\Campaigndocument $campaigndocument
     *
     * @return Campaigndocumentresponse
     */
    public function setCampaigndocument(\TraceBundle\Entity\Campaigndocument $campaigndocument = null)
    {
        $this->campaigndocument = $campaigndocument;

        return $this;
    }

    /**
     * Get campaigndocument
     *
     * @return \TraceBundle\Entity\Campaigndocument
     */
    public function getCampaigndocument()
    {
        return $this->campaigndocument;
    }

    /**
     * Set applicant
     *
     * @param \TraceBundle\Entity\Applicant $applicant
     *
     * @return Campaigndocumentresponse
     */
    public function setApplicant(\TraceBundle\Entity\Applicant $applicant = null)
    {
        $this->applicant = $applicant;

        return $this;
    }

    /**
     * Get applicant
     *
     * @return \TraceBundle\Entity\Applicant
     */
    public function getApplicant()
    {
        return $this->applicant;
    }

    /**
     * Set isuploaded
     *
     * @param boolean $isuploaded
     *
     * @return Campaigndocumentresponse
     */
    public function setIsuploaded($isuploaded)
    {
        $this->isuploaded = $isuploaded;

        return $this;
    }

    /**
     * Get isuploaded
     *
     * @return boolean
     */
    public function getIsuploaded()
    {
        return $this->isuploaded;
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return Campaigndocumentresponse
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }
}
