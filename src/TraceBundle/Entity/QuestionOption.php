<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * QuestionOption
 *
 * @ORM\Table(name="questionoption")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\QuestionOptionRepository")
 */
class QuestionOption {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank(message="Option value cannot be blank", groups={"questions"})
     * @var string
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    protected $value;
    
    /**
     * @ORM\ManyToOne(targetEntity="Question", inversedBy="questionoptions")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    protected $question;

    function __clone() {
        $this->id = null;
        $this->question = null;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return QuestionOption
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set question
     *
     * @param \TraceBundle\Entity\Question $question
     *
     * @return QuestionOption
     */
    public function setQuestion(\TraceBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \TraceBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }
}
