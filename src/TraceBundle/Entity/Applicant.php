<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Applicant
 *
 * @ORM\Table(name="applicant")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\ApplicantRepository")
 */
class Applicant {
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Firstname cannot be blank", groups={"addapplicantpage"})
     * @Assert\Length(min=4,max=100,minMessage="Firstname should be minimum {{ limit }} characters", maxMessage="Your Firstname cannot be longer than {{ limit }} characters",groups={"addapplicantpage"}
     * )
     * @Assert\Regex(
     *      pattern="/[^A-Za-zà-ÿÀ-Ÿ .\']/i",
     *      match=false,
     *      message="Firstname allows only a-z,A-Z and few special characters (. ')",groups={"addapplicantpage"})
     * @ORM\Column(name="firstname", type="string", length=100, nullable=false)
     */
    protected $firstname;

    /**
     * @var string
     * @Assert\NotBlank(message="Lastname cannot be blank", groups={"addapplicantpage"})
     * @Assert\Length(min=4,max=100,minMessage="Lastname should be minimum {{ limit }} characters", maxMessage="Your Lastname cannot be longer than {{ limit }} characters",groups={"addapplicantpage"}
     * )
     * @Assert\Regex(
     *      pattern="/[^A-Za-zà-ÿÀ-Ÿ .\']/i",
     *      match=false,
     *      message="Lastname allows only a-z,A-Z and few special characters (. ')",groups={"addapplicantpage"})
     * @ORM\Column(name="lastname", type="string", length=100, nullable=false)
     */
    protected $lastname;
    
    /**
     * @var string
     * @Assert\Email(message = "Email not valid", groups={"addapplicantpage"})
     * @Assert\NotBlank(message = "Email cannot be blank", groups={"addapplicantpage"})
     * @Assert\Length(max=320,maxMessage="Email cannot exceeds 320 characters", groups={"addapplicantpage"})
     * @ORM\Column(name="email", type="string", length=320, nullable=true)
     */
    protected $email;
    
    /**
     * @var string
     * @Assert\Length(max=16,maxMessage="Email cannot exceeds 16 characters", groups={"addapplicantpage"})
     * @ORM\Column(name="phonenumber", type="string", length=16, nullable=true)
     */
    protected $phonenumber;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="invitedate", type="datetime", nullable=true)
     */
    protected $invitedate;
    
    /**
     * @var string
     *
     * @ORM\Column(name="inviteurl", type="string", length=2083, nullable=true)
     */
    protected $inviteurl;

    /**
     * @var string
     *
     * @ORM\Column(name="uniqueinvitecode", type="string", length=10, nullable=true, unique=true)
     */
    protected $uniqueinvitecode;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expirydate", type="datetime", nullable=true)
     */
    protected $expirydate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="extensionrequesttime", type="datetime", nullable=true)
     */
    protected $extensionrequesttime;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="isextended", type="boolean", nullable=true)
     */
    protected $isextended;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="isextensionrequested", type="boolean", nullable=true)
     */
    protected $isextensionrequested=false;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="isuniquecodeerror", type="boolean", nullable=true)
     */
    protected $isuniquecodeerror=false;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="allowduplicate", type="boolean", nullable=true)
     */
    protected $allowduplicate=false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="statusupdatedon", type="datetime", nullable=true)
     */
    protected $statusupdatedon;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdon", type="datetime", nullable=true)
     */
    protected $createdon;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedon", type="datetime", nullable=true)
     */
    protected $updatedon;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isansweredall", type="boolean", nullable=true)
     */
    protected $isansweredall=false;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="isuploadedalldocs", type="boolean", nullable=true)
     */
    protected $isuploadedalldocs=false;
    
    /**
     * @var string
     * @Assert\NotBlank(message="Reason cannot be blank", groups={"extensionrequest"})
     * @ORM\Column(name="reason", type="text", nullable=true)
     */
    protected $reason;
    
    /**
     * @var float
     * @ORM\Column(name="averagerating", type="float", precision=6, scale=2, nullable=true)
     * @Assert\Range(
     *      min = 0.00,
     *      max = 5.00,
     *      minMessage = "The average rating cannot be less than {{ limit }}",
     *      maxMessage = "The average rating cannot be greater than {{ limit }}"
     * )
     */
    protected $averagerating;
    
    /**
     * @var int
     * 
     * @ORM\Column(name="liveinterviewduration", type="integer", nullable=true)
     */
    protected $liveinterviewduration;
    
    /**
     * @ORM\ManyToOne(targetEntity="Campaign", inversedBy="applicants", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    protected $campaign;
    
    /**
     * @ORM\ManyToOne(targetEntity="Tenant", inversedBy="applicants", cascade={"persist"})
     * @ORM\JoinColumn(name="tenant_id", referencedColumnName="id")
     */
    protected $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="Applicantstatus", inversedBy="applicants", cascade={"persist"})
     * @ORM\JoinColumn(name="applicantstatus_id", referencedColumnName="id")
     */
    protected $applicantstatus;

    /**
     * @ORM\ManyToOne(targetEntity="Interviewtype", inversedBy="applicants", cascade={"persist"})
     * @ORM\JoinColumn(name="interviewtype_id", referencedColumnName="id")
     */
    protected $interviewtype;

    /**
     * @ORM\OneToMany(targetEntity="Campaignquestionresponse", mappedBy="applicant")
     */
    protected $campaignquestionresponses;
  
    /**
     * @ORM\OneToMany(targetEntity="Campaigndocumentresponse", mappedBy="applicant")
     */
    protected $campaigndocumentresponses;
    
    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="applicant")
     */
    protected $comments;

    /**
     * @ORM\OneToMany(targetEntity="ApplicantEvaluation", mappedBy="applicant")
     */
    protected $applicantevaluations;
    
    /**
     * @ORM\OneToMany(targetEntity="Practicerecording", mappedBy="applicant")
     */
    protected $practicerecordings;
    
    public function __construct() {
        $this->campaignquestionresponses = new ArrayCollection();
        $this->campaigndocumentresponses = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->applicantevaluations = new ArrayCollection();
        $this->practicerecordings = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Applicant
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return Applicant
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Applicant
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phonenumber
     *
     * @param string $phonenumber
     *
     * @return Applicant
     */
    public function setPhonenumber($phonenumber)
    {
        $this->phonenumber = $phonenumber;

        return $this;
    }

    /**
     * Get phonenumber
     *
     * @return string
     */
    public function getPhonenumber()
    {
        return $this->phonenumber;
    }

    /**
     * Set invitedate
     *
     * @param \DateTime $invitedate
     *
     * @return Applicant
     */
    public function setInvitedate($invitedate)
    {
        $this->invitedate = $invitedate;

        return $this;
    }

    /**
     * Get invitedate
     *
     * @return \DateTime
     */
    public function getInvitedate()
    {
        return $this->invitedate;
    }

    /**
     * Set inviteurl
     *
     * @param string $inviteurl
     *
     * @return Applicant
     */
    public function setInviteurl($inviteurl)
    {
        $this->inviteurl = $inviteurl;

        return $this;
    }

    /**
     * Get inviteurl
     *
     * @return string
     */
    public function getInviteurl()
    {
        return $this->inviteurl;
    }

    /**
     * Set uniqueinvitecode
     *
     * @param string $uniqueinvitecode
     *
     * @return Applicant
     */
    public function setUniqueinvitecode($uniqueinvitecode)
    {
        $this->uniqueinvitecode = $uniqueinvitecode;

        return $this;
    }

    /**
     * Get uniqueinvitecode
     *
     * @return string
     */
    public function getUniqueinvitecode()
    {
        return $this->uniqueinvitecode;
    }

    /**
     * Set expirydate
     *
     * @param \DateTime $expirydate
     *
     * @return Applicant
     */
    public function setExpirydate($expirydate)
    {
        $this->expirydate = $expirydate;

        return $this;
    }

    /**
     * Get expirydate
     *
     * @return \DateTime
     */
    public function getExpirydate()
    {
        return $this->expirydate;
    }

    /**
     * Set isextended
     *
     * @param boolean $isextended
     *
     * @return Applicant
     */
    public function setIsextended($isextended)
    {
        $this->isextended = $isextended;

        return $this;
    }

    /**
     * Get isextended
     *
     * @return boolean
     */
    public function getIsextended()
    {
        return $this->isextended;
    }

    /**
     * Set allowduplicate
     *
     * @param boolean $allowduplicate
     *
     * @return Applicant
     */
    public function setAllowduplicate($allowduplicate)
    {
        $this->allowduplicate = $allowduplicate;

        return $this;
    }

    /**
     * Get allowduplicate
     *
     * @return boolean
     */
    public function getAllowduplicate()
    {
        return $this->allowduplicate;
    }

    /**
     * Set createdon
     *
     * @param \DateTime $createdon
     *
     * @return Applicant
     */
    public function setCreatedon($createdon)
    {
        $this->createdon = $createdon;

        return $this;
    }

    /**
     * Get createdon
     *
     * @return \DateTime
     */
    public function getCreatedon()
    {
        return $this->createdon;
    }

    /**
     * Set updatedon
     *
     * @param \DateTime $updatedon
     *
     * @return Applicant
     */
    public function setUpdatedon($updatedon)
    {
        $this->updatedon = $updatedon;

        return $this;
    }

    /**
     * Get updatedon
     *
     * @return \DateTime
     */
    public function getUpdatedon()
    {
        return $this->updatedon;
    }

    /**
     * Set campaign
     *
     * @param \TraceBundle\Entity\Campaign $campaign
     *
     * @return Applicant
     */
    public function setCampaign(\TraceBundle\Entity\Campaign $campaign = null)
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * Get campaign
     *
     * @return \TraceBundle\Entity\Campaign
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * Set tenant
     *
     * @param \TraceBundle\Entity\Tenant $tenant
     *
     * @return Applicant
     */
    public function setTenant(\TraceBundle\Entity\Tenant $tenant = null)
    {
        $this->tenant = $tenant;

        return $this;
    }

    /**
     * Get tenant
     *
     * @return \TraceBundle\Entity\Tenant
     */
    public function getTenant()
    {
        return $this->tenant;
    }

    /**
     * Set applicantstatus
     *
     * @param \TraceBundle\Entity\Applicantstatus $applicantstatus
     *
     * @return Applicant
     */
    public function setApplicantstatus(\TraceBundle\Entity\Applicantstatus $applicantstatus = null)
    {
        $this->applicantstatus = $applicantstatus;

        return $this;
    }

    /**
     * Get applicantstatus
     *
     * @return \TraceBundle\Entity\Applicantstatus
     */
    public function getApplicantstatus()
    {
        return $this->applicantstatus;
    }

    /**
     * Set interviewtype
     *
     * @param \TraceBundle\Entity\Interviewtype $interviewtype
     *
     * @return Applicant
     */
    public function setInterviewtype(\TraceBundle\Entity\Interviewtype $interviewtype = null)
    {
        $this->interviewtype = $interviewtype;

        return $this;
    }

    /**
     * Get interviewtype
     *
     * @return \TraceBundle\Entity\Interviewtype
     */
    public function getInterviewtype()
    {
        return $this->interviewtype;
    }

    /**
     * Set isuniquecodeerror
     *
     * @param boolean $isuniquecodeerror
     *
     * @return Applicant
     */
    public function setIsuniquecodeerror($isuniquecodeerror)
    {
        $this->isuniquecodeerror = $isuniquecodeerror;

        return $this;
    }

    /**
     * Get isuniquecodeerror
     *
     * @return boolean
     */
    public function getIsuniquecodeerror()
    {
        return $this->isuniquecodeerror;
    }

    /**
     * Add campaignquestionresponse
     *
     * @param \TraceBundle\Entity\Campaignquestionresponse $campaignquestionresponse
     *
     * @return Applicant
     */
    public function addCampaignquestionresponse(\TraceBundle\Entity\Campaignquestionresponse $campaignquestionresponse)
    {
        $this->campaignquestionresponses[] = $campaignquestionresponse;

        return $this;
    }

    /**
     * Remove campaignquestionresponse
     *
     * @param \TraceBundle\Entity\Campaignquestionresponse $campaignquestionresponse
     */
    public function removeCampaignquestionresponse(\TraceBundle\Entity\Campaignquestionresponse $campaignquestionresponse)
    {
        $this->campaignquestionresponses->removeElement($campaignquestionresponse);
    }

    /**
     * Get campaignquestionresponses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaignquestionresponses()
    {
        return $this->campaignquestionresponses;
    }

    /**
     * Add campaigndocumentresponse
     *
     * @param \TraceBundle\Entity\Campaigndocumentresponse $campaigndocumentresponse
     *
     * @return Applicant
     */
    public function addCampaigndocumentresponse(\TraceBundle\Entity\Campaigndocumentresponse $campaigndocumentresponse)
    {
        $this->campaigndocumentresponses[] = $campaigndocumentresponse;

        return $this;
    }

    /**
     * Remove campaigndocumentresponse
     *
     * @param \TraceBundle\Entity\Campaigndocumentresponse $campaigndocumentresponse
     */
    public function removeCampaigndocumentresponse(\TraceBundle\Entity\Campaigndocumentresponse $campaigndocumentresponse)
    {
        $this->campaigndocumentresponses->removeElement($campaigndocumentresponse);
    }

    /**
     * Get campaigndocumentresponses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaigndocumentresponses()
    {
        return $this->campaigndocumentresponses;
    }

    /**
     * Set isansweredall
     *
     * @param boolean $isansweredall
     *
     * @return Applicant
     */
    public function setIsansweredall($isansweredall)
    {
        $this->isansweredall = $isansweredall;

        return $this;
    }

    /**
     * Get isansweredall
     *
     * @return boolean
     */
    public function getIsansweredall()
    {
        return $this->isansweredall;
    }

    /**
     * Set isuploadedalldocs
     *
     * @param boolean $isuploadedalldocs
     *
     * @return Applicant
     */
    public function setIsuploadedalldocs($isuploadedalldocs)
    {
        $this->isuploadedalldocs = $isuploadedalldocs;

        return $this;
    }

    /**
     * Get isuploadedalldocs
     *
     * @return boolean
     */
    public function getIsuploadedalldocs()
    {
        return $this->isuploadedalldocs;
    }

    /**
     * Set extensionrequesttime
     *
     * @param \DateTime $extensionrequesttime
     *
     * @return Applicant
     */
    public function setExtensionrequesttime($extensionrequesttime)
    {
        $this->extensionrequesttime = $extensionrequesttime;

        return $this;
    }

    /**
     * Get extensionrequesttime
     *
     * @return \DateTime
     */
    public function getExtensionrequesttime()
    {
        return $this->extensionrequesttime;
    }

    /**
     * Set isextensionrequested
     *
     * @param boolean $isextensionrequested
     *
     * @return Applicant
     */
    public function setIsextensionrequested($isextensionrequested)
    {
        $this->isextensionrequested = $isextensionrequested;

        return $this;
    }

    /**
     * Get isextensionrequested
     *
     * @return boolean
     */
    public function getIsextensionrequested()
    {
        return $this->isextensionrequested;
    }

    /**
     * Set reason
     *
     * @param string $reason
     *
     * @return Applicant
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Add comment
     *
     * @param \TraceBundle\Entity\Comment $comment
     *
     * @return Applicant
     */
    public function addComment(\TraceBundle\Entity\Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \TraceBundle\Entity\Comment $comment
     */
    public function removeComment(\TraceBundle\Entity\Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Add applicantevaluation
     *
     * @param \TraceBundle\Entity\ApplicantEvaluation $applicantevaluation
     *
     * @return Applicant
     */
    public function addApplicantevaluation(\TraceBundle\Entity\ApplicantEvaluation $applicantevaluation)
    {
        $this->applicantevaluations[] = $applicantevaluation;

        return $this;
    }

    /**
     * Remove applicantevaluation
     *
     * @param \TraceBundle\Entity\ApplicantEvaluation $applicantevaluation
     */
    public function removeApplicantevaluation(\TraceBundle\Entity\ApplicantEvaluation $applicantevaluation)
    {
        $this->applicantevaluations->removeElement($applicantevaluation);
    }

    /**
     * Get applicantevaluations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApplicantevaluations()
    {
        return $this->applicantevaluations;
    }

    /**
     * Set averagerating
     *
     * @param float $averagerating
     *
     * @return Applicant
     */
    public function setAveragerating($averagerating)
    {
        $this->averagerating = $averagerating;

        return $this;
    }

    /**
     * Get averagerating
     *
     * @return float
     */
    public function getAveragerating()
    {
        return $this->averagerating;
    }

    /**
     * Set statusupdatedon
     *
     * @param \DateTime $statusupdatedon
     *
     * @return Applicant
     */
    public function setStatusupdatedon($statusupdatedon)
    {
        $this->statusupdatedon = $statusupdatedon;

        return $this;
    }

    /**
     * Get statusupdatedon
     *
     * @return \DateTime
     */
    public function getStatusupdatedon()
    {
        return $this->statusupdatedon;
    }

    /**
     * Add practicerecording
     *
     * @param \TraceBundle\Entity\Practicerecording $practicerecording
     *
     * @return Applicant
     */
    public function addPracticerecording(\TraceBundle\Entity\Practicerecording $practicerecording)
    {
        $this->practicerecordings[] = $practicerecording;

        return $this;
    }

    /**
     * Remove practicerecording
     *
     * @param \TraceBundle\Entity\Practicerecording $practicerecording
     */
    public function removePracticerecording(\TraceBundle\Entity\Practicerecording $practicerecording)
    {
        $this->practicerecordings->removeElement($practicerecording);
    }

    /**
     * Get practicerecordings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticerecordings()
    {
        return $this->practicerecordings;
    }

    /**
     * Set liveinterviewduration
     *
     * @param integer $liveinterviewduration
     *
     * @return Applicant
     */
    public function setLiveinterviewduration($liveinterviewduration)
    {
        $this->liveinterviewduration = $liveinterviewduration;

        return $this;
    }

    /**
     * Get liveinterviewduration
     *
     * @return integer
     */
    public function getLiveinterviewduration()
    {
        return $this->liveinterviewduration;
    }
    
//    /**
//     * This function validates for unique email and campaignid
//     * @Assert\Callback(groups={"addapplicantpage"})
//     */
//    public function uniqueemailandcampaignValidate(ExecutionContextInterface $context, $payload) {
//        $count=0;
//        $campaign = $this->campaign;
//        $email = $this->email;
//        $applicants = $campaign->getApplicants();
//        foreach($applicants as $applicant){
//            $existingemail = $applicant->getEmail();
//            if($existingemail==$email){
//                $count++;
//                break;
//            }
//        }
//        
//        if($count>0){
//            $context->buildViolation('This email is already registered')
//                    ->atPath('email')
//                    ->addViolation();
//        }
//        $count=0;
//    }
}
