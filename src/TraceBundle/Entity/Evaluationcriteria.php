<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Evaluationcriteria
 *
 * @ORM\Table(name="evaluationcriteria")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\EvaluationcriteriaRepository")
 */
class Evaluationcriteria {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Criterion cannot be blank", groups={"questions"})
     * @ORM\Column(name="criterion", type="string", length=400, nullable=true)
     * @Assert\Length(max=300,maxMessage="Criterion cannot exceed 300 characters", groups={"questions"})
     */
    protected $criterion;
    
    /**
     * @ORM\ManyToOne(targetEntity="Campaign", inversedBy="evaluationcriteria", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    protected $campaign;
    
    /**
     * @ORM\ManyToOne(targetEntity="Tenant", inversedBy="evaluationcriteria", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="tenant_id", referencedColumnName="id")
     */
    protected $tenant;
    
    /**
     * @ORM\OneToMany(targetEntity="ApplicantEvaluation", mappedBy="evaluationcriterion")
     */
    protected $applicantevaluations;
    
    public function __construct() {
        $this->applicantevaluations = new ArrayCollection();
    }

    function __clone() {
        $this->id = null;
        $this->campaign = null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set criterion
     *
     * @param string $criterion
     *
     * @return Evaluationcriteria
     */
    public function setCriterion($criterion)
    {
        $this->criterion = $criterion;

        return $this;
    }

    /**
     * Get criterion
     *
     * @return string
     */
    public function getCriterion()
    {
        return $this->criterion;
    }

    /**
     * Set campaign
     *
     * @param \TraceBundle\Entity\Campaign $campaign
     *
     * @return Evaluationcriteria
     */
    public function setCampaign(\TraceBundle\Entity\Campaign $campaign = null)
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * Get campaign
     *
     * @return \TraceBundle\Entity\Campaign
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * Set tenant
     *
     * @param \TraceBundle\Entity\Tenant $tenant
     *
     * @return Evaluationcriteria
     */
    public function setTenant(\TraceBundle\Entity\Tenant $tenant = null)
    {
        $this->tenant = $tenant;

        return $this;
    }

    /**
     * Get tenant
     *
     * @return \TraceBundle\Entity\Tenant
     */
    public function getTenant()
    {
        return $this->tenant;
    }

    /**
     * Add applicantevaluation
     *
     * @param \TraceBundle\Entity\ApplicantEvaluation $applicantevaluation
     *
     * @return Evaluationcriteria
     */
    public function addApplicantevaluation(\TraceBundle\Entity\ApplicantEvaluation $applicantevaluation)
    {
        $this->applicantevaluations[] = $applicantevaluation;

        return $this;
    }

    /**
     * Remove applicantevaluation
     *
     * @param \TraceBundle\Entity\ApplicantEvaluation $applicantevaluation
     */
    public function removeApplicantevaluation(\TraceBundle\Entity\ApplicantEvaluation $applicantevaluation)
    {
        $this->applicantevaluations->removeElement($applicantevaluation);
    }

    /**
     * Get applicantevaluations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApplicantevaluations()
    {
        return $this->applicantevaluations;
    }
}
