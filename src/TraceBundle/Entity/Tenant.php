<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use TraceBundle\Entity\Client;
use TraceBundle\Entity\Language;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tenant
 *
 * @ORM\Table(name="tenant")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\TenantRepository")
 */
class Tenant {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * 
     * @ORM\OneToMany(targetEntity="Client", mappedBy="tenant")
     */
    protected $clients;
    
    
    /**
     * Many Tenant have One Language.
     * @ORM\ManyToOne(targetEntity="Language", inversedBy="tenants")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    protected $language;
    
    /**
     * @ORM\OneToMany(targetEntity="Campaign", mappedBy="tenant")
     */
    protected $campaigns;
    
    /**
     * @ORM\OneToMany(targetEntity="Evaluationcriteria", mappedBy="tenant")
     */
    protected $evaluationcriteria;
    
    /**
     * @ORM\OneToMany(targetEntity="Question", mappedBy="tenant")
     */
    protected $questions;
    
    /**
     * @ORM\OneToMany(targetEntity="Document", mappedBy="tenant")
     */
    protected $documents;
    
    /**
     * @ORM\OneToMany(targetEntity="Applicant", mappedBy="tenant")
     */
    protected $applicants;
    
    public function __construct() {
        $this->clients = new ArrayCollection();
        $this->campaigns = new ArrayCollection();
        $this->evaluationcriteria = new ArrayCollection();
        $this->questions = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }
    
    /**
     * 
     * @Assert\NotBlank( message = "Name cannot be blank", groups={"registrationpage"})
     * @Assert\Regex(
     *      pattern="/[^a-zA-Z0-9-_]/", 
     *      match = false,
     *      message = "Name allows only a-z,A-Z,0-9,_", groups={"registrationpage"})
     * @Assert\Length(min=4,max=30,minMessage="Name should be minimum {{ limit }} characters",maxMessage="Name cannot exceeds {{ limit }} characters",groups={"registrationpage"})
     * @ORM\Column(name="name", type="text", length=100, nullable=false)
     */
    protected $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="officialwebsiteurl", type="string", length=2083, nullable=true)
     */
    protected $officialwebsiteurl;
    
    /**
     * @var string
     *
     * @ORM\Column(name="tenantsubdomainurl", type="string", length=2083, nullable=true)
     */
    protected $tenantsubdomainurl;
    
    /**
     * @var string
     *
     * @ORM\Column(name="logourl", type="string", length=2083, nullable=true)
     */
    protected $logourl;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdon", type="datetime", nullable=true)
     */
    protected $createdon;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedon", type="datetime", nullable=true)
     */
    protected $updatedon;
            
        
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tenant
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set officialwebsiteurl
     *
     * @param string $officialwebsiteurl
     *
     * @return Tenant
     */
    public function setOfficialwebsiteurl($officialwebsiteurl)
    {
        $this->officialwebsiteurl = $officialwebsiteurl;

        return $this;
    }

    /**
     * Get officialwebsiteurl
     *
     * @return string
     */
    public function getOfficialwebsiteurl()
    {
        return $this->officialwebsiteurl;
    }

    /**
     * Set tenantsubdomainurl
     *
     * @param string $tenantsubdomainurl
     *
     * @return Tenant
     */
    public function setTenantsubdomainurl($tenantsubdomainurl)
    {
        $this->tenantsubdomainurl = $tenantsubdomainurl;

        return $this;
    }

    /**
     * Get tenantsubdomainurl
     *
     * @return string
     */
    public function getTenantsubdomainurl()
    {
        return $this->tenantsubdomainurl;
    }

    /**
     * Set logourl
     *
     * @param string $logourl
     *
     * @return Tenant
     */
    public function setLogourl($logourl)
    {
        $this->logourl = $logourl;

        return $this;
    }

    /**
     * Get logourl
     *
     * @return string
     */
    public function getLogourl()
    {
        return $this->logourl;
    }

    /**
     * Set createdon
     *
     * @param \DateTime $createdon
     *
     * @return Tenant
     */
    public function setCreatedon($createdon)
    {
        $this->createdon = $createdon;

        return $this;
    }

    /**
     * Get createdon
     *
     * @return \DateTime
     */
    public function getCreatedon()
    {
        return $this->createdon;
    }

    /**
     * Set updatedon
     *
     * @param \DateTime $updatedon
     *
     * @return Tenant
     */
    public function setUpdatedon($updatedon)
    {
        $this->updatedon = $updatedon;

        return $this;
    }

    /**
     * Get updatedon
     *
     * @return \DateTime
     */
    public function getUpdatedon()
    {
        return $this->updatedon;
    }

    /**
     * Add client
     *
     * @param \TraceBundle\Entity\Client $client
     *
     * @return Tenant
     */
    public function addClient(\TraceBundle\Entity\Client $client)
    {
        $this->clients[] = $client;

        return $this;
    }

    /**
     * Remove client
     *
     * @param \TraceBundle\Entity\Client $client
     */
    public function removeClient(\TraceBundle\Entity\Client $client)
    {
        $this->clients->removeElement($client);
    }

    /**
     * Get clients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * Set language
     *
     * @param \TraceBundle\Entity\Language $language
     *
     * @return Tenant
     */
    public function setLanguage(\TraceBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \TraceBundle\Entity\Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Add campaign
     *
     * @param \TraceBundle\Entity\Campaign $campaign
     *
     * @return Tenant
     */
    public function addCampaign(\TraceBundle\Entity\Campaign $campaign)
    {
        $this->campaigns[] = $campaign;

        return $this;
    }

    /**
     * Remove campaign
     *
     * @param \TraceBundle\Entity\Campaign $campaign
     */
    public function removeCampaign(\TraceBundle\Entity\Campaign $campaign)
    {
        $this->campaigns->removeElement($campaign);
    }

    /**
     * Get campaigns
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }

    /**
     * Add evaluationcriterium
     *
     * @param \TraceBundle\Entity\Evaluationcriteria $evaluationcriterium
     *
     * @return Tenant
     */
    public function addEvaluationcriterium(\TraceBundle\Entity\Evaluationcriteria $evaluationcriterium)
    {
        $this->evaluationcriteria[] = $evaluationcriterium;

        return $this;
    }

    /**
     * Remove evaluationcriterium
     *
     * @param \TraceBundle\Entity\Evaluationcriteria $evaluationcriterium
     */
    public function removeEvaluationcriterium(\TraceBundle\Entity\Evaluationcriteria $evaluationcriterium)
    {
        $this->evaluationcriteria->removeElement($evaluationcriterium);
    }

    /**
     * Get evaluationcriteria
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvaluationcriteria()
    {
        return $this->evaluationcriteria;
    }

    /**
     * Add question
     *
     * @param \TraceBundle\Entity\Question $question
     *
     * @return Tenant
     */
    public function addQuestion(\TraceBundle\Entity\Question $question)
    {
        $this->questions[] = $question;

        return $this;
    }

    /**
     * Remove question
     *
     * @param \TraceBundle\Entity\Question $question
     */
    public function removeQuestion(\TraceBundle\Entity\Question $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Add document
     *
     * @param \TraceBundle\Entity\Document $document
     *
     * @return Tenant
     */
    public function addDocument(\TraceBundle\Entity\Document $document)
    {
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \TraceBundle\Entity\Document $document
     */
    public function removeDocument(\TraceBundle\Entity\Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Add applicant
     *
     * @param \TraceBundle\Entity\Applicant $applicant
     *
     * @return Tenant
     */
    public function addApplicant(\TraceBundle\Entity\Applicant $applicant)
    {
        $this->applicants[] = $applicant;

        return $this;
    }

    /**
     * Remove applicant
     *
     * @param \TraceBundle\Entity\Applicant $applicant
     */
    public function removeApplicant(\TraceBundle\Entity\Applicant $applicant)
    {
        $this->applicants->removeElement($applicant);
    }

    /**
     * Get applicants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApplicants()
    {
        return $this->applicants;
    }
}
