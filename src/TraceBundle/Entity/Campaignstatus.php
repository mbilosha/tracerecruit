<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Campaignstatus
 *
 * @ORM\Table(name="campaignstatus")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\CampaignstatusRepository")
 */
class Campaignstatus {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Campaign Status cannot be blank")
     * @ORM\Column(name="value", type="string", length=60, nullable=true)
     */
    protected $value;

    /**
     * @ORM\OneToMany(targetEntity="Campaign", mappedBy="campaignstatus")
     */
    protected $campaigns;

    public function __construct() {
        $this->campaigns = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Campaignstatus
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Add campaign
     *
     * @param \TraceBundle\Entity\Campaign $campaign
     *
     * @return Campaignstatus
     */
    public function addCampaign(\TraceBundle\Entity\Campaign $campaign)
    {
        $this->campaigns[] = $campaign;

        return $this;
    }

    /**
     * Remove campaign
     *
     * @param \TraceBundle\Entity\Campaign $campaign
     */
    public function removeCampaign(\TraceBundle\Entity\Campaign $campaign)
    {
        $this->campaigns->removeElement($campaign);
    }

    /**
     * Get campaigns
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }
}
