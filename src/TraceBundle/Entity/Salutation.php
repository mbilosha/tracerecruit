<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use TraceBundle\Entity\Client;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Salutation
 *
 * @ORM\Table(name="salutation")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\SalutationRepository")
 */
class Salutation {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     * @Assert\NotBlank(message="Salutation cannot be blank", groups={"salutation"})
     * @ORM\Column(name="value", type="string", length=65, nullable=true)
     * @Assert\Length(max=65,maxMessage="Salutation cannot exceeds 65 characters", groups={"salutation"})
     */
    protected $value;
    
    /**
     * 
     * @ORM\OneToMany(targetEntity="Client", mappedBy="salutation")
     */
    protected $clients;
    
    public function __construct() {
        $this->clients = new ArrayCollection();
    }
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Salutation
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Add client
     *
     * @param \TraceBundle\Entity\Client $client
     *
     * @return Salutation
     */
    public function addClient(\TraceBundle\Entity\Client $client)
    {
        $this->clients[] = $client;

        return $this;
    }

    /**
     * Remove client
     *
     * @param \TraceBundle\Entity\Client $client
     */
    public function removeClient(\TraceBundle\Entity\Client $client)
    {
        $this->clients->removeElement($client);
    }

    /**
     * Get clients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClients()
    {
        return $this->clients;
    }
}
