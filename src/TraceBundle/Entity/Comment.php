<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Comment
 *
 * @ORM\Table(name="comment")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\CommentRepository")
 */
class Comment { 
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     * @Assert\NotBlank(message="Comment cannot be blank")
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    protected $value;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="commented_on", type="datetime", nullable=true)
     */
    protected $commentedon;
    
    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="comments", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="commentator_id", referencedColumnName="id")
     */
    protected $client;

    /**
     * @ORM\ManyToOne(targetEntity="Applicant", inversedBy="comments", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="applicant_id", referencedColumnName="id")
     */
    protected $applicant;

    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Comment
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set commentedon
     *
     * @param \DateTime $commentedon
     *
     * @return Comment
     */
    public function setCommentedon($commentedon)
    {
        $this->commentedon = $commentedon;

        return $this;
    }

    /**
     * Get commentedon
     *
     * @return \DateTime
     */
    public function getCommentedon()
    {
        return $this->commentedon;
    }

    /**
     * Set client
     *
     * @param \TraceBundle\Entity\Client $client
     *
     * @return Comment
     */
    public function setClient(\TraceBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \TraceBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set applicant
     *
     * @param \TraceBundle\Entity\Applicant $applicant
     *
     * @return Comment
     */
    public function setApplicant(\TraceBundle\Entity\Applicant $applicant = null)
    {
        $this->applicant = $applicant;

        return $this;
    }

    /**
     * Get applicant
     *
     * @return \TraceBundle\Entity\Applicant
     */
    public function getApplicant()
    {
        return $this->applicant;
    }
}
