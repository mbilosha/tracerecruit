<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Documenttype
 *
 * @ORM\Table(name="documenttype")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\DocumenttypeRepository")
 */
class Documenttype {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="value", type="string", length=60, nullable=true)
     */
    protected $value;
    
    /**
     * @ORM\OneToMany(targetEntity="Document", mappedBy="documenttype")
     */
    protected $documents;

    public function __construct() {
        $this->documents = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Documenttype
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Add document
     *
     * @param \TraceBundle\Entity\Document $document
     *
     * @return Documenttype
     */
    public function addDocument(\TraceBundle\Entity\Document $document)
    {
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \TraceBundle\Entity\Document $document
     */
    public function removeDocument(\TraceBundle\Entity\Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }
}
