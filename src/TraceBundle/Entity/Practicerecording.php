<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Practicerecording
 *
 * @ORM\Table(name="practicerecording")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\PracticerecordingRepository")
 */
class Practicerecording { 
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     * @ORM\Column(name="payload", type="string", length=60, nullable=true)
     */
    protected $payload;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=2083, nullable=true)
     */
    protected $url;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdon", type="datetime", nullable=true)
     */
    protected $createdon;

    /**
     * @ORM\ManyToOne(targetEntity="Applicant", inversedBy="practicerecordings", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="applicant_id", referencedColumnName="id")
     */
    protected $applicant;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set payload
     *
     * @param string $payload
     *
     * @return Practicerecording
     */
    public function setPayload($payload)
    {
        $this->payload = $payload;

        return $this;
    }

    /**
     * Get payload
     *
     * @return string
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Practicerecording
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set createdon
     *
     * @param \DateTime $createdon
     *
     * @return Practicerecording
     */
    public function setCreatedon($createdon)
    {
        $this->createdon = $createdon;

        return $this;
    }

    /**
     * Get createdon
     *
     * @return \DateTime
     */
    public function getCreatedon()
    {
        return $this->createdon;
    }

    /**
     * Set applicant
     *
     * @param \TraceBundle\Entity\Applicant $applicant
     *
     * @return Practicerecording
     */
    public function setApplicant(\TraceBundle\Entity\Applicant $applicant = null)
    {
        $this->applicant = $applicant;

        return $this;
    }

    /**
     * Get applicant
     *
     * @return \TraceBundle\Entity\Applicant
     */
    public function getApplicant()
    {
        return $this->applicant;
    }
}
