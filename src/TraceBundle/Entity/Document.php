<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Document
 *
 * @ORM\Table(name="document")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\DocumentRepository")
 */
class Document {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="documentname", type="string", length=300, nullable=true)
     */
    protected $documentname;
    
    /**
     * @ORM\ManyToOne(targetEntity="Tenant", inversedBy="documents", cascade={"persist"})
     * @ORM\JoinColumn(name="tenant_id", referencedColumnName="id")
     */
    protected $tenant;
    
    /**
     * @ORM\ManyToOne(targetEntity="Documenttype", inversedBy="documents")
     * @ORM\JoinColumn(name="documenttype_id", referencedColumnName="id")
     */
    protected $documenttype;
    
    /**
     * @ORM\ManyToMany(targetEntity="Question", mappedBy="documents")
     */
    private $questions;

    public function __construct() {
        $this->questions = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set documentname
     *
     * @param string $documentname
     *
     * @return Document
     */
    public function setDocumentname($documentname)
    {
        $this->documentname = $documentname;

        return $this;
    }

    /**
     * Get documentname
     *
     * @return string
     */
    public function getDocumentname()
    {
        return $this->documentname;
    }

    /**
     * Set documenttype
     *
     * @param \TraceBundle\Entity\Documenttype $documenttype
     *
     * @return Document
     */
    public function setDocumenttype(\TraceBundle\Entity\Documenttype $documenttype = null)
    {
        $this->documenttype = $documenttype;

        return $this;
    }

    /**
     * Get documenttype
     *
     * @return \TraceBundle\Entity\Documenttype
     */
    public function getDocumenttype()
    {
        return $this->documenttype;
    }

    /**
     * Add question
     *
     * @param \TraceBundle\Entity\Question $question
     *
     * @return Document
     */
    public function addQuestion(\TraceBundle\Entity\Question $question)
    {
        $this->questions[] = $question;

        return $this;
    }

    /**
     * Remove question
     *
     * @param \TraceBundle\Entity\Question $question
     */
    public function removeQuestion(\TraceBundle\Entity\Question $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Set tenant
     *
     * @param \TraceBundle\Entity\Tenant $tenant
     *
     * @return Document
     */
    public function setTenant(\TraceBundle\Entity\Tenant $tenant = null)
    {
        $this->tenant = $tenant;

        return $this;
    }

    /**
     * Get tenant
     *
     * @return \TraceBundle\Entity\Tenant
     */
    public function getTenant()
    {
        return $this->tenant;
    }
}
