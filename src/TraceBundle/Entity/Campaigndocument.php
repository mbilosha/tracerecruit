<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Campaigndocument
 *
 * @ORM\Table(name="campaigndocument")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\CampaigndocumentRepository")
 */
class Campaigndocument { 
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     * @Assert\NotBlank(message="Campaign Document cannot be blank")
     * @ORM\Column(name="value", type="string", length=300, nullable=true)
     * @Assert\Length(max=300,maxMessage="Campaign Document cannot exceed 300 characters")
     */
    protected $value;
    
    /**
     * @ORM\ManyToOne(targetEntity="Campaign", inversedBy="campaigndocuments", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    protected $campaign;

    /**
     * @ORM\OneToMany(targetEntity="Campaigndocumentresponse", mappedBy="campaigndocument")
     */
    protected $campaigndocumentresponses;
  
    public function __toString() {
        return $this->value;
    }
    
    public function __construct() {
        $this->campaigndocumentresponses = new ArrayCollection();
    }
    
    function __clone() {
        $this->id = null;
        $this->campaign = null;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Campaigndocument
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set campaign
     *
     * @param \TraceBundle\Entity\Campaign $campaign
     *
     * @return Campaigndocument
     */
    public function setCampaign(\TraceBundle\Entity\Campaign $campaign = null)
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * Get campaign
     *
     * @return \TraceBundle\Entity\Campaign
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * Add campaigndocumentresponse
     *
     * @param \TraceBundle\Entity\Campaigndocumentresponse $campaigndocumentresponse
     *
     * @return Campaigndocument
     */
    public function addCampaigndocumentresponse(\TraceBundle\Entity\Campaigndocumentresponse $campaigndocumentresponse)
    {
        $this->campaigndocumentresponses[] = $campaigndocumentresponse;

        return $this;
    }

    /**
     * Remove campaigndocumentresponse
     *
     * @param \TraceBundle\Entity\Campaigndocumentresponse $campaigndocumentresponse
     */
    public function removeCampaigndocumentresponse(\TraceBundle\Entity\Campaigndocumentresponse $campaigndocumentresponse)
    {
        $this->campaigndocumentresponses->removeElement($campaigndocumentresponse);
    }

    /**
     * Get campaigndocumentresponses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaigndocumentresponses()
    {
        return $this->campaigndocumentresponses;
    }
}
