<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\QuestionRepository")
 */
class Question {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Question cannot be blank", groups={"questions"})
     * @Assert\Length(min=10,max=400,minMessage="Question should be minimum {{ limit }} characters", maxMessage="Your question cannot be longer than {{ limit }} characters", groups={"questions"})
     * @ORM\Column(name="question", type="text", nullable=true)
     */
    protected $question;
        
    /**
     * @var boolean
     *
     * @ORM\Column(name="isvideo", type="boolean", nullable=true)
     */
    protected $isvideo;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="isimage", type="boolean", nullable=true)
     */
    protected $isimage;
       
    /**
     * @var integer
     * 
     * @ORM\Column(name="sortorder", type="integer", nullable=true)
     */
    protected $sortorder;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="israndom", type="boolean", nullable=true)
     */
    protected $israndom=false;
    
    /**
     * @var integer
     * @ORM\Column(name="minduration", type="integer", nullable=true)
     */
    protected $minduration=10;
    
    /**
     * @var integer
     * @ORM\Column(name="maxduration", type="integer", nullable=true)
     */
    protected $maxduration=60;
    
    /**
     * @var integer
     * @ORM\Column(name="thinktime", type="integer", nullable=true)
     */
    protected $thinktime=0;
    
    /**
     * @var integer
     * @Assert\NotBlank(message="You must select a maxtake", groups={"videoanswer"})
     * @ORM\Column(name="maxtake", type="integer", nullable=true)
     */
    protected $maxtake;
    
    /**
     *
     * @Assert\File(maxSize="5M", maxSizeMessage="Maximum size for image is 5MB")
     * @Assert\Image(
     *     minWidth = 60,
     *     minHeight = 60
     * )
     * @Assert\File(mimeTypes={"image/jpg","image/png","image/jpeg","image/gif"}, mimeTypesMessage="Image format not supported")
     */
    protected $imageattachment;
    
    /**
     *
     * @Assert\File(maxSize="5M", maxSizeMessage="Maximum size for image is 5MB", groups={"video"})
     * @Assert\File(mimeTypes={"video/mp4"}, mimeTypesMessage="Image format not supported", groups={"video"})
     */
    protected $videoattachment;
    
    /**
     * @var integer
     * 
     * @ORM\Column(name="responsetype", type="integer", nullable=true)
     */
    protected $responsetype=1;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hasmultipleanswer", type="boolean", nullable=true)
     */
    protected $hasmultipleanswer=false;
    
    /**
     * @ORM\ManyToOne(targetEntity="Campaign", inversedBy="questions", cascade={"persist"})
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    protected $campaign;
    
    /**
     * @ORM\ManyToOne(targetEntity="Tenant", inversedBy="questions", cascade={"persist"})
     * @ORM\JoinColumn(name="tenant_id", referencedColumnName="id")
     */
    protected $tenant;
    
    /**
     * @ORM\OneToMany(targetEntity="QuestionOption", mappedBy="question")
     */
    protected $questionoptions;

    /**
     * @ORM\OneToMany(targetEntity="Campaignquestionresponse", mappedBy="question")
     */
    protected $campaignquestionresponses;

    /**
     * @ORM\ManyToMany(targetEntity="Document", inversedBy="questions")
     * @ORM\JoinTable(name="questions_documents")
     */
    private $documents;
    
    public function __construct() {
        $this->questionoptions = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->campaignquestionresponses = new ArrayCollection();
    }
    
    function __clone() {
        $this->id = null;
        $this->campaign = null;
        $this->questionoptions = null;
        $this->documents = null;
    }

    public function getImageattachment() {
        return $this->imageattachment;
    }

    public function getVideoattachment() {
        return $this->videoattachment;
    }

    public function setImageattachment(\Symfony\Component\HttpFoundation\File\File $imageattachment) {
        $this->imageattachment = $imageattachment;
    }

    public function setVideoattachment(\Symfony\Component\HttpFoundation\File\File $videoattachment) {
        $this->videoattachment = $videoattachment;
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return Question
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set isvideo
     *
     * @param boolean $isvideo
     *
     * @return Question
     */
    public function setIsvideo($isvideo)
    {
        $this->isvideo = $isvideo;

        return $this;
    }

    /**
     * Get isvideo
     *
     * @return boolean
     */
    public function getIsvideo()
    {
        return $this->isvideo;
    }

    /**
     * Set isimage
     *
     * @param boolean $isimage
     *
     * @return Question
     */
    public function setIsimage($isimage)
    {
        $this->isimage = $isimage;

        return $this;
    }

    /**
     * Get isimage
     *
     * @return boolean
     */
    public function getIsimage()
    {
        return $this->isimage;
    }

    /**
     * Set sortorder
     *
     * @param integer $sortorder
     *
     * @return Question
     */
    public function setSortorder($sortorder)
    {
        $this->sortorder = $sortorder;

        return $this;
    }

    /**
     * Get sortorder
     *
     * @return integer
     */
    public function getSortorder()
    {
        return $this->sortorder;
    }

    /**
     * Set israndom
     *
     * @param boolean $israndom
     *
     * @return Question
     */
    public function setIsrandom($israndom)
    {
        $this->israndom = $israndom;

        return $this;
    }

    /**
     * Get israndom
     *
     * @return boolean
     */
    public function getIsrandom()
    {
        return $this->israndom;
    }

    /**
     * Set minduration
     *
     * @param integer $minduration
     *
     * @return Question
     */
    public function setMinduration($minduration)
    {
        $this->minduration = $minduration;

        return $this;
    }

    /**
     * Get minduration
     *
     * @return integer
     */
    public function getMinduration()
    {
        return $this->minduration;
    }

    /**
     * Set maxduration
     *
     * @param integer $maxduration
     *
     * @return Question
     */
    public function setMaxduration($maxduration)
    {
        $this->maxduration = $maxduration;

        return $this;
    }

    /**
     * Get maxduration
     *
     * @return integer
     */
    public function getMaxduration()
    {
        return $this->maxduration;
    }

    /**
     * Set thinktime
     *
     * @param integer $thinktime
     *
     * @return Question
     */
    public function setThinktime($thinktime)
    {
        $this->thinktime = $thinktime;

        return $this;
    }

    /**
     * Get thinktime
     *
     * @return integer
     */
    public function getThinktime()
    {
        return $this->thinktime;
    }

    /**
     * Set maxtake
     *
     * @param integer $maxtake
     *
     * @return Question
     */
    public function setMaxtake($maxtake)
    {
        $this->maxtake = $maxtake;

        return $this;
    }

    /**
     * Get maxtake
     *
     * @return integer
     */
    public function getMaxtake()
    {
        return $this->maxtake;
    }

    /**
     * Set responsetype
     *
     * @param integer $responsetype
     *
     * @return Question
     */
    public function setResponsetype($responsetype)
    {
        $this->responsetype = $responsetype;

        return $this;
    }

    /**
     * Get responsetype
     *
     * @return integer
     */
    public function getResponsetype()
    {
        return $this->responsetype;
    }

    /**
     * Set hasmultipleanswer
     *
     * @param boolean $hasmultipleanswer
     *
     * @return Question
     */
    public function setHasmultipleanswer($hasmultipleanswer)
    {
        $this->hasmultipleanswer = $hasmultipleanswer;

        return $this;
    }

    /**
     * Get hasmultipleanswer
     *
     * @return boolean
     */
    public function getHasmultipleanswer()
    {
        return $this->hasmultipleanswer;
    }

    /**
     * Add questionoption
     *
     * @param \TraceBundle\Entity\QuestionOption $questionoption
     *
     * @return Question
     */
    public function addQuestionoption(\TraceBundle\Entity\QuestionOption $questionoption)
    {
        $this->questionoptions[] = $questionoption;

        return $this;
    }

    /**
     * Remove questionoption
     *
     * @param \TraceBundle\Entity\QuestionOption $questionoption
     */
    public function removeQuestionoption(\TraceBundle\Entity\QuestionOption $questionoption)
    {
        $this->questionoptions->removeElement($questionoption);
    }

    /**
     * Get questionoptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionoptions()
    {
        return $this->questionoptions;
    }

    /**
     * Add document
     *
     * @param \TraceBundle\Entity\document $document
     *
     * @return Question
     */
    public function addDocument(\TraceBundle\Entity\document $document)
    {
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \TraceBundle\Entity\document $document
     */
    public function removeDocument(\TraceBundle\Entity\document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Set campaign
     *
     * @param \TraceBundle\Entity\Campaign $campaign
     *
     * @return Question
     */
    public function setCampaign(\TraceBundle\Entity\Campaign $campaign = null)
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * Get campaign
     *
     * @return \TraceBundle\Entity\Campaign
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * Set tenant
     *
     * @param \TraceBundle\Entity\Tenant $tenant
     *
     * @return Question
     */
    public function setTenant(\TraceBundle\Entity\Tenant $tenant = null)
    {
        $this->tenant = $tenant;

        return $this;
    }

    /**
     * Get tenant
     *
     * @return \TraceBundle\Entity\Tenant
     */
    public function getTenant()
    {
        return $this->tenant;
    }

    /**
     * Add campaignquestionresponse
     *
     * @param \TraceBundle\Entity\Campaignquestionresponse $campaignquestionresponse
     *
     * @return Question
     */
    public function addCampaignquestionresponse(\TraceBundle\Entity\Campaignquestionresponse $campaignquestionresponse)
    {
        $this->campaignquestionresponses[] = $campaignquestionresponse;

        return $this;
    }

    /**
     * Remove campaignquestionresponse
     *
     * @param \TraceBundle\Entity\Campaignquestionresponse $campaignquestionresponse
     */
    public function removeCampaignquestionresponse(\TraceBundle\Entity\Campaignquestionresponse $campaignquestionresponse)
    {
        $this->campaignquestionresponses->removeElement($campaignquestionresponse);
    }

    /**
     * Get campaignquestionresponses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaignquestionresponses()
    {
        return $this->campaignquestionresponses;
    }
}
