<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Campaignquestionresponse
 *
 * @ORM\Table(name="campaignquestionresponse")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\CampaignquestionresponseRepository")
 */
class Campaignquestionresponse {
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="value", type="string", nullable=true)
     */
    protected $value;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="isanswered", type="boolean", nullable=true)
     */
    protected $isanswered;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="isviewed", type="boolean", nullable=true)
     */
    protected $isviewed;
    
    /**
     * @ORM\ManyToOne(targetEntity="Question", inversedBy="campaignquestionresponses", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    protected $question;

    /**
     * @ORM\ManyToOne(targetEntity="Applicant", inversedBy="campaignquestionresponses", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="applicant_id", referencedColumnName="id")
     */
    protected $applicant;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Campaignquestionresponse
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set isanswered
     *
     * @param boolean $isanswered
     *
     * @return Campaignquestionresponse
     */
    public function setIsanswered($isanswered)
    {
        $this->isanswered = $isanswered;

        return $this;
    }

    /**
     * Get isanswered
     *
     * @return boolean
     */
    public function getIsanswered()
    {
        return $this->isanswered;
    }

    /**
     * Set isviewed
     *
     * @param boolean $isviewed
     *
     * @return Campaignquestionresponse
     */
    public function setIsviewed($isviewed)
    {
        $this->isviewed = $isviewed;

        return $this;
    }

    /**
     * Get isviewed
     *
     * @return boolean
     */
    public function getIsviewed()
    {
        return $this->isviewed;
    }

    /**
     * Set question
     *
     * @param \TraceBundle\Entity\Question $question
     *
     * @return Campaignquestionresponse
     */
    public function setQuestion(\TraceBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \TraceBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set applicant
     *
     * @param \TraceBundle\Entity\Applicant $applicant
     *
     * @return Campaignquestionresponse
     */
    public function setApplicant(\TraceBundle\Entity\Applicant $applicant = null)
    {
        $this->applicant = $applicant;

        return $this;
    }

    /**
     * Get applicant
     *
     * @return \TraceBundle\Entity\Applicant
     */
    public function getApplicant()
    {
        return $this->applicant;
    }
}
