<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Campaignsharing
 *
 * @ORM\Table(name="campaignsharing")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\CampaignsharingRepository")
 */
class Campaignsharing {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="isvisibletoall", type="boolean", nullable=true)
     */
    protected $isvisibletoall;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="canevaluate", type="boolean", nullable=true)
     */
    protected $canevaluate;
    
    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="campaignsharings", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    protected $client;
    
    /**
     * @ORM\ManyToOne(targetEntity="Campaign", inversedBy="campaignsharings", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    protected $campaign;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isvisibletoall
     *
     * @param boolean $isvisibletoall
     *
     * @return Campaignsharing
     */
    public function setIsvisibletoall($isvisibletoall)
    {
        $this->isvisibletoall = $isvisibletoall;

        return $this;
    }

    /**
     * Get isvisibletoall
     *
     * @return boolean
     */
    public function getIsvisibletoall()
    {
        return $this->isvisibletoall;
    }

    /**
     * Set canevaluate
     *
     * @param boolean $canevaluate
     *
     * @return Campaignsharing
     */
    public function setCanevaluate($canevaluate)
    {
        $this->canevaluate = $canevaluate;

        return $this;
    }

    /**
     * Get canevaluate
     *
     * @return boolean
     */
    public function getCanevaluate()
    {
        return $this->canevaluate;
    }

    /**
     * Set client
     *
     * @param \TraceBundle\Entity\Client $client
     *
     * @return Campaignsharing
     */
    public function setClient(\TraceBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \TraceBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set campaign
     *
     * @param \TraceBundle\Entity\Campaign $campaign
     *
     * @return Campaignsharing
     */
    public function setCampaign(\TraceBundle\Entity\Campaign $campaign = null)
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * Get campaign
     *
     * @return \TraceBundle\Entity\Campaign
     */
    public function getCampaign()
    {
        return $this->campaign;
    }
}
