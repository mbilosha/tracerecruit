<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Responsetype
 *
 * @ORM\Table(name="responsetype")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\ResponsetypeRepository")
 */
class Responsetype { 
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     * @Assert\NotBlank(message="Response Type cannot be blank", groups={"questions"})
     * @ORM\Column(name="value", type="string", length=45, nullable=true)
     * @Assert\Length(max=45,maxMessage="Response Type cannot exceed 45 characters")
     */
    protected $value;
    
    /**
     * @ORM\OneToMany(targetEntity="Question", mappedBy="responsetype")
     */
    protected $questions;
    
    public function __construct() {
        $this->questions = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Responsetype
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Add question
     *
     * @param \TraceBundle\Entity\Question $question
     *
     * @return Responsetype
     */
    public function addQuestion(\TraceBundle\Entity\Question $question)
    {
        $this->questions[] = $question;

        return $this;
    }

    /**
     * Remove question
     *
     * @param \TraceBundle\Entity\Question $question
     */
    public function removeQuestion(\TraceBundle\Entity\Question $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }
}
