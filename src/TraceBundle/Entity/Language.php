<?php

namespace TraceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use TraceBundle\Entity\Tenant;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Language
 *
 * @ORM\Table(name="language")
 * @ORM\Entity(repositoryClass="TraceBundle\Repository\LanguageRepository")
 */
class Language { 
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     * @Assert\NotBlank(message="Language cannot be blank", groups={"language"})
     * @ORM\Column(name="value", type="string", length=65, nullable=true)
     * @Assert\Length(max=65,maxMessage="Language cannot exceeds 65 characters", groups={"language"})
     */
    protected $value;
    
    /**
     * @ORM\OneToMany(targetEntity="Campaign", mappedBy="language")
     */
    protected $campaigns;
    
    /**
     * One Language has Many Tenants.
     * @ORM\OneToMany(targetEntity="Tenant", mappedBy="language")
     */
    protected $tenants;
    
    public function __construct() {
        $this->tenants = new ArrayCollection();
        $this->campaigns = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Language
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Add tenant
     *
     * @param \TraceBundle\Entity\Tenant $tenant
     *
     * @return Language
     */
    public function addTenant(\TraceBundle\Entity\Tenant $tenant)
    {
        $this->tenants[] = $tenant;

        return $this;
    }

    /**
     * Remove tenant
     *
     * @param \TraceBundle\Entity\Tenant $tenant
     */
    public function removeTenant(\TraceBundle\Entity\Tenant $tenant)
    {
        $this->tenants->removeElement($tenant);
    }

    /**
     * Get tenants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTenants()
    {
        return $this->tenants;
    }

    /**
     * Add campaign
     *
     * @param \TraceBundle\Entity\Campaign $campaign
     *
     * @return Language
     */
    public function addCampaign(\TraceBundle\Entity\Campaign $campaign)
    {
        $this->campaigns[] = $campaign;

        return $this;
    }

    /**
     * Remove campaign
     *
     * @param \TraceBundle\Entity\Campaign $campaign
     */
    public function removeCampaign(\TraceBundle\Entity\Campaign $campaign)
    {
        $this->campaigns->removeElement($campaign);
    }

    /**
     * Get campaigns
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }
}
