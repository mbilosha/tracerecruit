<?php

namespace TraceBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TraceBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}  