<?php

namespace TraceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use TraceBundle\Entity\Campaignquestionresponse;
use TraceBundle\Entity\Campaigndocumentresponse;
use TraceBundle\Entity\Practicerecording;
use Symfony\Component\Filesystem\Filesystem;
use TraceBundle\Form\Type\CampaignDocResponseType;
use TraceBundle\Form\Type\CampaignDocResponseDetailsType;
use TraceBundle\Form\Type\ExtensionRequestType;
use DateTimeImmutable;
use TraceBundle\Constant\TraceConstants;
use TraceBundle\Form\Type\AddApplicantType;
use TraceBundle\Entity\Applicant;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

/**
 * @Route("/response")
 */
class ResponseController extends Controller {

    /**
     * @Route("/startinterview/{uniquecode}", name="startinterviewpage")
     */
    public function startinterviewAction(Request $request, $uniquecode) {
        $em = $this->getDoctrine()->getManager();
        $applicant = $em->getRepository('TraceBundle:Applicant')->findOneBy(array('uniqueinvitecode' => $uniquecode));
        $applicantid = $applicant->getId();
        $campaign = $applicant->getCampaign();
        $campaignid = $campaign->getId();
        $applicantstatus = $applicant->getApplicantstatus()->getId();
        if($applicantstatus==TraceConstants::STATUS_SHORTLISTED || $applicantstatus==TraceConstants::STATUS_WAITLISTED || $applicantstatus==TraceConstants::STATUS_ARCHIVED || $applicantstatus==TraceConstants::STATUS_REJECTED || $applicantstatus==TraceConstants::STATUS_CANCELLED){
            return $this->render('TraceBundle:Status:applicantstatus.html.twig', array(
                            'status' => $applicant->getApplicantstatus()->getValue(), 
            ));
        } elseif($applicantstatus==TraceConstants::STATUS_COMPLETED || $applicantstatus==TraceConstants::STATUS_TO_BE_VALIDATED || $applicantstatus==TraceConstants::STATUS_TO_BE_MANUALLY_VERIFIED){
            return new Response('We are processing your application and shall notify you shortly about your result');
        } 
//        elseif($applicantstatus==TraceConstants::STATUS_COMPLETED){
//            return new Response('Your application is under review. We shall notify you shortly about your result');
//        }
        
        $val = $this->container->get('extension_utility')->getExtension($applicant);
        if($val==2){
         
            $form = $this->createForm(ExtensionRequestType::class, $applicant);  
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()) {
                $applicant->setIsextensionrequested(true);
                $currentdate = new DateTimeImmutable();
                $applicant->setExtensionrequesttime($currentdate);
                $em->persist($applicant);
                $em->flush();
                return new Response('Your extension request has been sent');
            }
            return $this->render('TraceBundle:Extension:extensionrequest.html.twig', array(
                            'form' => $form->createView(), 
                            'uniquecode' => $uniquecode, 
            ));
        }
        elseif($val==3){
            return new Response('Your extension request is pending');
        }
        elseif($val==4){
            return new Response('Your extension request has been rejected');
        }
        elseif($val==5){
            return new Response('you will not be allowed to continue');
        }
        elseif($val==6){
            return new Response('Your interview has expired');
        }
        
        //if application in progress or getExtension returns the value to continue interview
        if($applicantstatus==TraceConstants::STATUS_IN_PROGRESS || $val == 1){
            $diagnostic = $this->get('session')->get('diagnostic');
            if(!$diagnostic){
                $url = $this->generateUrl('welcomepage', array('uniquecode' => $uniquecode));
                $response = new RedirectResponse($url);
                return $response;
            }      
            $questions = $applicant->getCampaign()->getQuestions();
            foreach ($questions as $question){               
                $questionarray[$question->getSortorder()] = $question;
            }
            ksort($questionarray);
            foreach ($questionarray as $question){
                $cqr = $em->getRepository('TraceBundle:Campaignquestionresponse')->findOneBy(array('question' => $question->getId(),'applicant'=>$applicantid));
                if($cqr && $cqr->getIsviewed()==true){
                    continue;
                }
                else if(!$cqr){
                    $cqr = new Campaignquestionresponse();
                    $cqr->setValue(null);
                    $cqr->setIsanswered(false);
                    $cqr->setIsviewed(false);
                    $cqr->setQuestion($question);
                    $cqr->setApplicant($applicant);
                    $em->persist($cqr);
                    $em->flush();
                }               
           
                $document = '';
                foreach ($question->getDocuments() as $doc) {
                        if($doc->getDocumenttype()->getId() == 1){ 
                           $document = $doc; 
                        }
                }  
                return $this->render('TraceBundle:Question:viewquestion.html.twig', array(
                            'question' => $question,
                            'campaign' => $campaign,
                            'uniquecode' => $uniquecode,
                            'questionairelength' => sizeof($questionarray),
                            'document' => $document,
                ));
                
            }
            //document logic
            $cdr = new Campaigndocumentresponse();
            $fs = new Filesystem();
            //campaign/campaignid/applicant/applicantid/cv.jpeg
            $dirPath = $request->server->get('DOCUMENT_ROOT') . $request->getBasePath() . '/campaign/' . $campaignid . '/applicant/' . $applicantid ;
            $form = $this->createForm(CampaignDocResponseType::class, $cdr, array('campaign' => $campaign));  
            $form->handleRequest($request);
            if ($form->isValid()) {
//                var_dump($form->getData()->getFilename());exit;
                $cdr->setCampaigndocument($form->getData()->getFilename());
                $cdr->setApplicant($applicant);
                $em->persist($cdr);
                $em->flush();
                
                $extension = $cdr->getAttachment()->guessExtension();
                if (!$extension) {
                    $extension = 'bin';
                }

                if (!$fs->exists($dirPath)) {
                    $fs->mkdir($dirPath);
                }

                $currentcdr = $em->getRepository('TraceBundle:Campaigndocumentresponse')->findOneBy(array('campaigndocument' => $cdr->getFilename()->getId(), 'applicant' => $applicant->getId()));

                if (empty($currentcdr)) {
                    $cdr->setIsuploaded(true);
                    $cdr->setValue($cdr->getFilename()->getValue() . '.' . $extension);
//                    $cdr->setCreatedon(new \DateTime());
                    $em->persist($cdr);
                } else {
//                    var_dump($currentcdr);exit;
                    $fs->remove($dirPath . '/' . $currentcdr->getValue());
                    $em->remove($currentcdr);
                    $cdr->setValue($cdr->getFilename()->getValue() . '.' . $extension);
                    $cdr->setIsuploaded(true);
                    $em->persist($cdr);
                }
                $cdr->getAttachment()->move($dirPath, $cdr->getCampaigndocument()->getValue() . '.' . $extension);
                $em->flush();
                $this->container->get('session')->getFlashBag()->add('uploadsuccess', 'File Uploaded Sucessfully');
            }
            
            $form1 = $this->createForm(CampaignDocResponseDetailsType::class, $cdr, array('applicant' => $applicant));
            $form1->handleRequest($request);

            if ($form1->isSubmitted()) {
                //delete selected files
                $files = $request->request->get('campaign_doc_response_details');
                $file = $files['filename'];
                $deletefile = $em->getRepository('TraceBundle:Campaigndocumentresponse')->findOneBy(array('id' => $file));
//                var_dump($deletefile);exit;
                $fs->remove($dirPath . '/' . $deletefile->getValue());
                $deletefile->setApplicant(null);
                $deletefile->setCampaigndocument(null);
                $em->remove($deletefile);
                $em->flush();
                $this->container->get('session')->getFlashBag()->add('removesuccess', 'File Removed Sucessfully');
            }
            
            if($request->request->get('doc')=='Submit-Application'){
                if ($campaign->getCampaigndocuments()->count() == $applicant->getCampaigndocumentresponses()->count()){
                    //set applicant status to 'to be validated'
                    $status = $em->getRepository('TraceBundle:Applicantstatus')->findOneBy(array('id' => TraceConstants::STATUS_TO_BE_VALIDATED));
                    $applicant->setApplicantstatus($status);
                    $currentdate = new DateTimeImmutable();
                    $applicant->setStatusupdatedon($currentdate);
                    $em->persist($applicant);
                    $em->flush();
                    //redirect to success
                    return $this->render('TraceBundle:Document:uploadsuccess.html.twig', array(
//                                    'uniquecode' => $uniquecode,
//                                    'applicant' => $applicant,
//                                    'campaign' => $campaign,
                    ));
                } else {
                    foreach ($campaign->getCampaigndocuments() as $document) {
                        $docnamearray[] = $document->getValue();
                    }
                    $docnames = implode(',', $docnamearray);
                    $this->container->get('session')->getFlashBag()->add('documentfailure', 'Please upload all the requested documents ('.$docnames.')');
                }
            }
            
            return $this->render('TraceBundle:Document:uploaddocument.html.twig', array(
                            'form' => $form->createView(), 
                            'form1' => $form1->createView(),
                            'uniquecode' => $uniquecode,
                            'applicant' => $applicant,
                            'campaign' => $campaign,
            ));
        }
        
        $i = 0;
        $j = 0;
        
        foreach($applicant->getCampaignquestionresponses() as $questionresponse){
            if($questionresponse->getIsviewed() == true){
                $i++;
            }
        }
        
        foreach($applicant->getCampaigndocumentresponses() as $documentresponse){
            if($documentresponse->getIsuploaded() == true){
                $j++;
            }
        }
        //satisfies condition when no question has been viewed
        if($i == 0) {
            //show welcome page
        }
        elseif ($i == $applicant->getCampaign()->getQuestions()->count() && $j == $applicant->getCampaign()->getCampaigndocuments()->count()) {
            //show interview completed page
        }
        elseif ($i == $applicant->getCampaign()->getQuestions()->count() && $j < $applicant->getCampaign()->getCampaigndocuments()->count()) {
            //please click here to upload document
        }
        elseif ($i > 0 && $i < $applicant->getCampaign()->getQuestions()->count() && $j < $applicant->getCampaign()->getCampaigndocuments()->count()) {
            //show question number page
        }
        elseif (($i == 0 || $i < $applicant->getCampaign()->getQuestions()->count() || $j < $applicant->getCampaign()->getCampaigndocuments()->count()) && $this->isCampaignExpired($applicant)) {
            //show expired page with extension form
        }
    }

    public function isCampaignExpired($applicant) {
        $invitedate = $applicant->getInvitedate();
        $deadlinetoanswer = $applicant->getCampaign()->getDeadlinetoanswer();
        $deadlinedate = $invitedate->modify('+'.$deadlinetoanswer.' days');
        $currentdate = date('d-m-Y H:i:s');
        if($deadlinedate < $currentdate){
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * @Route("/questionresponse/{uniquecode}", name="questionresponsepage")
     */
    public function questionresponseAction(Request $request, $uniquecode) {
        $em = $this->getDoctrine()->getManager();
        $applicant = $em->getRepository('TraceBundle:Applicant')->findOneBy(array('uniqueinvitecode' => $uniquecode));
        $applicantid = $applicant->getId();
        $questionid = $request->request->get('question_id');
        $question = $em->getRepository('TraceBundle:Question')->findOneBy(array('id' => $questionid));
        $cqr = $em->getRepository('TraceBundle:Campaignquestionresponse')->findOneBy(array('question' => $questionid, 'applicant' => $applicantid));
        $answer = $request->request->get('answer');
        if($question->getHasmultipleanswer()==true){
            $value='';
            foreach ($answer as $answer){
                $value = $value.$answer.';';
            }
            $cqr->setValue($value);
        } else {
        $cqr->setValue($answer);
        }
        $cqr->setIsanswered(true);
        $em->persist($cqr);
        $em->flush();
        $url = $this->generateUrl('startinterviewpage', array('uniquecode' => $uniquecode));
        $response = new RedirectResponse($url);
        return $response;
    }
    
    /**
     * @Route("/questionview", options={"expose"=true}, name="questionviewpage")
     */
    public function questionviewAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if($request->isXmlHttpRequest()){
        $questionid = $request->request->get('question_id');
        $uniquecode = $request->request->get('unique_code');
        $applicant = $em->getRepository('TraceBundle:Applicant')->findOneBy(array('uniqueinvitecode' => $uniquecode));
        $applicantid = $applicant->getId();
        $cqr = $em->getRepository('TraceBundle:Campaignquestionresponse')->findOneBy(array('question' => $questionid, 'applicant' => $applicantid));
        $cqr->setIsviewed(true);
        $em->persist($cqr);
        $em->flush();
        }
    }

    /**
     * @Route("/pendingdocuments/{uniquecode}", name="pendingdocumentspage")
     */
    public function pendingdocumentsAction(Request $request, $uniquecode) {
        $em = $this->getDoctrine()->getManager();
        $applicant = $em->getRepository('TraceBundle:Applicant')->findOneBy(array('uniqueinvitecode' => $uniquecode));
        $campaign = $applicant->getCampaign();
        $expirydate = $applicant->getExpirydate();
        return $this->render('TraceBundle:Document:pendingdocuments.html.twig', array(
                            'campaign' => $campaign,
                            'expiry' => $expirydate,
            ));
    }
    
    /**
     * @Route("/diagnostictest/{uniquecode}", name="diagnostictestpage")
     */
    public function diagnostictestAction(Request $request, $uniquecode) {
        $em = $this->getDoctrine()->getManager();
        $applicant = $em->getRepository('TraceBundle:Applicant')->findOneBy(array('uniqueinvitecode' => $uniquecode));
        $campaign = $applicant->getCampaign();
        if($request->request->get('continue')=='Continue'){
            $this->get('session')->set('diagnostic', true);
            $url = $this->generateUrl('practicequestionpage', array('uniquecode' => $uniquecode));
            $response = new RedirectResponse($url);
            return $response;
        } 
        return $this->render('TraceBundle:Diagnostic:diagnostictest.html.twig', array(
                            'uniquecode' => $uniquecode,
                            'campaign' => $campaign,
            ));
    }

    /**
     * @Route("/practicequestion/{uniquecode}", name="practicequestionpage")
     */
    public function practicequestionAction(Request $request, $uniquecode) {
        $em = $this->getDoctrine()->getManager();
        $applicant = $em->getRepository('TraceBundle:Applicant')->findOneBy(array('uniqueinvitecode' => $uniquecode));
        $campaign = $applicant->getCampaign();
        //generate 20 chars randon string(payload)
        $randomstring = substr(md5(microtime()*rand(0,9999)),0,20);
        
        if($request->request->get('submit')=='submit'){
            $practicerec = new Practicerecording();
            $practicerec->setPayload($randomstring);
            $practicerec->setApplicant($applicant);
            $currentdate = new DateTimeImmutable();
            $practicerec->setCreatedon($currentdate);
            $em->persist($practicerec);
            $em->flush();
            $url = $this->generateUrl('practicequestionfeedbackpage', array('uniquecode' => $uniquecode, 'payload' => $randomstring));
            $response = new RedirectResponse($url);
            return $response;
        } 
        return $this->render('TraceBundle:question:practicequestion.html.twig', array(
                            'uniquecode' => $uniquecode,
                            'campaign' => $campaign,
                            'applicant' => $applicant,
                            'randomstring' => $randomstring,
            ));
    }

    /**
     * @Route("/practicequestionfeedback/{uniquecode}", name="practicequestionfeedbackpage")
     */
    public function practicequestionfeedbackAction(Request $request, $uniquecode) {
        $em = $this->getDoctrine()->getManager();
        $applicant = $em->getRepository('TraceBundle:Applicant')->findOneBy(array('uniqueinvitecode' => $uniquecode));
        $applicantid = $applicant->getId();
        $campaign = $applicant->getCampaign();
        $payload = $request->request->get('payload');
        $practicerec = $em->getRepository('TraceBundle:Practicerecording')->findOneBy(array('payload' => $payload, 'applicant' => $applicantid));
        
        if($request->request->get('submit')=='submit'){
            $url = $this->generateUrl('startinterviewpage', array('uniquecode' => $uniquecode));
            $response = new RedirectResponse($url);
            return $response;
        } 
        return $this->render('TraceBundle:question:practicequestionfeedback.html.twig', array(
                            'uniquecode' => $uniquecode,
                            'campaign' => $campaign,
                            'practicerec' => $practicerec,
            ));
    }

    /**
     * @Route("/welcome/{uniquecode}", name="welcomepage")
     */
    public function welcomeAction(Request $request, $uniquecode) {
        if($request->request->get('continue')=='Continue'){
            $url = $this->generateUrl('diagnostictestpage', array('uniquecode' => $uniquecode));
            $response = new RedirectResponse($url);
            return $response;
        }
        
        return $this->render('TraceBundle:Welcome:welcomepage.html.twig', array(
                            'uniquecode' => $uniquecode,
        ));
    }

    /**
     * @Route("/extensionrequestlist", name="extensionrequestlistpage")
     */
    public function extensionrequestlistAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $tenantid = $this->getUser()->getTenant()->getId();
        $applicants = $em->getRepository('TraceBundle:Applicant')->findBy(array(
            'tenant' => $tenantid,
            'isextensionrequested' => true,
            'isextended' => null
        ));
        
        return $this->render('TraceBundle:Extension:extensionrequestlist.html.twig', array(
                            'applicants' => $applicants,
            ));
    }
    
    /**
     * @Route("/extensionrequestapproval", options={"expose"=true}, name="extensionrequestapprovalpage")
     */
    public function extensionrequestapprovalAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if($request->isXmlHttpRequest()){
        $applicantid = $request->request->get('applicant_id');
        $applicant = $em->getRepository('TraceBundle:Applicant')->findOneBy(array('id' => $applicantid));
        $flag = $request->request->get('flag');
        if($flag=='approve'){
            $applicant->setIsextended(true);
            $currentdate = new DateTimeImmutable();
            $deadlinetoanswer = $applicant->getCampaign()->getDeadlinetoanswer();
            $applicant->setExpirydate($currentdate->modify('+'.$deadlinetoanswer.' days'));
            $status = $em->getRepository('TraceBundle:Applicantstatus')->findOneBy(array('id' => TraceConstants::STATUS_IN_PROGRESS));
            $applicant->setApplicantstatus($status);
            $applicant->setStatusupdatedon($currentdate);
            $em->persist($applicant);
            $em->flush();
            return new Response(json_encode('success'));
        } elseif($flag=='reject'){
            $applicant->setIsextended(false);
            $currentdate = new DateTimeImmutable();
            $status = $em->getRepository('TraceBundle:Applicantstatus')->findOneBy(array('id' => TraceConstants::STATUS_ARCHIVED));
            $applicant->setApplicantstatus($status);
            $applicant->setStatusupdatedon($currentdate);
            $em->persist($applicant);
            $em->flush();
            return new Response(json_encode('success'));
        }
        
        
        }
    }

    /**
     * @Route("/public/{campaignid}", options={"expose"=true}, name="publiclinkpage")
     */
    public function publiclinkAction(Request $request, $campaignid) {
        $em = $this->getDoctrine()->getManager();
        $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
        $tenant = $campaign->getTenant();
        if($campaign->getCampaignstatus()->getId()==1 && $campaign->getIspublic()==true){
            $applicant = new Applicant();  
            $form = $this->createForm(AddApplicantType::class, $applicant);  
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                try {
//                $campaignid = $request->request->get('campaignid');
//                $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
                $applicant->setCampaign($campaign);
//                $user = $this->getUser();
//                $tenant = $user->getTenant();
                $applicant->setTenant($tenant);
                $applicantstatus = $em->getRepository('TraceBundle:Applicantstatus')->findOneBy(array('id'=>TraceConstants::STATUS_IN_PROGRESS));
                $applicant->setApplicantstatus($applicantstatus);
                $interviewtype = $em->getRepository('TraceBundle:Interviewtype')->findOneBy(array('id'=>1));
                $applicant->setInterviewtype($interviewtype);
                $token = $this->container->get('unique_token')->getToken();
                if($token){
                    $applicant->setUniqueinvitecode($token);
                } else {
                    $applicant->setIsuniquecodeerror(true);
                }
                $currentdate = new DateTimeImmutable();
                $deadline = $campaign->getDeadlinetoanswer();
                $expirydate = $currentdate->modify('+'.$deadline.' days');
                $applicant->setExpirydate($expirydate);
                $applicant->setInvitedate($currentdate);
                $applicant->setCreatedon($currentdate);
                $applicant->setStatusupdatedon($currentdate);
                $em->persist($applicant);
                $em->flush();
                
                //before redirecting an email needs to be send to the  applicant with interview link
                
                $url = $this->generateUrl('startinterviewpage', array('uniquecode' => $applicant->getUniqueinvitecode()));
                $response = new RedirectResponse($url);
                return $response;
            }
            catch (UniqueConstraintViolationException $e) {
                    $this->addFlash("notice", "This email is already registred for this campaign.");
                }
            }

            return $this->render('TraceBundle:Applicant:registerapplicant.html.twig', array(
                        'form' => $form->createView(),
                        'campaign' => $campaign,
            ));
        }
    }    
}
