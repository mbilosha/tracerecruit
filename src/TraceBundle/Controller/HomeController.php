<?php

namespace TraceBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TraceBundle\Form\Type\AddColleagueType;
use TraceBundle\Entity\Client;
use TraceBundle\Entity\Tenant;
use Symfony\Component\HttpFoundation\RedirectResponse;
use TraceBundle\Constant\TraceConstants;

class HomeController extends Controller{ 
    
    /**
     * @Route("/colleagues", name="colleaguespage")
     */
    public function colleaguesAction(Request $request){ 
        
      return $this->render('TraceBundle:Home:colleaguespage.html.twig', array(
                    
        ));
    } 
    
    /**
     * @Route("/addcolleague", name="addcolleaguepage")
     */
    public function addcolleagueAction(Request $request){ 
        // loggedin user
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $client = new Client();                             
        $form = $this->createForm(AddColleagueType::class, $client);  
        $form->handleRequest($request);
        
        if ($form->isSubmitted()&& $form->isValid()) {     
            $client = $form->getData();
            $roles = $form->getData()->getRole();           
            // auto-generate password
            $tokenGenerator = $this->container->get('fos_user.util.token_generator');
            $password = substr($tokenGenerator->generateToken(), 0, 6);
            //persisting client object
            $client->setPassword($password);
            $client->setUsername($client->getEmail());           
            $client->setEnabled(true);  
            $client->addRole($roles);
            $tenant = $user->getTenant();          
            $client->setTenant($tenant);
            $em->persist($client);
            $em->flush();
            $this->addFlash('notice', 'Colleague Added Successfully');
            return $this->redirect($request->getUri());
        }
        
      return $this->render('TraceBundle:Home:addcolleague.html.twig', array(
            'form' => $form->createView(),        
        ));
    } 
    
    /**
     * @Route("/dashboard", name="dashboardpage")
     */
    public function adddashboardAction(Request $request){ 
      $user = $this->get('security.token_storage')->getToken()->getUser();
      
      return $this->render('TraceBundle:Home:dashboard.html.twig', array(
                    'user'=>$user,
        ));
    } 
    
    /**
     * @Route("/campaigns", name="campaignspage")
     */
    public function campaignsAction(Request $request){ 
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $tenant = $user->getTenant();
        $tenantid = $tenant->getId();
        $status = $request->query->get('value');
        if(!$status){
            $status = 1;
        }
        $campaigns = $em->getRepository('TraceBundle:Campaign')->findBy(array('campaignstatus'=>$status, 'tenant' => $tenantid));                     
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                                    $campaigns, /* listings NOT result */ 
                                    $request->query->getInt('page', 1)/* page number */,
                                    $this->container->getParameter('pagerange')/* limit per page */
        );
        return $this->render('TraceBundle:Home:campaigns.html.twig', array(
                    'pagination' => $pagination,
                    'status' => $status,
        ));  
    } 
    
    /**
     * @Route("/lives", name="livespage")
     */
    public function livesAction(Request $request){ 
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $tenant = $user->getTenant();
        $tenantid = $tenant->getId();
        $applicants = $em->getRepository('TraceBundle:Applicant')->findBy(array('interviewtype' => 2, 'tenant' => $tenantid, 'applicantstatus'=>TraceConstants::STATUS_IN_PROGRESS));
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                                    $applicants, /* listings NOT result */ 
                                    $request->query->getInt('page', 1)/* page number */,
                                    $this->container->getParameter('pagerange')/* limit per page */
        );
      return $this->render('TraceBundle:Home:lives.html.twig', array(
                    'pagination' => $pagination,
        ));
    } 
    
    /**
     * @Route("/medialibrary", name="medialibrarypage")
     */
    public function medialibraryAction(Request $request){ 
        
      return $this->render('TraceBundle:Home:medialibrarypage.html.twig', array(
                    
        ));
    } 
    /**
     * @Route("/help", name="helppage")
     */
    public function helpAction(Request $request){ 
        
      return $this->render('TraceBundle:Home:helppage.html.twig', array(
                    
        ));
    } 
    /**
     * @Route("/profile", name="profilepage")
     */
    public function profileAction(Request $request){ 
      
     $user = $this->get('security.token_storage')->getToken()->getUser();
        
      return $this->render('TraceBundle:Home:profilepage.html.twig', array(
                    'user' => $user,
        ));
    } 
    /**
     * @Route("/preference", name="preferencepage")
     */
    public function preferenceAction(Request $request){ 
        
      return $this->render('TraceBundle:Home:preferencepage.html.twig', array(
                    
        ));
    } 
    /**
     * @Route("/candidatemessage", name="candidatemessagepage")
     */
    public function candidatemessageAction(Request $request){ 
        
      return $this->render('TraceBundle:Home:candidatemessagepage.html.twig', array(
                    
        ));
    } 
}  