<?php

namespace TraceBundle\Controller\Api;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use TraceBundle\Entity\Client;
use TraceBundle\Form\Type\ApiAdminRegistrationType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\FormInterface;

class RegistrationController extends BaseController {
    
    public function __construct() {
      //        parent::__construct();
    }

    /**
     * @Route("/api/register", name="api_registration")
     * @Method("POST")
     */ 
    public function registerAction(Request $request)
    {  
        $em = $this->getDoctrine()->getManager();
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        
        $body = $request->getContent();
        $data = json_decode($body, true);
        
        $client = new Client();  
        $form = $this->createForm(ApiAdminRegistrationType::class, $client);  
        $form->submit($data);

        if(!$form->isValid()){
            $errors = $this->getErrorsFromForm($form);
            $errorbody = array(
                'type' => 'validation_error',
                'title' => 'There was a validation error',
                'errors' => $errors
            );
//            return new JsonResponse($errorbody, 400);
            return new JsonResponse($errorbody, 200);
        }
        
        $salutation = $em->getRepository('TraceBundle:Salutation')->findOneBy(array('id' => 1));
        $tenant = $em->getRepository('TraceBundle:Tenant')->findOneBy(array('id' => 1));
        
        $client->setSalutation($salutation);
        $client->setTenant($tenant);
        $client->setEnabled(true);
        $client->addRole('ROLE_ADMIN');
        $em->persist($client);
        $em->flush();
        
        $response = new Response('It worked', 201);
//        $response->headers->set('Location', '/some/url');
        
        return $response;
    }
    
    /**
     * @Route("/api/gettenant", name="api_get_tenant")
     * @Method("POST")
     */ 
    public function getTenantAction(Request $request) {
        $user = $this->getUser();
        $tenant = $user->getTenant()->getName();
        return new Response(json_encode($tenant), 200);
    }
    
    private function getErrorsFromForm(FormInterface $form) {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach($form->all() as $childForm){
            if($childForm instanceof FormInterface){
                if($childErrors = $this->getErrorsFromForm($childForm)){
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }
} 