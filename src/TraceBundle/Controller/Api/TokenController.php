<?php

namespace TraceBundle\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;

class TokenController extends Controller{

    /**
     * @Route("/api/tokens")
     * @Method("POST")
     */
    public function newTokenAction(Request $request) {
        $user = $this->getDoctrine()
                ->getRepository('TraceBundle:Client')
                ->findOneBy(array('username' => $request->getUser()));
        
        if(!$user){
            throw new createNotFoundException('No User');
        }
        
        $isValid = $this->get('security.password_encoder')
                ->isPasswordValid($user, $request->getPassword());
        
        if(!$isValid){
            throw new BadCredentialsException();
        }
        
        $token = $this->get('lexik_jwt_authentication.encoder')
                ->encode([
                    'username' => $user->getUsername(),
                    'exp' => time() + 1296000,
                ]);
        
        return new JsonResponse([
            'token' => $token
        ]);
    }
}
