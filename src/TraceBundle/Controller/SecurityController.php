<?php

namespace TraceBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SecurityController extends BaseController {
    
    
//    public function loginAction(Request $request) {
//
//        if ($this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {  
//            return new RedirectResponse($this->generateUrl('superadminpage'));
//            
//        } else 
//        if ($this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {  
//            return new RedirectResponse($this->generateUrl('adminhome'));
//        }
//        else if ($this->container->get('security.authorization_checker')->isGranted('ROLE_STUDENT')) {
//            return new RedirectResponse($this->generateUrl('studenthomepage'));
//        } else if ($this->container->get('security.authorization_checker')->isGranted('ROLE_PROVIDER')) {
//            return new RedirectResponse($this->generateUrl('providerhome'));
//        }        
//         /** @var $session \Symfony\Component\HttpFoundation\Session\Session */
//        $session = $request->getSession();
//        $securityContext = $this->container->get('security.authorization_checker');
//        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
//            $user = $this->getUser();         
//        }
//        
//        if (class_exists('\Symfony\Component\Security\Core\Security')) {
//            $authErrorKey = Security::AUTHENTICATION_ERROR;
//            $lastUsernameKey = Security::LAST_USERNAME;
//        } else {
//            // BC for SF < 2.6
//            $authErrorKey = SecurityContextInterface::AUTHENTICATION_ERROR;
//            $lastUsernameKey = SecurityContextInterface::LAST_USERNAME;
//        }
//
//        // get the error if any (works with forward and redirect -- see below)
//        if ($request->attributes->has($authErrorKey)) {
//            $error = $request->attributes->get($authErrorKey);
//        } elseif (null !== $session && $session->has($authErrorKey)) {
//            $error = $session->get($authErrorKey);
//            $session->remove($authErrorKey);
//        } else {
//            $error = null;
//        }
//        
//        if (!$error instanceof AuthenticationException) {
//            $error = null; // The value does not come from the security component.
//        }
//        
//        if ($error) {
//          //  $error = $error->getMessage();
//            $this->addFlash('error', $error->getMessage());
//        }
//        
//        // last username entered by the user
//        $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);
//
//        if ($this->has('security.csrf.token_manager')) {
//            $csrfToken = $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue();
//        } else {
//            // BC for SF < 2.4
//            $csrfToken = $this->has('form.csrf_provider')
//                ? $this->get('form.csrf_provider')->generateCsrfToken('authenticate')
//                : null;
//        }
//
//        return $this->renderLogin(array(
//            'last_username' => $lastUsername,
//            'error' => $error,
//            'csrf_token' => $csrfToken,
//        ));
//        
//    }

    /**
     *
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderLogin(array $data)
    { 
        
//        $template = sprintf('EpitaHousingBundle:Security:login.html.twig');
        $template = sprintf('FOSUserBundle:Security:login.html.twig');
        return $this->container->get('templating')->renderResponse($template, $data);       
    }
        
    public function checkAction()
    {
        throw new \RuntimeException('You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.');
    }

    public function logoutAction()
    {
        throw new \RuntimeException('You must activate the logout in your security firewall configuration.');
    }
}   