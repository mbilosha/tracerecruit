<?php

namespace TraceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @Route("/live")
 */
class LiveController extends Controller {

    /**
     * @Route("/startliveinterview/{uniquecode}", name="startliveinterviewpage")
     */
    public function startliveinterviewAction(Request $request, $uniquecode) {
        $em = $this->getDoctrine()->getManager();
        $applicant = $em->getRepository('TraceBundle:Applicant')->findOneBy(array('uniqueinvitecode' => $uniquecode));
        $campaign = $applicant->getCampaign();
        
        return $this->render('TraceBundle:Live:startliveinterview.html.twig', array(
                            'uniquecode' => $uniquecode,
                            'campaign' => $campaign,
                            'applicant' => $applicant,
            ));
    }

}
