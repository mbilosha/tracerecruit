<?php

namespace TraceBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\UserBundle\Controller\ProfileController as BaseController;
use Symfony\Component\HttpFoundation\Request;
//use TraceBundle\Entity\Client;
//use TraceBundle\Form\Type\AdminRegistrationType;
//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ProfileController extends BaseController {
    
    public function __construct() {
      //        parent::__construct();
    }
    
    /**
     * @Route("/adminpage", name="adminhome")
     */
    public function showaAction(Request $request)
    {  
        $tenant = $request->get('tenantid');
        
        //role based access--if admin access this page send him out
        $user = $this->get('security.token_storage')->getToken()->getUser();
//        if (!is_object($user) || !$user instanceof UserInterface) {
//            throw new AccessDeniedException('This user does not have access to this section.');
//        }

        return $this->render('TraceBundle:Profile:show.html.twig', array(
                    'user' => $user,
        ));
    }

}
