<?php

namespace TraceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use TraceBundle\Entity\Campaign;
use TraceBundle\Entity\Question;
use TraceBundle\Form\Type\CampaignType;
use TraceBundle\Form\Type\AddApplicantType;
use TraceBundle\Form\Type\CampaignPreRecQuestionType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Filesystem\Filesystem;
use Doctrine\Common\Collections\ArrayCollection;
use TraceBundle\Form\Type\CampaignPreRecEvalCriteriaType;
use TraceBundle\Entity\Document;
use TraceBundle\Entity\QuestionOption;
use TraceBundle\Entity\Evaluationcriteria;
use DateTime;
use TraceBundle\Entity\Campaigndocument;
use TraceBundle\Entity\Applicant;
use TraceBundle\Entity\Comment;
use TraceBundle\Entity\ApplicantEvaluation;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use DateTimeImmutable;
use TraceBundle\Constant\TraceConstants;
use TraceBundle\Form\Type\AddApplicantLiveType;

/**
 * @Route("/campaign")
 */
class CampaignController extends Controller {

    /**
     * @Route("/create", name="createcampaign")
     */
    public function createCampaignAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $campaign = new Campaign();
        $user = $this->getUser();
        $tenant = $user->getTenant();
        $campaign->setTenant($tenant);
        //createdby
        $campaign->setClient($user);
        //set in draft status
        $status = $em->getRepository('TraceBundle:Campaignstatus')->findOneBy(array('id' => 3));
        $campaign->setCampaignstatus($status);
        $em->persist($campaign);
        $em->flush();
        $campaignid = $campaign->getId();
        $url = $this->generateUrl('loadgeneralpage', array('campaign_id' => $campaignid));
        $response = new RedirectResponse($url);
        return $response;
    }

    /**
     * @Route("/delete", name="deletecampaign")
     */
    public function deleteCampaignAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $campaignid = $request->query->get('campaign_id');
        $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
        //set archived status
        $status = $em->getRepository('TraceBundle:Campaignstatus')->findOneBy(array('id' => 2));
        $campaign->setCampaignstatus($status);
        $em->persist($campaign);
        $em->flush();
        $url = $this->generateUrl('campaignspage');
        $response = new RedirectResponse($url);
        return $response;
    }

    /**
     * @Route("/duplicate", name="duplicatecampaign")
     */
    public function duplicatecampaignAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $campaignid = $request->query->get('campaign_id');
        $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
        $duplicatecampaign = clone $campaign;
        $duplicatecampaign->setCampaigntitle('Copy of '.$campaign->getCampaigntitle());
        $em->persist($duplicatecampaign);
        $em->flush();
        //set in draft status
        $status = $em->getRepository('TraceBundle:Campaignstatus')->findOneBy(array('id' => 3));
        $duplicatecampaign->setCampaignstatus($status);
        //set created(duplicated) by
        $duplicatecampaign->setClient($user);
        //evaluation criteria
        foreach ($campaign->getEvaluationcriteria() as $criterion) {
            $duplicatecriterion = clone $criterion;
            $duplicatecriterion->setCampaign($duplicatecampaign);
            $em->persist($duplicatecriterion);
            $em->flush();
        } 
        //questions
        foreach ($campaign->getQuestions() as $question) {
            $duplicatequestion = clone $question;
            $duplicatequestion->setCampaign($duplicatecampaign);
            $em->persist($duplicatequestion);
            $em->flush();
            
            //question options
            if($question->getQuestionoptions()){
            foreach ($question->getQuestionoptions() as $option) {
                $duplicateoption = clone $option;
                $duplicateoption->setQuestion($duplicatequestion);
                $em->persist($duplicateoption);
                $em->flush();
                }
            }            
        }
        //campaign documents
        foreach ($campaign->getCampaigndocuments() as $campaigndocuemnt) {
            $duplicatecampaigndocuemnt = clone $campaigndocuemnt;
            $duplicatecampaigndocuemnt->setCampaign($duplicatecampaign);
            $em->persist($duplicatecampaigndocuemnt);
            $em->flush();
        }
        //campaign clients
        foreach ($campaign->getCampaignclients() as $campaignclient) {
            $duplicatecampaignclient = clone $campaignclient;
            $duplicatecampaignclient->setCampaign($duplicatecampaign);
            $em->persist($duplicatecampaignclient);
            $em->flush();
        }
        $duplicatecampaignid = $duplicatecampaign->getId();
        $url = $this->generateUrl('loadgeneralpage', array('campaign_id' => $duplicatecampaignid));
        $response = new RedirectResponse($url);
        return $response;
    }

    /**
     * @Route("/activated", options={"expose"=true}, name="campaignactivatedpage")
     */
    public function campaignactivatedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $campaignid = $request->query->get('campaign_id');
        $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
        //set active status
        $status = $em->getRepository('TraceBundle:Campaignstatus')->findOneBy(array('id' => 1));
        $campaign->setCampaignstatus($status);
        $em->persist($campaign);
        $em->flush();
        $url = $this->generateUrl('campaignspage');
        $response = new RedirectResponse($url);
        return $response;
    }
    
    /**
     * @Route("/loadgeneral", options={"expose"=true}, name="loadgeneralpage")
     */
    public function loadgeneralAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $campaignid = $request->query->get('campaign_id');
        $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
        if ($campaign->getCampaigndocuments()->isEmpty()) {
            $document = new Campaigndocument();
            $campaign->addCampaigndocument($document);
            $document->setCampaign($campaign);
            $em->persist($document);
        }
        $language = $em->getRepository('TraceBundle:Language')->findOneBy(array('value' => 'en'));
        $campaign->setLanguage($language);
        //set in draft status
        $status = $em->getRepository('TraceBundle:Campaignstatus')->findOneBy(array('id' => 3));
        $campaign->setCampaignstatus($status);
        $em->persist($campaign);
        $em->flush();
        
        return $this->render('TraceBundle:General:loadgeneral.html.twig', array(
            'documents' => $campaign->getCampaigndocuments(),
            'campaign' => $campaign,
        ));
    }

    /**
     * @Route("/loadapplicantlanguage", options={"expose"=true}, name="loadapplicantlanguagepage")
     */
    public function loadapplicantlanguageAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $campaignid = $request->request->get('campaign_id');
            $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
            $language = $campaign->getLanguage()->getValue();
            return new Response(json_encode($language));
        }
    }
    
    /**
     * @Route("/edittitle", options={"expose"=true}, name="edittitlepage")
     */
    public function edittitleAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $campaignid = $request->request->get('campaign_id');
            $titlevalue = $request->request->get('value');
            $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
            $campaign->setCampaigntitle($titlevalue);
            $em->persist($campaign);
            $em->flush();
            return new Response(json_encode('success'));
        }
    }
    
    /**
     * @Route("/editlocation", options={"expose"=true}, name="editlocationpage")
     */
    public function editlocationAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $campaignid = $request->request->get('campaign_id');
            $locationvalue = $request->request->get('value');
            $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
            $campaign->setLocation($locationvalue);
            $em->persist($campaign);
            $em->flush();
            return new Response(json_encode('success'));
        }
    }
    
    /**
     * @Route("/editjobreference", options={"expose"=true}, name="editjobreferencepage")
     */
    public function editjobreferenceAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $campaignid = $request->request->get('campaign_id');
            $jobreferencevalue = $request->request->get('value');
            $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
            $campaign->setJobreference($jobreferencevalue);
            $em->persist($campaign);
            $em->flush();
            return new Response(json_encode('success'));
        }
    }
    
    /**
     * @Route("/editapplicantlanguage", options={"expose"=true}, name="editapplicantlanguagepage")
     */
    public function editapplicantlanguageAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $campaignid = $request->request->get('campaign_id');
            $applicantlanguagevalue = $request->request->get('value');
            $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
            $language = $em->getRepository('TraceBundle:Language')->findOneBy(array('value' => $applicantlanguagevalue));            
            $campaign->setLanguage($language);
            $em->persist($campaign);
            $em->flush();
            return new Response(json_encode('success'));
        }
    }
    
    /**
     * @Route("/editdocumentname", options={"expose"=true}, name="editdocumentnamepage")
     */
    public function editdocumentnameAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $documentid = $request->request->get('document_id');
            $document = $em->getRepository('TraceBundle:Campaigndocument')->findOneBy(array('id' => $documentid));
            $documentnamevalue = $request->request->get('value');
            $document->setValue($documentnamevalue);
            $em->persist($document);
            $em->flush();
            return new Response(json_encode('success'));
        }
    }
    
    /**
     * @Route("/adddocument", options={"expose"=true}, name="adddocumentpage")
     */
    public function adddocumentAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $campaignid = $request->request->get('campaign_id');
            $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
            $document = new Campaigndocument();
            $document->setCampaign($campaign);
            $em->persist($document);
            $em->flush();
            $html = $this->renderView('TraceBundle:CampaignDocument:campaigndocument.html.twig', array('document' => $document));
            return new Response(json_encode($html));
        }
    }

    /**
     * @Route("/deletedocument", options={"expose"=true}, name="deletedocumentpage")
     */
    public function deletedocumentAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $documentid = $request->request->get('document_id');
            $document = $em->getRepository('TraceBundle:Campaigndocument')->findOneBy(array('id' => $documentid));
            $document->setCampaign(null);
            $em->remove($document);
            $em->flush();
            return new Response(json_encode('success'));
        }
    }
    
    /**
     * @Route("/loadprerecorded", options={"expose"=true}, name="loadprerecordedpage")
     */
    public function loadprerecordedAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $campaignid = $request->query->get('campaign_id');
        $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
        $user = $this->getUser();
        $tenant = $user->getTenant();

        if ($campaign->getQuestions()->isEmpty()) {
            $question = new Question();
            $campaign->addQuestion($question);
            $question->setCampaign($campaign);
            $question->setTenant($tenant);
            $em->persist($question);
            $em->persist($campaign);
            $em->flush();
        }
        
        if ($campaign->getEvaluationcriteria()->isEmpty()) {
            $criterion = new Evaluationcriteria();
            $criterion->setCriterion('Career consistency');
            $campaign->addEvaluationcriterium($criterion);
            $criterion->setCampaign($campaign);
            $criterion->setTenant($tenant);
            $em->persist($criterion);
            $em->persist($campaign);
            $em->flush();           
        }

        return $this->render('TraceBundle:Prerecorded:loadprerecorded.html.twig', array(
                    'criterions' => $campaign->getEvaluationcriteria(),
                    'questions' => $campaign->getQuestions(),
                    'campaign' => $campaign,
        ));
    }
    
    /**
     * @Route("/addquestion", options={"expose"=true}, name="addquestionpage")
     */
    public function addquestionAction(Request $request) {
        $em = $this->getDoctrine()->getManager();        
        $user = $this->getUser();
        $tenant = $user->getTenant();
        if ($request->isXmlHttpRequest()) {
            $campaignid = $request->request->get('campaign_id');
            $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
            $question = new Question();
            $campaign->addQuestion($question);
            $question->setCampaign($campaign);
            $question->setTenant($tenant);
            $em->persist($campaign);
            $em->persist($question);
            $em->flush();
            $questionid = $question->getId();
            $html = $this->renderView('TraceBundle:question:questiontemplate.html.twig', array('question' => $question));
            return new Response(json_encode(array('html' => $html, 'questionid' => $questionid)));
        }
    }

    /**
     * @Route("/deletequestion", options={"expose"=true}, name="deletequestionpage")
     */
    public function deletequestionAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $tenant = $user->getTenant();
        if ($request->isXmlHttpRequest()) {
            $questionid = $request->request->get('question_id');
            $question = $em->getRepository('TraceBundle:Question')->findOneBy(array('id' => $questionid));
            foreach($question->getCampaignquestionresponses() as $cqr){
                $question->removeCampaignquestionresponse($cqr);
                $cqr->setQuestion(null);
                $em->persist($cqr);
                $em->flush();
            } 
            if(!$question->getQuestionoptions()->isEmpty()){
                foreach($question->getQuestionoptions() as $choice){
                $question->removeQuestionoption($choice);
                $choice->setQuestion(null);
                $em->remove($choice);
                $em->flush();
                }
            }
            $em->remove($question);
            $em->flush();
            return new Response(json_encode('success'));
        }
    }

    /**
     * @Route("/israndom", options={"expose"=true}, name="editisrandom")
     */
    public function israndomAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $questionid = $request->request->get('question_id');
            $israndomvalue = $request->request->get('value');
            $question = $em->getRepository('TraceBundle:Question')->findOneBy(array('id' => $questionid));
            $question->setIsrandom($israndomvalue);
            $em->persist($question);
            $em->flush();
            return new Response(json_encode('success'));
        }
    }

    /**
     * @Route("/loadquestion", options={"expose"=true}, name="loadquestionpage")
     */
    public function loadquestionAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $flag = $request->request->get('flag');
            $questionid = $request->request->get('question_id');
            $question = $em->getRepository('TraceBundle:Question')->findOneBy(array('id' => $questionid));
            if ($flag == 'loadisrandom') {
                $israndomvalue = $question->getIsrandom();
                $response = json_encode($israndomvalue);
            } elseif ($flag == 'loadvideo') {
                if ($question->getIsvideo() === true) {
                    $this->get('logger')->addInfo('somesh'.$question->getDocuments()[0]->getId());
                    foreach ($question->getDocuments() as $doc) {
                        $result['documentid'] = ($doc->getDocumenttype()->getId() == 1) ? $doc->getId() : null;
                    }
                }
                $result['isvideo'] = $question->getIsvideo();
                $response = json_encode($result);
            } elseif ($flag == 'loadquestionname') {
                $response = json_encode($question->getQuestion());
            } elseif ($flag == 'loadresponsetype') {
                $responsetypevalue = $question->getResponsetype();
                if ($responsetypevalue == '4') {
                    $choices = $question->getQuestionoptions();
                    $html = $this->renderView('TraceBundle:choice:choiceform.html.twig', array('choices' => $choices, 'question' => $question));
                    $result['html'] = $html;
                } elseif ($responsetypevalue == '3') {
                    $html = $this->renderView('TraceBundle:maxtake:maxtakewidget.html.twig', array('question' => $question));
                    $result['html'] = $html;
                }
                $result['responsetype'] = $responsetypevalue;
                $response = json_encode($result);
            } elseif ($flag == 'loadmaxtakevalue') {
                $response = json_encode($question->getMaxtake());
            } elseif ($flag == 'loadhasmultiple') {
                $response = json_encode($question->getHasmultipleanswer());
            } elseif ($flag == 'loadthinktime') {
                $response = json_encode($question->getThinktime());
            } elseif ($flag == 'loadslider') {
                $res['min'] = $question->getMinduration();
                $res['max'] = $question->getMaxduration();
                $response = json_encode($res);
            }
            return new Response($response);
        }
    }

    /**
     * @Route("/editquestion", options={"expose"=true}, name="editquestionpage")
     */
    public function editquestionAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $tenant = $user->getTenant();
        $tenantid = $tenant->getId();
        if ($request->isXmlHttpRequest()) {
            $flag = $request->request->get('flag');
            $questionid = $request->request->get('question_id');
            $question = $em->getRepository('TraceBundle:Question')->findOneBy(array('id' => $questionid));
            $value = $request->request->get('value');
            $html = '';
            if ($flag == 'editquestionname') {
                $question->setQuestion($value);
            } elseif ($flag == 'editresponsetype') {
                $question->setResponsetype($value);
                if ($value == '4') { //change the response values to CONSTANT..
                    $html = $this->renderView('TraceBundle:choice:choiceform.html.twig', array('question' => $question));
                } elseif ($value == '3') {
                    $html = $this->renderView('TraceBundle:maxtake:maxtakewidget.html.twig', array('question' => $question));
                }
            } elseif ($flag == 'editminduration') {
                $minduration = $value == '' ? null : $value;
                $question->setMinduration($minduration);
            } elseif ($flag == 'editmaxduration') {
                $maxduration = $value == '' ? null : $value;
                $question->setMaxduration($maxduration);
            } elseif ($flag == 'editthinktime') {
                $thinktime = $value == '' ? null : $value;
                $question->setThinktime($thinktime);
            } elseif ($flag == 'editmaxtake') {
                $maxtake = $value == '' ? null : $value;
                $question->setMaxtake($maxtake);
            } elseif ($flag == 'editvideo') {
                $documents = $em->getRepository('TraceBundle:Document')->findBy(array('tenant' => $tenantid));
                $html = $this->renderView('TraceBundle:video:videoupload.html.twig', array('documents' => $documents, 'question' => $question, 'tenant' => $tenant));
                return new Response(json_encode(array('html' => $html)));
            }
            //ensure render view is collected for $html object
            $response = ($html == '') ? json_encode('success') : json_encode($html);
            $em->persist($question);
            $em->flush();
            return new Response($response);
        }
    }

    /**
     * @Route("/addcriterion", options={"expose"=true}, name="addcriterionpage")
     */
    public function addcriterionAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $tenant = $user->getTenant();
        if ($request->isXmlHttpRequest()) {
            $campaignid = $request->request->get('campaign_id');
            $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
            $criterion = new Evaluationcriteria();
            $campaign->addEvaluationcriterium($criterion);
            $criterion->setCampaign($campaign);
            $criterion->setTenant($tenant);
            $em->persist($campaign);
            $em->persist($criterion);
            $em->flush();
            $html = $this->renderView('TraceBundle:evalcriteria:criterion.html.twig', array('criterion' => $criterion));
            return new Response(json_encode($html));
        }
    }

    /**
     * @Route("/deletecriterion", options={"expose"=true}, name="deletecriterionpage")
     */
    public function deletecriterionAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $criterionid = $request->request->get('criterion_id');
            $criterion = $em->getRepository('TraceBundle:Evaluationcriteria')->findOneBy(array('id' => $criterionid));
            $criterion->setTenant(null);
            $criterion->setCampaign(null);
            $em->remove($criterion);
            $em->flush();
            return new Response(json_encode('success'));
        }
    }

    /**
     * @Route("/editcriterion", options={"expose"=true}, name="editcriterionpage")
     */
    public function editcriterionAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $criterionid = $request->request->get('criterion_id');
            $criterion = $em->getRepository('TraceBundle:Evaluationcriteria')->findOneBy(array('id' => $criterionid));
            $criterionvalue = $request->request->get('value');
            $criterion->setCriterion($criterionvalue);
            $em->persist($criterion);
            $em->flush();
            return new Response(json_encode('success'));
        }
    }

    /**
     * @Route("/loadispublic", options={"expose"=true}, name="loadispublicpage")
     */
    public function loadispublicAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $campaignid = $request->request->get('campaign_id');
            $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
            $resvalue = $campaign->getIspublic();
            return new Response(json_encode($resvalue));
        }
    }

    /**
     * @Route("/editispublic", options={"expose"=true}, name="editispublicpage")
     */
    public function editispublicAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $value = $request->request->get('value');
            $campaignid = $request->request->get('campaign_id');
            $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
            $campaign->setIspublic($value);
            $em->persist($campaign);
            $em->flush();
            return new Response(json_encode('success'));
        }
    }
    
    /**
     * @Route("/loadmsgsanddeadline", options={"expose"=true}, name="loadmsgsanddeadlinepage")
     */
    public function loadmsgsanddeadlineAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $campaignid = $request->request->get('campaign_id');
            $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
            $welcomemsg = $campaign->getPrerecwelcomemsg();
            $thankyoumsg = $campaign->getPrerecthankyoumsg();
            $deadline = $campaign->getDeadlinetoanswer();
            $resvalue = [];
            $resvalue['welcome'] = $welcomemsg ? $welcomemsg : '';
            $resvalue['thankyou'] = $thankyoumsg ? $thankyoumsg : '';
            $resvalue['deadline'] = $deadline ? $deadline : '';
            return new Response(json_encode($resvalue));
        }
    }

    /**
     * @Route("/editmsgsanddeadline", options={"expose"=true}, name="editmsgsanddeadlinepage")
     */
    public function editmsgsanddeadlineAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $name = $request->query->get('name');
            $value = $request->query->get('value');
            $campaignid = $request->query->get('campaign_id');
            $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
            if ($name == 'txt_intro') {
                $campaign->setPrerecwelcomemsg($value);
            } elseif ($name == 'txt_outro') {
                $campaign->setPrerecthankyoumsg($value);
            } elseif ($name == 'answer_period') {
                $campaign->setDeadlinetoanswer($value);
            }
            $em->persist($campaign);
            $em->flush();
            return new Response(json_encode('success'));
        }
    }

    /**
     * @Route("/addchoice", options={"expose"=true}, name="addchoicepage")
     */
    public function addchoiceAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $questionid = $request->request->get('question_id');
            $question = $em->getRepository('TraceBundle:Question')->findOneBy(array('id' => $questionid));
            $questionoption = new QuestionOption();
            $questionoption->setQuestion($question);
            $em->persist($questionoption);
            $em->flush();
            $html = $this->renderView('TraceBundle:choice:choicewidget.html.twig', array('choice' => $questionoption));
            return new Response(json_encode($html));
        }
    }

    /**
     * @Route("/deletechoice", options={"expose"=true}, name="deletechoicepage")
     */
    public function deletechoiceAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $flag = $request->request->get('flag');
            if ($flag == 'deletechoice') {
                $choiceid = $request->request->get('choice_id');
                $questionoption = $em->getRepository('TraceBundle:QuestionOption')->findOneBy(array('id' => $choiceid));
                $em->remove($questionoption);
                $em->flush();
            } elseif ($flag == 'deleteallchoices') {
                $questionid = $request->request->get('question_id');
                $question = $em->getRepository('TraceBundle:Question')->findOneBy(array('id' => $questionid));
                $question->setHasmultipleanswer(false);
                $choices = $question->getQuestionoptions();
                foreach ($choices as $choice) {
                    $question->removeQuestionoption($choice);
                    $em->remove($choice);
                }
                $em->persist($question);
                $em->flush();
            }
            return new Response(json_encode('success'));
        }
    }

    /**
     * @Route("/editchoice", options={"expose"=true}, name="editchoicepage")
     */
    public function editchoiceAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $flag = $request->request->get('flag');
            $value = $request->request->get('value');
            if ($flag == 'edithasmultiple') {
                $questionid = $request->request->get('question_id');
                $hasmultiplevalue = ($value == true) ? 1 : 0;
                $question = $em->getRepository('TraceBundle:Question')->findOneBy(array('id' => $questionid));
                $question->setHasmultipleanswer($hasmultiplevalue);
                $em->persist($question);
                $em->flush();
            } elseif ($flag == 'editchoicevalue') {
                $choiceid = $request->request->get('choice_id');
                $questionoption = $em->getRepository('TraceBundle:QuestionOption')->findOneBy(array('id' => $choiceid));
                $questionoption->setValue($value);
                $em->persist($questionoption);
                $em->flush();
            }
            return new Response(json_encode('success'));
        }
    }

    /**
     * @Route("/loadchoice", options={"expose"=true}, name="loadchoicepage")
     */
    public function loadchoiceAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $choiceid = $request->request->get('choice_id');
            $choice = $em->getRepository('TraceBundle:QuestionOption')->findOneBy(array('id' => $choiceid));
            $choicevalue = $choice->getValue();
            return new Response(json_encode($choicevalue));
        }
    }

    /**
     * @Route("/isactivable", options={"expose"=true}, name="isactivable")
     */
    public function activateAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $validator = $this->get('validator');
        if ($request->isXmlHttpRequest()) {
            $campaignid = $request->request->get('campaign_id');
            $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
            $questions = $campaign->getQuestions();
            $criterions = $campaign->getEvaluationcriteria();
            $i = 0;
            $j = 0;
            $k = 0;
            $l = 0;
            foreach ($questions as $question) {
                if ($question->getResponsetype() == 4) {
                    $choices = $question->getQuestionoptions();
                    foreach ($choices as $choice) {
                        $choiceerrors = $validator->validate($choice, null, array('questions'));
                        if (count($choiceerrors) > 0) {
                            $i++;
                        }
                    }
                } elseif ($question->getResponsetype() == 3) {
                    $questionerrors1 = $validator->validate($question, null, array('videoanswer'));
                    $questionerrors2 = $validator->validate($question, null, array('questions'));
                    if (count($questionerrors1) > 0 || count($questionerrors2) > 0) {
                        $j++;
                    }
                } else {
                    $questionerrors = $validator->validate($question, null, array('questions'));
                    if (count($questionerrors) > 0) {
                        $j++;
                    }
                }
            }
            foreach ($criterions as $criterion) {
                $criterionerrors = $validator->validate($criterion, null, array('questions'));
                if (count($criterionerrors) > 0) {
                    $k++;
                }
            }
            $campaignerrors = $validator->validate($campaign, null, array('create_campaign'));
            if (count($campaignerrors) > 0) {
                $l++;
            }
            if ($i > 0 || $j > 0 || $k > 0 || $l > 0) {
                return new Response(json_encode('error'));
            } 
              return new Response(json_encode('success'));
            
        }
    }

    /**
     * @Route("/videoupload", options={"expose"=true}, name="videouploadpage")
     */
    public function videouploadAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $tenant = $user->getTenant();
        $document = new Document();
        $document->setTenant($tenant);
        $em->persist($document);
        $em->flush();
        $documentid = $document->getId();
        $fs = new Filesystem();
        $dirPath = $request->server->get('DOCUMENT_ROOT') . $request->getBasePath() . '/tenant/'. $tenant->getId() .'/document/' . $documentid . '/video';
        if (!$fs->exists($dirPath)) {
            $fs->mkdir($dirPath);
        }
        $videoname = $request->files->get('file')->getClientOriginalName();
        $request->files->get('file')->move($dirPath, $videoname);
        $document->setDocumentname($videoname);
        $documenttype = $em->getRepository('TraceBundle:Documenttype')->findOneBy(array('id' => 1));
        $document->setDocumenttype($documenttype);
        $em->persist($document);
        $em->flush();
        $html = $this->renderView('TraceBundle:video:videodisplay.html.twig', array('document' => $document, 'tenant' => $tenant));
        return new Response($html);
    }

    /**
     * @Route("/videoselect", options={"expose"=true}, name="videoselectpage")
     */
    public function videoselectAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $questionid = $request->request->get('question_id');
            $question = $em->getRepository('TraceBundle:Question')->findOneBy(array('id' => $questionid));
            $documentid = $request->request->get('document_id');
            $document = $em->getRepository('TraceBundle:Document')->findOneBy(array('id' => $documentid));
            if ($question->getIsvideo() === true) {
                foreach ($question->getDocuments() as $doc) {
                    $existingdoc = $em->getRepository('TraceBundle:Document')->findOneBy(array('id' => $doc->getId(), 'documenttype' => 1));
                    if ($existingdoc) {
                        $question->removeDocument($existingdoc);      
                        break;
                    } 
                }
                $question->addDocument($document);
                $em->persist($question);
                $em->flush();
            } else {
                $question->addDocument($document);
                $question->setIsvideo(true);
                $em->persist($question);
                $em->flush();
            }
            return new Response(json_encode('success'));
        }
    }

    /**
     * @Route("/videoremove", options={"expose"=true}, name="videoremovepage")
     */
    public function videoremoveAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $questionid = $request->request->get('question_id');
            $question = $em->getRepository('TraceBundle:Question')->findOneBy(array('id' => $questionid));
            $documentid = $request->request->get('document_id');
            $document = $em->getRepository('TraceBundle:Document')->findOneBy(array('id' => $documentid));
            $question->setIsvideo(false);
            $question->removeDocument($document);
            $em->persist($question);
            $em->flush();

            return new Response(json_encode('success'));
        }
    }

    /**
     * @Route("/videoplay", options={"expose"=true}, name="videoplaypage")
     */
    public function videoplayAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $tenant = $user->getTenant();
        if ($request->isXmlHttpRequest()) {
            $questionid = $request->request->get('question_id');
            $question = $em->getRepository('TraceBundle:Question')->findOneBy(array('id' => $questionid));
            $documentid = $request->request->get('document_id');
            $document = $em->getRepository('TraceBundle:Document')->findOneBy(array('id' => $documentid));
            $html = $this->renderView('TraceBundle:video:videoplay.html.twig', array('document' => $document, 'question' => $question, 'tenant' => $tenant));
            return new Response(json_encode($html));
        }
    }

    /**
     * @Route("/sortorder", options={"expose"=true}, name="sortorderpage")
     */
    public function sortorderAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        if ($request->isXmlHttpRequest()) {
            $questionids = $request->request->get('questions');
            $campaignid = $request->request->get('campaign_id');
            foreach ($questionids as $questionid) {
                $questions[] = $em->getRepository('TraceBundle:Question')->findOneBy(array('id' => $questionid, 'campaign' => $campaignid));
            }
            foreach ($questions as $key => $question) {
                $question->setSortorder($key + 1);
                $em->persist($question);
                $em->flush();
            }
            return new Response(json_encode('success'));
        }
    }

    /**
     * @Route("/tinymceedit", name="tinymceeditpage" , options = { "expose" = true })
     */
    public function edittinymceAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $flag = $request->query->get('flag');
            if ($flag == 'deadlinetoanswer' || $flag == 'welcomemsg') {
                $name = $request->query->get('name');
                $value = $request->query->get('value');
                return new Response(json_encode($name));
            }
        }
    }
    
    /**
     * @Route("/loadlive", options={"expose"=true}, name="loadlivepage")
     */
    public function loadliveAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $campaignid = $request->query->get('campaign_id');
        $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
        if ($request->isXmlHttpRequest()) {
            $campaignid = $request->request->get('campaign_id');
            $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
            $welcomemsg = $campaign->getLivewelcomemsg(); 
            $thankyoumsg = $campaign->getLivethankyoumsg();
            $resvalue = [];
            $resvalue['welcome'] = $welcomemsg ? $welcomemsg : '';
            $resvalue['thankyou'] = $thankyoumsg ? $thankyoumsg : '';
            return new Response(json_encode($resvalue));
        } 
        
        return $this->render('TraceBundle:Live:loadlive.html.twig', array(
                    'campaign' => $campaign,
        ));
    }
        
    /**
     * @Route("/editlive", options={"expose"=true}, name="editlivepage")
     */
    public function editliveAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $name = $request->query->get('name');
            $value = $request->query->get('value');                
            $campaignid = $request->query->get('campaign_id');           
            $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
            if ($name == 'live_txt_intro') {         
                $campaign->setLivewelcomemsg($value);
            } elseif ($name == 'live_txt_outro') {
                $campaign->setLivethankyoumsg($value);
            } 
            $em->persist($campaign);
            $em->flush();
            return new Response(json_encode('success'));
        }
    }
    
    /**
     * @Route("/loadsharing", options={"expose"=true}, name="loadsharingpage")
     */
    public function loadsharingAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $campaignid = $request->query->get('campaign_id');
        $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
        $campaignclients = $em->getRepository('TraceBundle:CampaignClient')->findBy(array('campaign' => $campaignid));
        return $this->render('TraceBundle:Sharing:loadsharing.html.twig', array(
                'campaign' => $campaign,
                'campaignclients' => $campaignclients,
        ));
    }
    
    /**
     * @Route("/editvisibility", options={"expose"=true}, name="editvisibilitypage")
     */
    public function editvisibilityAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $campaignclientid = $request->request->get('campaignclient_id');
            $campaignclient = $em->getRepository('TraceBundle:CampaignClient')->findOneBy(array('id' => $campaignclientid));
            $visibilityvalue = $request->request->get('value');
            $campaignclient->setVisibility($visibilityvalue);
            $em->persist($campaignclient);
            $em->flush();
            return new Response(json_encode('success'));
        }
    }
    
    /**
     * @Route("/editevaluation", options={"expose"=true}, name="editevaluationpage")
     */
    public function editevaluationAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) {
            $campaignclientid = $request->request->get('campaignclient_id');
            $campaignclient = $em->getRepository('TraceBundle:CampaignClient')->findOneBy(array('id' => $campaignclientid));
            $evaluationvalue = $request->request->get('value');
            $campaignclient->setEvaluation($evaluationvalue);
            $em->persist($campaignclient);
            $em->flush();
            return new Response(json_encode('success'));
        }
    }  
    
    /**
     * @Route("/addapplicant/{campaign_id}", options={"expose"=true}, name="addapplicantpage")
     */
    public function addapplicantAction(Request $request, $campaign_id) {
        $em = $this->getDoctrine()->getManager();
//        $campaignid = $request->query->get('campaign_id');
        $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaign_id));
        $applicant = new Applicant();  
        $form = $this->createForm(AddApplicantType::class, $applicant);  
        $form->handleRequest($request);
//        var_dump($form->getErrors());exit;
//        var_dump($form['email']->getErrors());exit;
        
        
        if ($form->isSubmitted() && $form->isValid()) {
//            $campaignid = $request->request->get('campaignid');
//            $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id' => $campaignid));
            try {
                $applicant->setCampaign($campaign);
            $user = $this->getUser();
            $tenant = $user->getTenant();
            $applicant->setTenant($tenant);
            $applicantstatus = $em->getRepository('TraceBundle:Applicantstatus')->findOneBy(array('id'=>TraceConstants::STATUS_IN_PROGRESS));
            $applicant->setApplicantstatus($applicantstatus);
            $interviewtype = $em->getRepository('TraceBundle:Interviewtype')->findOneBy(array('id'=>1));
            $applicant->setInterviewtype($interviewtype);
            $token = $this->container->get('unique_token')->getToken();
            if($token){
                $applicant->setUniqueinvitecode($token);
            } else {
                $applicant->setIsuniquecodeerror(true);
            }
            $currentdate = new DateTimeImmutable();
            $deadline = $campaign->getDeadlinetoanswer();
            $expirydate = $currentdate->modify('+'.$deadline.' days');
            $applicant->setExpirydate($expirydate);
            $applicant->setInvitedate($currentdate);
            $applicant->setCreatedon($currentdate);
            $applicant->setStatusupdatedon($currentdate);
            $em->persist($applicant);
            $em->flush();
            $url = $this->generateUrl('addapplicantpage', array('campaign_id' => $campaign_id));
            $response = new RedirectResponse($url);
            return $response;
            } 
            catch (UniqueConstraintViolationException $e) {
            $this->addFlash("notice", "This email is already registred for this campaign.");
            }   
        }
        
        return $this->render('TraceBundle:Applicant:addapplicant.html.twig', array(
                    'form' => $form->createView(),
                    'campaign' => $campaign,
        ));
    }  

    /**
     * @Route("/addapplicantlive", options={"expose"=true}, name="addapplicantlivepage")
     */
    public function addapplicantliveAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $tenant = $user->getTenant();
        $applicant = new Applicant();
        
        $form = $this->createForm(AddApplicantLiveType::class, $applicant, array('tenant' => $tenant));
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $applicant->setTenant($tenant);
            $applicantstatus = $em->getRepository('TraceBundle:Applicantstatus')->findOneBy(array('id'=>TraceConstants::STATUS_IN_PROGRESS));
            $applicant->setApplicantstatus($applicantstatus);
            $interviewtype = $em->getRepository('TraceBundle:Interviewtype')->findOneBy(array('id'=>2));
            $applicant->setInterviewtype($interviewtype);
            $token = $this->container->get('unique_token')->getToken();
            if($token){
                $applicant->setUniqueinvitecode($token);
            } else {
                $applicant->setIsuniquecodeerror(true);
            }
            $currentdate = new DateTimeImmutable();
            $applicant->setCreatedon($currentdate);
            $applicant->setStatusupdatedon($currentdate);
            $em->persist($applicant);
            $em->flush();
            $url = $this->generateUrl('livespage');
            $response = new RedirectResponse($url);
            return $response;
        }
        
        return $this->render('TraceBundle:Live:addapplicantlive.html.twig', array(
                    'form' => $form->createView(),
        ));
    }
    
    /**
     * @Route("/applicantlist/{campaignid}", options={"expose"=true}, name="applicantlistpage")
     */
    public function applicantlistAction(Request $request, $campaignid) {
        $em = $this->getDoctrine()->getManager();
        $applicantstatus = $request->query->get('status');
        if(!$applicantstatus){
            $applicantstatus = 'ongoing';
        }
        if($applicantstatus=='ongoing'){
           $applicantstatusid = array(1,7,8,9,10); 
        } elseif($applicantstatus=='shortlisted'){
            $applicantstatusid = 2;
        } elseif($applicantstatus=='archived'){
           $applicantstatusid = array(4,5,6); 
        }
        $applicants = $em->getRepository('TraceBundle:Applicant')->findBy(array('applicantstatus'=>$applicantstatusid, 'campaign'=>$campaignid, 'interviewtype'=>1));
        $campaign = $em->getRepository('TraceBundle:Campaign')->findOneBy(array('id'=>$campaignid));
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                                    $applicants, /* listings NOT result */ 
                                    $request->query->getInt('page', 1)/* page number */,
                                    $this->container->getParameter('pagerange')/* limit per page */
        );
        return $this->render('TraceBundle:Applicant:applicantlist.html.twig', array(
                    'pagination' => $pagination,
                    'status' => $applicantstatus,
                    'campaign' => $campaign,
        ));
    }
    
    /**
     * @Route("/viewinterview/{applicantid}", options={"expose"=true}, name="viewinterviewpage")
     */
    public function viewinterviewAction(Request $request, $applicantid) {
        $em = $this->getDoctrine()->getManager();
        $applicant = $em->getRepository('TraceBundle:Applicant')->findOneBy(array('id' => $applicantid));        
        $campaign = $applicant->getCampaign();
        $cqrs = $applicant->getCampaignquestionresponses();
        
        return $this->render('TraceBundle:Applicant:viewinterview.html.twig', array(
                    'applicant' => $applicant,
                    'campaign' => $campaign,
                    'cqrs' => $cqrs,
        ));
    }

    /**
     * @Route("/deletecdr/{id}", options={"expose"=true}, name="deletecdrpage")
     */
    public function deletecdrAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $cdr = $em->getRepository('TraceBundle:Campaigndocumentresponse')->findOneBy(array('id' => $id));
        $applicant = $cdr->getApplicant();
        $campaign = $applicant->getCampaign();
        
        $fs = new Filesystem();
        $dirpath = $request->server->get('DOCUMENT_ROOT') . $request->getBasePath() . '/campaign/' . $campaign->getId() . '/applicant/' . $applicant->getId() ;
        $fs->remove($dirpath . '/' . $cdr->getValue());
        $cdr->setApplicant(null);
        $cdr->setCampaigndocument(null);
        $em->remove($cdr);
        $em->flush();
        
        $url = $this->generateUrl('viewinterviewpage', array(
            'applicantid' => $applicant->getId(),
        ));
        $response = new RedirectResponse($url);
        return $response;
    }
    
    /**
     * @Route("/comment/{applicantid}", options={"expose"=true}, name="commentpage")
     */
    public function commentAction(Request $request, $applicantid) {
        $em = $this->getDoctrine()->getManager();
        $applicant = $em->getRepository('TraceBundle:Applicant')->findOneBy(array('id' => $applicantid));        
        $user = $this->getUser();
        if ($request->isXmlHttpRequest()) {
            $flag = $request->request->get('flag');
            if($flag=='addcomment'){
                $value = $request->request->get('value');
                $comment = new Comment();
                $comment->setValue($value);
                $currentdate = new DateTimeImmutable();
                $comment->setCommentedon($currentdate);
                //add commentator
                $comment->setClient($user);
                $comment->setApplicant($applicant);
                $em->persist($comment);
                $em->flush();
                $html = $this->renderView('TraceBundle:Comment:comment.html.twig', array('comment' => $comment));
                return new Response(json_encode($html));
            } elseif ($flag=='deletecomment'){
                $commentid = $request->request->get('comment_id');
                $comment = $em->getRepository('TraceBundle:Comment')->findOneBy(array('id' => $commentid));
                $comment->setClient(null);
                $comment->setApplicant(null);
                $em->remove($comment);
                $em->flush();
                return new Response(json_encode('success'));
            } elseif($flag=='editcomment'){
                $value = $request->request->get('value');
                $commentid = $request->request->get('comment_id');
                $comment = $em->getRepository('TraceBundle:Comment')->findOneBy(array('id' => $commentid));$comment->setValue($value);
                $currentdate = new DateTimeImmutable();
                $comment->setCommentedon($currentdate);
                //add commentator
                $comment->setClient($user);
                $em->persist($comment);
                $em->flush();
                $html = $this->renderView('TraceBundle:Comment:editcomment.html.twig', array('comment' => $comment));
                return new Response(json_encode($html));
            }
            
        }
        
    }

    /**
     * @Route("/rating", options={"expose"=true}, name="ratingpage")
     */
    public function ratingAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        
        if ($request->isXmlHttpRequest()) {
            $flag = $request->request->get('flag');
            $applicantid = $request->request->get('applicant_id');
            $applicant = $em->getRepository('TraceBundle:Applicant')->findOneBy(array('id' => $applicantid));        
            if($flag=='editrating'){
                $criterionid = $request->request->get('criterion_id');
                $evalcriteria = $em->getRepository('TraceBundle:Evaluationcriteria')->findOneBy(array('id' => $criterionid));        
                $applicantevaluation = $em->getRepository('TraceBundle:ApplicantEvaluation')->findOneBy(array('applicant' => $applicantid, 'evaluationcriterion' => $criterionid));
                if(!$applicantevaluation){
                    $applicantevaluation = new ApplicantEvaluation();
                    $applicantevaluation->setApplicant($applicant);
                    $applicantevaluation->setEvaluationcriterion($evalcriteria);
                }
                $ratingvalue = $request->request->get('rating_value');
                $applicantevaluation->setValue($ratingvalue);
                $em->persist($applicantevaluation);
                $em->flush();
                //calculate average rating below
                $campaign = $applicant->getCampaign();
                $numofcriteria = sizeof($campaign->getEvaluationcriteria());
                $sumofratings = 0;
                foreach($applicant->getApplicantevaluations() as $evaluation){
                    $sumofratings = $sumofratings + $evaluation->getValue();
                }
                $average = $sumofratings/$numofcriteria;
                $applicant->setAveragerating($average);
                $em->persist($applicant);
                $em->flush();
                return new Response(json_encode($average));
            } elseif($flag=='loadrating'){
                $result = [];
                $criterionid = $request->request->get('criterion_id');
                $applicantevaluation = $em->getRepository('TraceBundle:ApplicantEvaluation')->findOneBy(array('applicant' => $applicantid, 'evaluationcriterion' => $criterionid));
                $result['rating'] = $applicantevaluation->getValue();
                $result['average'] = $applicant->getAveragerating();
                if(!$applicant->getAveragerating()){
                    $result['average'] = 0;
                }
                return new Response(json_encode($result));
            }            
        }
        
    }
    
    /**
     * @Route("/archive/{applicantid}", name="archivepage")
     */
    public function archiveAction(Request $request, $applicantid) {
        $em = $this->getDoctrine()->getManager();
        $applicant = $em->getRepository('TraceBundle:Applicant')->findOneBy(array('id' => $applicantid));        
        $campaignid = $applicant->getCampaign()->getId();
        $status = $em->getRepository('TraceBundle:Applicantstatus')->findOneBy(array('id' => TraceConstants::STATUS_ARCHIVED));
        $applicant->setApplicantstatus($status);
        $currentdate = new DateTimeImmutable();
        $applicant->setStatusupdatedon($currentdate);
        $em->persist($applicant);
        $em->flush();
        if($applicant->getInterviewtype()->getId()==1){
            $url = $this->generateUrl('applicantlistpage', array(
                'campaignid' => $campaignid,
            ));
        } else {
            $url = $this->generateUrl('livespage');
        }
        
        $response = new RedirectResponse($url);
        return $response;
    }

    /**
     * @Route("/shortlist/{applicantid}", name="shortlistpage")
     */
    public function shortlistAction(Request $request, $applicantid) {
        $em = $this->getDoctrine()->getManager();
        $applicant = $em->getRepository('TraceBundle:Applicant')->findOneBy(array('id' => $applicantid));        
        $campaignid = $applicant->getCampaign()->getId();
        $status = $em->getRepository('TraceBundle:Applicantstatus')->findOneBy(array('id' => TraceConstants::STATUS_SHORTLISTED));
        $applicant->setApplicantstatus($status);
        $currentdate = new DateTimeImmutable();
        $applicant->setStatusupdatedon($currentdate);
        $em->persist($applicant);
        $em->flush();
        if($applicant->getInterviewtype()->getId()==1){
            $url = $this->generateUrl('applicantlistpage', array(
                'campaignid' => $campaignid,
            ));
        } else {
            $url = $this->generateUrl('livespage');
        }
        
        $response = new RedirectResponse($url);
        return $response;
    }

    /**
     * @Route("/reject/{applicantid}", name="rejectedpage")
     */
    public function rejectAction(Request $request, $applicantid) {
        $em = $this->getDoctrine()->getManager();
        $applicant = $em->getRepository('TraceBundle:Applicant')->findOneBy(array('id' => $applicantid));        
        $campaignid = $applicant->getCampaign()->getId();
        $status = $em->getRepository('TraceBundle:Applicantstatus')->findOneBy(array('id' => TraceConstants::STATUS_REJECTED));
        $applicant->setApplicantstatus($status);
        $currentdate = new DateTimeImmutable();
        $applicant->setStatusupdatedon($currentdate);
        $em->persist($applicant);
        $em->flush();
        if($applicant->getInterviewtype()->getId()==1){
            $url = $this->generateUrl('applicantlistpage', array(
                'campaignid' => $campaignid,
            ));
        } else {
            $url = $this->generateUrl('livespage');
        }
        
        $response = new RedirectResponse($url);
        return $response;
    }

    /**
     * @Route("/pendingevaluation/{applicantid}", name="pendingevaluationpage")
     */
    public function pendingevaluationAction(Request $request, $applicantid) {
        $em = $this->getDoctrine()->getManager();
        $applicant = $em->getRepository('TraceBundle:Applicant')->findOneBy(array('id' => $applicantid));        
        $campaignid = $applicant->getCampaign()->getId();
        $status = $em->getRepository('TraceBundle:Applicantstatus')->findOneBy(array('id' => TraceConstants::STATUS_PENDING_EVALUATION));
        $applicant->setApplicantstatus($status);
        $currentdate = new DateTimeImmutable();
        $applicant->setStatusupdatedon($currentdate);
        $em->persist($applicant);
        $em->flush();
        if($applicant->getInterviewtype()->getId()==1){
            $url = $this->generateUrl('applicantlistpage', array(
                'campaignid' => $campaignid,
            ));
        } else {
            $url = $this->generateUrl('livespage');
        }
        
        $response = new RedirectResponse($url);
        return $response;
    }
}

