<?php

namespace TraceBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use TraceBundle\Entity\Client;
use TraceBundle\Form\Type\AdminRegistrationType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class RegistrationController extends BaseController {
    
    public function __construct() {
      //        parent::__construct();
    }

    /**
     * @Route("/register/", name="registration")
     */ 
    public function registerAction(Request $request)
    {                
        $client = new Client();  
        $form = $this->createForm(AdminRegistrationType::class, $client);  
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {           
            $client = $form->getData();
            $client->setEnabled(true);
            $client->addRole('ROLE_ADMIN');
            $em = $this->getDoctrine()->getManager();
            $em->persist($client);
            $em->flush();
            $url = $this->generateUrl('successpage');
            $response = new RedirectResponse($url);
            return $response;
        }
        
        return $this->render('TraceBundle:Registration:adminregistration.html.twig', array(
                    'form' => $form->createView(),
        ));            
    }
    
    /**
     * @Route("/success", name="successpage")
     */
    public function successAction()
    {
        return $this->render('default/success.html.twig', array(                 
        ));
    }
    
} 